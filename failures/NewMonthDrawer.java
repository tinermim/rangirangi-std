	public static MonthDrawer drawMonth(Context context, SolarCalendar jDate, int offset) {
		MONTH_OFFSET = offset;
		CONTEXT = context;
		
		if (mainLayout == null) initializeMainLayout(context);
		
		// loading public holidays
		if (MainActivity.holidays == null)
			MainActivity.holidays = Utils.cacheHolidays(context);
 
        int firstDayWeekDay = (35 + jDate.weekDay - (jDate.date - 1) + 1) % 7;
        // last "+ 1" is for making "Shambe" weekday == 0
        
        jDate = jDate.addDays(-(jDate.date - 1));
        int currentMonth = jDate.month;
        jDate = jDate.addDays(6 - firstDayWeekDay); // make calendar RTL
        SolarCalendar today = new SolarCalendar();
        
        ((TextView) mainLayout.findViewById(MONTH_TITLE_ID)).setText(jDate.strMonth);
        
        for (int w = 1; w < 7; w++) {
            for (int i = 0; i < 7; i++) {
            	TextView textView = (TextView) mainLayout.findViewById(7 * w + i + 1);
            	
                if (jDate.month != currentMonth) {
                	if (w != 1 && jDate.date >= 7) break; // no need to have 7 lines of week                	 
                	textView.setBackgroundColor(Color.LTGRAY);
                	textView.setTextColor(Color.WHITE);
                	textView.setText("" + jDate.date);
                	jDate = jDate.addDays(-1); // RTL matter
                }
                else {
                	if (jDate.equals(today)) {
                		textView.setTextColor(TODAY_FONT_COLOR);
                        textView.setBackgroundColor(Color.WHITE);
                	}
                	else {
                		textView.setTextColor(Color.WHITE);
                        textView.setBackgroundColor(Utils.getColor(jDate));     
                	}
                	// putting a circle around holidays cells
                	if (jDate.weekDay == 5 || 
                			jDate.year == Utils.EXISTING_YEAR &&
                			contains(MainActivity.holidays[currentMonth], jDate.date)) {
                		View circle = new View(context);
                		circle.setBackgroundResource(R.drawable.circle);
                		((ViewGroup) textView.getParent()).addView(circle);
                	}
                    textView.setText("" + jDate.date);

                    final int eventYear = jDate.miladiYear - 1900;
                    final int eventMonth = jDate.miladiMonth - 1;
                    final int eventDay = jDate.miladiDate;
                    textView.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            goToDayEvent(eventDay, eventMonth, eventYear);
                        }
                    });
                    int oldDate = jDate.date;
                    jDate = jDate.addDays(-1); // RTL matter
                    
                    // very ugly work. to make Farvardin work.
                    if (currentMonth == 1 && jDate.month == 1 && jDate.date - oldDate != -1)
                    	jDate = jDate.addDays(1);
                }
            }
            if (w != 0) jDate = jDate.addDays(14);            
        }
		
        ViewGroup parent = (ViewGroup) mainLayout.getParent();
        if (parent != null)
        	parent.removeView(mainLayout);
		return mainLayout;
	}
	
	private static void initializeMainLayout(Context context) {
		mainLayout = new MonthDrawer(context);
	}
		
	/**
	 * Creates a blank month layout that can be filled later
	 * @param context The context that MonthDrawer will be drawn to
	 */
	public MonthDrawer(Context context) {
		super(context);
		this.setOrientation(LinearLayout.VERTICAL);
		
		if (MainActivity.SCREEN_WIDTH == 0)
			MainActivity.SCREEN_WIDTH = Utils.getScreenWidth(context);
		
		// Calculating width of the layout
        int layoutWidth = MainActivity.SCREEN_WIDTH;
        
        // initializing basic sizes
        CELL_WIDTH = layoutWidth / 7;
        CELL_FONT_SIZE = CELL_WIDTH / 2;
        WEEKDAY_FONT_SIZE = CELL_WIDTH / 3;
        TITLE_FONT_SIZE = CELL_WIDTH / 2;
        
        // setting month title
        TextView monthYearTitle = new TextView(context);
        monthYearTitle.setTypeface(Utils.getCustomFont(context));
    	monthYearTitle.setGravity(Gravity.CENTER);
    	monthYearTitle.setTextSize(TypedValue.COMPLEX_UNIT_DIP ,
    			Utils.pixelsToDip(context, TITLE_FONT_SIZE));
    	monthYearTitle.setId(MONTH_TITLE_ID);
    	this.addView(monthYearTitle);
    	
    	// adding month day cells
    	final LayoutParams params = new LayoutParams(0, LayoutParams.FILL_PARENT, 1);
        params.setMargins(MonthView.MARGIN, MonthView.MARGIN, MonthView.MARGIN, MonthView.MARGIN);
        for (int w = 0; w < 7; w++) {
            LinearLayout weekLine = new LinearLayout(context);
            weekLine.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, CELL_WIDTH));
            this.addView(weekLine);

            for (int i = 0; i < 7; i++) {
            	FrameLayout cell = new FrameLayout(context);
            	MyTextView textView = new MyTextView(context);
            	cell.setLayoutParams(params);
            	cell.addView(textView);            	
                textView.setGravity(Gravity.CENTER);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 
                		Utils.pixelsToDip(context, CELL_FONT_SIZE));
                if (w == 0) drawWeekdayCell(textView, 6 - i, false);
                textView.setId(7 * w + i + 1);
                weekLine.addView(cell);
            }
        }
	}
