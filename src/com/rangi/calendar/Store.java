package com.rangi.calendar;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;

import com.rangi.calendar.customviews.MyTextView;
import com.rangi.calendar.purchase.util.IabHelper;
import com.rangi.calendar.purchase.util.IabResult;
import com.rangi.calendar.purchase.util.Inventory;
import com.rangi.calendar.purchase.util.Purchase;
/*
 * Store Activity where we offer our available packages which are ready for the customer to buy, here we have 3 main packages(static)
 * in a linear layout. each package has a purchase button which extends the android Button. when they are clicked they call DialogPurchase
 * which handles the process of buying the intended item.
 * */
public class Store extends RangiActionBarActivity {

	private PurchaseButton buyNotes;
	private PurchaseButton buyCalendar;
	private PurchaseButton buySyncGoogle;

	// //////////////////////////////////////
	public ProgressDialog dialog;

	SharedPreferences preferences = null;

	//private String PACKAGENAME = "com.test.purchase";

	// final String KEY = "PERIMIUM" ;

	// Debug tag, for logging
	static final String TAG = "savedPremium";

	// SKUs for our products: the premium upgrade (non-consumable)
	static final String SKU_NOTES = "notes";
	static final String SKU_CALENDAR = "gre_calendar";
	static final String SKU_SYNC_GOOGLE = "google_sync";

	// // Does the user have the premium upgrade?
	// boolean mIsPremium1 = false;

	boolean mIsNotes = false;
	boolean mIsCalendar = false;
	boolean mIsSyncGoogle = false;

	// (arbitrary) request code for the purchase flow
	static final int RC_REQUEST = 10001;

	// The helper object
	IabHelper mHelper;

	// ///////////////////////////////////////////////////

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_store);

		setPurchaseButton(buyNotes, R.id.purchase1,
				PurchaseComponent.PURCHASE_NOTES);
		setPurchaseButton(buyCalendar, R.id.purchase2,
				PurchaseComponent.PURCHASE_GEROGRIAN_HIJRI_CALENDAR);
		setPurchaseButton(buySyncGoogle, R.id.purchase3,
				PurchaseComponent.PURCHASE_SYNC_WITH_GOOGLE);
		setupActionBar();
		setupDrawer();

		
//		getSharedPreferences("PreferencesName", this.MODE_PRIVATE).edit().remove("Notes").commit();
//		getSharedPreferences("PreferencesName", this.MODE_PRIVATE).edit().remove("Calendar").commit();
//		getSharedPreferences("PreferencesName", this.MODE_PRIVATE).edit().remove("SyncGoogle").commit();
		
		// //////////////////////////////////////////////////
		// load your setting that are you premium or not?
		// preferences = getSharedPreferences(PACKAGENAME,Context.MODE_PRIVATE);
		// PACKAGENAME = getClass().getName();
		// Log.e("TAG", PACKAGENAME);
		// mIsPremium1 = preferences.getBoolean(KEY, false);
		// if (mIsPremium1 == true) {
		// updateUi();
		// return;
		// }

		
		

		// You can find it in your Bazaar console, in the Dealers section.
		// It is recommended to add more security than just pasting it in your
		// source code;


		// //////////////////////////////////////////////////////////////////////////////////////////

	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		setPurchaseButton(buyNotes, R.id.purchase1,
				PurchaseComponent.PURCHASE_NOTES);
		setPurchaseButton(buyCalendar, R.id.purchase2,
				PurchaseComponent.PURCHASE_GEROGRIAN_HIJRI_CALENDAR);
		setPurchaseButton(buySyncGoogle, R.id.purchase3,
				PurchaseComponent.PURCHASE_SYNC_WITH_GOOGLE);
	}

	public void setPurchaseButton(PurchaseButton b, int id, PurchaseComponent pc) {
		b = (PurchaseButton) findViewById(id);
		b.item = pc;
		//PurchaseControl.setPurchaseSituation(pc, false);
		disableButtonOnPurchased(b);
	}

	private void disableButtonOnPurchased(PurchaseButton b) {
		if(PurchaseControl.getInstance(this).isPurchased(b.item)){
			b.setBackgroundColor(getResources().getColor(R.color.purchased_button));
			b.setText("خریداری شده است");
			b.setClickable(false);
			
		}
	}

	protected void setupActionBar() {
		super.setupActionBar(R.layout.action_bar_store);
		// ((ImageView) findViewById(R.id.icon))
		// .setBackgroundResource(R.drawable.ic_store);
		((MyTextView) findViewById(R.id.title)).setText(R.string.action_store);
	
		((MyTextView) findViewById(R.id.title)).setTextSize(
				TypedValue.COMPLEX_UNIT_SP, 22);
	}

	// /////////////////////////////////////////////////////////////////////////////////////////////////
	IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
		public void onQueryInventoryFinished(IabResult result,
				Inventory inventory) {
			Log.d(TAG, "Query inventory finished.");
			if (result.isFailure()) {
				Log.d(TAG, "Failed to query inventory: " + result);
				dialog.hide();
				return;
			} else {
				Log.d(TAG, "Query inventory was successful.");
				// does the user have the premium upgrade?
				mIsNotes = inventory.hasPurchase(SKU_NOTES);
				mIsCalendar = inventory.hasPurchase(SKU_CALENDAR);
				mIsSyncGoogle = inventory.hasPurchase(SKU_SYNC_GOOGLE);

				// update UI accordingly

				Log.d(TAG, "User Notes is "
						+ (mIsNotes ? "PREMIUM" : "NOT PREMIUM"));
				Log.d(TAG, "User Calendar is "
						+ (mIsCalendar ? "PREMIUM" : "NOT PREMIUM"));
				Log.d(TAG, "User SyncGoogle is "
						+ (mIsSyncGoogle ? "PREMIUM" : "NOT PREMIUM"));

			}
			dialog.hide();
			updateUi();
			setWaitScreen(false);
			Toast.makeText(getApplicationContext(),
					mIsNotes ? "premuimUser" : "NotPremuimUser",
					Toast.LENGTH_SHORT).show();
			Log.d(TAG, "Initial inventory query finished; enabling main UI.");

		}

	};

	public void onSavedUpgradeAppButtonClicked(final View v) {

		// Log.e("item clikced" , ""+((PurchaseButton)v).item);
		// DialogPurchase cdd=new DialogPurchase((Activity)v.getContext() ,
		// ((PurchaseButton)v).item);
		// cdd.show();
		
		// show loading dialog with ProgressDialog

//		dialog = new ProgressDialog(this);
//		dialog.setMessage("loading...");
//		dialog.setCancelable(false);
//		dialog.setInverseBackgroundForced(false);
//		dialog.show();
		
	
		String base64EncodedPublicKey = "MIHNMA0GCSqGSIb3DQEBAQUAA4G7ADCBtwKBrwDgt9709p98OG03+SZtPvpWpwp6WdSTijCPUWECEgcAH/Fj5KBcP2IIotRbXaYIUWaBsW/Cwn2gsEshwVgAXeTergysML8ubqtMbtSWoRU81yR17wetYG73LVsTUxr8v0xSCl7F5s105/k7IpZMKmQM44yLznWXKbz1wDkYqSz5M3v+PD5Bz0s4KkJ45qyQicQWeWl/lK1RzE60c3SKkEuOjeQWiubE90GWOfbJkHcCAwEAAQ==";

		mHelper = new IabHelper(this, base64EncodedPublicKey);

		Log.d(TAG, "Starting setup.");
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			public void onIabSetupFinished(IabResult result) {
				Log.d(TAG, "Setup finished.");

				if (!result.isSuccess()) {
					// Oh noes, there was a problem.
					Log.d(TAG, "Problem setting up In-app Billing: " + result);
					return;
				}
				
				// Hooray, IAB is fully set up!
			//	mHelper.queryInventoryAsync(mGotInventoryListener);
				
				
				
				/*
				 * TODO: for security, generate your payload here for verification. See
				 * the comments on verifyDeveloperPayload() for more info. Since this is
				 * a SAMPLE, we just use an empty string, but on a production app you
				 * should carefully generate this.
				 */
				String payload = "gdhassdflsldaslfkahsjahsjakaasa";

				switch (((PurchaseButton) v).item) {
				case PURCHASE_NOTES:
					Log.i("Store-purchase", SKU_NOTES);
					mHelper.launchPurchaseFlow(Store.this, SKU_NOTES, RC_REQUEST,
							mPurchaseFinishedListener, payload);
					break;
				case PURCHASE_GEROGRIAN_HIJRI_CALENDAR:
					Log.i("Store-purchase", SKU_CALENDAR);
					mHelper.launchPurchaseFlow(Store.this, SKU_CALENDAR, RC_REQUEST,
							mPurchaseFinishedListener, payload);
					break;
				case PURCHASE_SYNC_WITH_GOOGLE:
					Log.i("Store-purchase", SKU_SYNC_GOOGLE);
					mHelper.launchPurchaseFlow(Store.this, SKU_SYNC_GOOGLE, RC_REQUEST,
							mPurchaseFinishedListener, payload);
					break;
				default:
					mHelper = null;

				}	
			}
		});
		
//		
//	if(PurchaseControl.isPurchased(((PurchaseButton)v).item))
//		Toast.makeText(this, "you've baught it before", Toast.LENGTH_SHORT).show();
//	else{
//		
//		PurchaseControl.setPurchaseSituation(((PurchaseButton)v).item,true);
//		Toast.makeText(this, "masalan kharidari shod", Toast.LENGTH_SHORT).show();
		
		
	//}
			
		
		Log.i("savedOnUpgrade", "purchase button clicked;"
				+ ((PurchaseButton) v).item);
		setWaitScreen(true);

		
		
	
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		Log.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + ","
				+ data);

		// Pass on the activity result to the helper for handling
		if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
			super.onActivityResult(requestCode, resultCode, data);
		} else {
			Log.d(TAG, "onActivityResult handled by IABUtil.");
		}
		

	}

	boolean verifyDeveloperPayload(Purchase p) {
	//	String payload = p.getDeveloperPayload();

		return true;
	}

	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
			Log.d(TAG, "Purchase finished: " + result + ", purchase: "
					+ purchase);
			if (result.isFailure()) {
				Log.d(TAG, "Error purchasing: " + result);
				setWaitScreen(false);
				return;
			}
			if (!verifyDeveloperPayload(purchase)) {
				complain("Error purchasing. Authenticity verification failed.");
				setWaitScreen(false);
				return;
			}

			Log.d(TAG, "Purchase successful.");

			if (purchase.getSku().equals(SKU_NOTES)) {
				Log.d(TAG, "Purchase is NOTES. Congratulating user.");
				PurchaseControl.getInstance(Store.this).setPurchaseSituation(PurchaseComponent.PURCHASE_NOTES, true);
				mIsNotes = true;
			} else if (purchase.getSku().equals(SKU_CALENDAR)) {
				Log.d(TAG, "Purchase is CALENDAR. Congratulating user.");
				PurchaseControl.getInstance(Store.this).setPurchaseSituation(PurchaseComponent.PURCHASE_GEROGRIAN_HIJRI_CALENDAR, true);
				mIsCalendar = true;
			} else if (purchase.getSku().equals(SKU_SYNC_GOOGLE)) {
				Log.d(TAG, "Purchase is SYNC_GOOGLE. Congratulating user.");
				PurchaseControl.getInstance(Store.this).setPurchaseSituation(PurchaseComponent.PURCHASE_SYNC_WITH_GOOGLE, true);
				mIsSyncGoogle = true;
			}
			alert("ممنون از خرید شما");
			updateUi();
			setWaitScreen(false);

		}
	};

	@Override
	public void onDestroy() {
		super.onDestroy();

		Log.d(TAG, "Destroying helper.");
		if (mHelper != null)
			mHelper.dispose();
		mHelper = null;
	}

	// Update button with updateUi
	public void updateUi() {		
		Log.d("updateUi", "calling DisableButtonOnPurchased1");
		disableButtonOnPurchased((PurchaseButton)findViewById(R.id.purchase1));
		Log.d("updateUi", "calling DisableButtonOnPurchased2");
		disableButtonOnPurchased((PurchaseButton)findViewById(R.id.purchase2));
		Log.d("updateUi", "calling DisableButtonOnPurchased3");
		disableButtonOnPurchased((PurchaseButton)findViewById(R.id.purchase3));
	}

	// TODO
	// Enables or disables the "please wait" screen.
	void setWaitScreen(boolean set) {
		// findViewById(R.id.screen_wait).setVisibility(set ? View.VISIBLE :
		// View.GONE);
		Log.e("Store-setWaitScreen", "NOthing's done here");
	}

	void complain(String message) {
		Log.e(TAG, "**** testbilling Error: " + message);
		alert("Error: " + message);
	}

	void alert(String message) {
		AlertDialog.Builder bld = new AlertDialog.Builder(this);
		bld.setMessage(message);
		bld.setNeutralButton("OK", null);
		Log.d(TAG, "Showing alert dialog: " + message);
		bld.create().show();

	}
}
