package com.rangi.calendar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rangi.calendar.MyDataTypes.Note;
import com.rangi.calendar.customviews.MyTextView;
import com.rangi.calendar.note.NoteManager;
import com.rangi.calendar.settings.SettingsUtil;
import com.rangi.calendar.utils.ColorUtils;
import com.rangi.calendar.utils.Utils;

import java.text.DateFormatSymbols;
import org.joda.time.LocalDate;
import org.joda.time.chrono.IslamicChronology;

public class MonthDrawer extends LinearLayout {

	private static final int TITLE_COLOR = Color.parseColor("#666666");
	private static final int SECONDARY_TITLE_COLOR = Color
			.parseColor("#c63009");
	public static int MONTH_OFFSET = 0;
	private static Context CONTEXT;

	// Class Constants
	private final static String SHANBE = "شنبه";
	private final static String JOME = "جمعه";
	private static final String[] SHORT_WEEKDAYS = new DateFormatSymbols()
			.getShortWeekdays();

	private final int CELL_WIDTH;
	private final int CELL_FONT_SIZE;
	private static int WEEKDAY_FONT_SIZE;
	private final int TITLE_FONT_SIZE;
	private final int SECONDARY_TITLE_FONT_SIZE;

	public MonthDrawer(Context context, int offset) {
		super(context);

		
		Log.i("MonthDrawer" , "monthDrawer");
		
		
		this.setOrientation(LinearLayout.VERTICAL);
		MONTH_OFFSET = offset;
		CONTEXT = context;

		// initializing basic sizes
		CELL_WIDTH = Utils.getScreenWidth(context) / 7;
		CELL_FONT_SIZE = CELL_WIDTH / 2;
		WEEKDAY_FONT_SIZE = CELL_WIDTH / 3;
		TITLE_FONT_SIZE = CELL_WIDTH / 2;
		SECONDARY_TITLE_FONT_SIZE = CELL_WIDTH / 4;

		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(context);
		if (settings.getString("primary_calendar", "0").equals("1")) // Jalali
																		// is
																		// primary
			drawMonth(context, SolarCalendar.offsetMonth(offset), offset);
		else
			// Gregorian is primary
			drawMonth(context, new LocalDate().plusMonths(offset)
					.withDayOfMonth(1), offset);
	}

	/**
	 * Jalali month drawer
	 */
	private void drawMonth(Context context, SolarCalendar jDate, int offset) {

		LocalDate date = new LocalDate(jDate.miladiYear, jDate.miladiMonth,
				jDate.miladiDate);
		date = date.minusDays(jDate.date - 1);

		addMonthTitle(jDate.strMonth, SolarCalendar.getFarsiMonthName(date)
				+ "/" + SolarCalendar.getFarsiMonthName(date.plusMonths(1)));

		// last "+ 1" is for making "Shambe" weekday == 0
		int firstDayWeekDay = (35 + jDate.weekDay - (jDate.date - 1) + 1) % 7;
		int currentMonth = jDate.month;
		date = date.plusDays(6 - firstDayWeekDay); // make calendar RTL

		final LayoutParams params = new LayoutParams(0,
				LayoutParams.MATCH_PARENT, 1);
		for (int w = 0; w < getNumOfRows(context); w++) {
			LinearLayout weekLine = new LinearLayout(context);
			weekLine.setLayoutParams(new LayoutParams(
					LayoutParams.MATCH_PARENT, 0, 1));
			this.addView(weekLine);

			for (int i = 0; i < 7; i++) {

				final TextView textView = new TextView(context);
				textView.setTypeface(Utils.getCustomFont(context));
				textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,
						Utils.pixelsToDip(CONTEXT, CELL_FONT_SIZE));

				jDate = new SolarCalendar(date);

				if (w == 0) {
					boolean shortName = false;
					drawWeekdayCell(textView, 6 - i, shortName); // (6 - i) RTL
																	// matter
					textView.setLayoutParams(params);
					weekLine.addView(textView);
				} else if (jDate.month != currentMonth) {

					boolean haveNote = NoteManager.hasNote(context, jDate);

					View dayCell = new DayCellDrawer(context, CELL_WIDTH, ""
							+ jDate.date, "", "", Color.LTGRAY, false, false,
							true, haveNote);// TODO

					dayCell.setLayoutParams(params);
					weekLine.addView(dayCell);
					date = date.minusDays(1); // RTL matter
				} else {

					String jDay = "" + jDate.date;
					String hDay = ""
							+ new LocalDate(date.toDate(),
									IslamicChronology.getInstance())
									.getDayOfMonth();
					String gDay = "" + date.getDayOfMonth();
					int color = ColorUtils.getColor(context, currentMonth, 7
							* (w - 1) + 6 - i);
					boolean isToday = false;
					if (date.equals(LocalDate.now()))
						isToday = true;
					boolean isHoliday = false;
					if (jDate.weekDay == 5 || Cache.isHoliday(context, date)
							&& SettingsUtil.showHolidays(context))
						isHoliday = true;

					boolean haveNote = NoteManager.hasNote(context, jDate);

					View dayCell = new DayCellDrawer(context, CELL_WIDTH, jDay,
							hDay, gDay, color, isToday, isHoliday, true,
							haveNote);// TODO
					dayCell.setLayoutParams(params);

					final int eventYear = date.getYear();
					final int eventMonth = date.getMonthOfYear();
					final int eventDay = date.getDayOfMonth();
					dayCell.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View arg0) {
							goToDayEvent(eventDay, eventMonth, eventYear);
						}
					});
					weekLine.addView(dayCell);
					date = date.minusDays(1); // RTL matter
				}
			}
			if (w != 0)
				date = date.plusDays(14);
		}
	}

	/**
	 * Gregorian month drawer
	 */
	private void drawMonth(Context context, LocalDate date, int offset) {

		addMonthTitle(SolarCalendar.getFarsiMonthName(date),
				new SolarCalendar(date.withDayOfMonth(1)).strMonth + "/"
						+ new SolarCalendar(date.withDayOfMonth(28)).strMonth);

		int currentMonth = date.getMonthOfYear();
		date = date.minusDays(date.getDayOfWeek() % 7);

		final LayoutParams params = new LayoutParams(0,
				LayoutParams.MATCH_PARENT, 1);
		for (int w = 0; w < getNumOfRows(context); w++) {
			LinearLayout weekLine = new LinearLayout(context);
			weekLine.setLayoutParams(new LayoutParams(
					LayoutParams.MATCH_PARENT, 0, 1));
			this.addView(weekLine);

			for (int i = 0; i < 7; i++) {

				final TextView textView = new TextView(context);
				textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,
						Utils.pixelsToDip(CONTEXT, CELL_FONT_SIZE));
				if (w == 0) {
					drawWeekdayCell(textView, SHORT_WEEKDAYS[i + 1]);
					textView.setLayoutParams(params);
					weekLine.addView(textView);
				} else if (date.getMonthOfYear() != currentMonth) {

					boolean haveNote = NoteManager.hasNote(context, date);

					View dayCell = new DayCellDrawer(context, CELL_WIDTH, ""
							+ date.getDayOfMonth(), "", "", Color.LTGRAY,
							false, false, false, haveNote);// TODO
					dayCell.setLayoutParams(params);
					weekLine.addView(dayCell);
					date = date.plusDays(1);
				} else {
					SolarCalendar jDate = new SolarCalendar(date);
					String jDay = "" + jDate.date;
					String hDay = ""
							+ new LocalDate(date.toDate(),
									IslamicChronology.getInstance())
									.getDayOfMonth();
					String gDay = "" + date.getDayOfMonth();
					int color = ColorUtils.getColor(context,
							(currentMonth + 9) % 12 + 1, 7 * (w - 1) + i);
					boolean isToday = false;
					if (date.equals(LocalDate.now()))
						isToday = true;
					boolean isHoliday = false;
					if (Cache.isHoliday(context, date)
							&& SettingsUtil.showHolidays(context))
						isHoliday = true;

					boolean haveNote = NoteManager.hasNote(context, date);

					View dayCell = new DayCellDrawer(context, CELL_WIDTH, gDay,
							hDay, jDay, color, isToday, isHoliday, false,
							haveNote);// TODO
					dayCell.setLayoutParams(params);

					final int eventYear = date.getYear();
					final int eventMonth = date.getMonthOfYear();
					final int eventDay = date.getDayOfMonth();
					dayCell.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View arg0) {
							goToDayEvent(eventDay, eventMonth, eventYear);
						}
					});
					weekLine.addView(dayCell);
					date = date.plusDays(1); // RTL matter
				}
			}
		}
	}

	private int getNumOfRows(Context context) {
		if ((double) Utils.getScreenHeight(context)
				/ Utils.getScreenWidth(context) < 1.6)
			return 7;
		return 8;
	}

	private void addMonthTitle(String month, String secondaryTitle) {
		MyTextView monthTitle = new MyTextView(CONTEXT);
		monthTitle.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				0, 2f / 3));
		monthTitle.setTextColor(TITLE_COLOR);
		monthTitle.setGravity(Gravity.CENTER);
		monthTitle.setTextSize(TypedValue.COMPLEX_UNIT_DIP,
				Utils.pixelsToDip(CONTEXT, TITLE_FONT_SIZE));
		monthTitle.setText(month);
		addView(monthTitle);

		MyTextView secondaryMonthTitle = new MyTextView(CONTEXT);
		secondaryMonthTitle.setLayoutParams(new LayoutParams(
				LayoutParams.MATCH_PARENT, 0, 0.5f));
		secondaryMonthTitle.setTextColor(SECONDARY_TITLE_COLOR);
		secondaryMonthTitle.setGravity(Gravity.CENTER);
		secondaryMonthTitle.setTextSize(TypedValue.COMPLEX_UNIT_DIP,
				Utils.pixelsToDip(CONTEXT, SECONDARY_TITLE_FONT_SIZE));
		secondaryMonthTitle.setText(secondaryTitle);
		
		
		// condition to show the purchased items
		// if not purchased we won't show other calendar months in Month View
		if(PurchaseControl.getInstance(getContext()).isPurchased(PurchaseComponent.PURCHASE_GEROGRIAN_HIJRI_CALENDAR) )
			addView(secondaryMonthTitle);
	}

	private void drawWeekdayCell(TextView textView, String weekDayName) {
		textView.setBackgroundResource(R.drawable.rectangle);
		textView.setTextColor(TITLE_COLOR);
		textView.setGravity(Gravity.CENTER);
		textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,
				Utils.pixelsToDip(CONTEXT, WEEKDAY_FONT_SIZE));

		textView.setText(weekDayName);
	}

	public static void drawWeekdayCell(TextView textView, int i,
			boolean shortName) {
		textView.setBackgroundResource(R.drawable.rectangle);
		textView.setTextColor(TITLE_COLOR);
		textView.setGravity(Gravity.CENTER);
		textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,
				Utils.pixelsToDip(CONTEXT, WEEKDAY_FONT_SIZE));
		textView.setText(getWeekdayName(i, shortName));
	}

	private static String getWeekdayName(int i, boolean shortName) {
		if (!shortName) {
			if (i == 0)
				return SHANBE;
			if (i == 6)
				return JOME;
			return i + "\n" + SHANBE;
		}
		return "!";
	}

	private static void goToDayEvent(final int day, final int month,
			final int year) {
		Intent intent = new Intent(CONTEXT, DayView.class);
		intent.putExtra(MainActivity.DAY, day);
		intent.putExtra(MainActivity.MONTH, month);
		intent.putExtra(MainActivity.YEAR, year);
		CONTEXT.startActivity(intent);
	}
}
