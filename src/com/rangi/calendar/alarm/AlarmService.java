package com.rangi.calendar.alarm;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import com.rangi.calendar.MyDataTypes.Reminder;
import com.rangi.calendar.SolarCalendar;
import com.rangi.calendar.database.DatabaseHelper;
import com.rangi.calendar.settings.SettingsUtil;
import com.rangi.calendar.widget.Widget1x1;
import com.rangi.calendar.widget.Widget4x1;
import com.rangi.calendar.widget.Widget4x2;

import org.joda.time.DateTime;

import java.util.List;

public class AlarmService extends Service {

	private static final int DAILY_NOTIF_ID = 0;

	public static final String ALARM_ID = "KEY_ALARM_ID";

	public static final String ON_TASK_REMOVED_RESTART = "KEY_ON_TASK_REMOVED_RESTART";

	public static final String SET_DAILY_ALARM = "ACTION_SET_DAILY_ALARM";
	public static final String CANCEL_DAILY_ALARM = "ACTION_CANCEL_DAILY_ALARM";
	public static final String RESET_DAILY_ALARM = "ACTION_RESET_DAILY_ALARM";

	public static final String SET_REMINDER = "ACTION_SET_REMINDER";
	public static final String CANCEL_REMINDER = "ACTION_CANCEL_REMINDER";
	public static final String RESET_REMINDER = "ACTION_RESET_REMINDER";

	private static final String SET_ALL_REMINDERS = "ACTION_SET_ALL_REMINDERS";

	private static Service refrence = null;

	@Override
	public void onCreate() {
		refrence = this;

		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_TIME_CHANGED);
		registerReceiver(timeManuallyChanged, filter);

		startWidgetUpdateReceiver(getApplicationContext());

		super.onCreate();
	}

	BroadcastReceiver timeManuallyChanged = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// reseting reminders
			cancelReminders();
			setReminders();

			// reseting daily alarm
			cancelDailyAlarm();
			setDailyAlarm();

			updateAllWidgets(context);
		}
	};

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	@Override
	public void onTaskRemoved(Intent rootIntent) {
		Intent restartService = new Intent(getApplicationContext(),
				this.getClass());
		restartService.putExtra(ON_TASK_REMOVED_RESTART, true);
		restartService.setPackage(getPackageName());
		PendingIntent restartServicePI = PendingIntent.getService(
				getApplicationContext(), 1, restartService,
				PendingIntent.FLAG_ONE_SHOT);
		AlarmManager alarmService = (AlarmManager) getApplicationContext()
				.getSystemService(Context.ALARM_SERVICE);
		alarmService.set(AlarmManager.ELAPSED_REALTIME,
				SystemClock.elapsedRealtime() + 1000, restartServicePI);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		long alarmId = -1;
		if (intent == null || intent.hasExtra(ON_TASK_REMOVED_RESTART)) {
			setDailyAlarm();
			setReminders();
		} else {
			String action = intent.getAction();
			if (intent.hasExtra(ALARM_ID))
				alarmId = intent.getLongExtra(ALARM_ID, -1);

			if (action.equals(SET_DAILY_ALARM))
				setDailyAlarm();
			else if (action.equals(CANCEL_DAILY_ALARM))
				cancelDailyAlarm();
			else if (action.equals(RESET_DAILY_ALARM)) {
				cancelDailyAlarm();
				setDailyAlarm();
			}

			else if (action.equals(SET_ALL_REMINDERS))
				setReminders();

			else if (action.equals(SET_REMINDER))
				setReminder(alarmId);
			else if (action.equals(CANCEL_REMINDER)) {
				cancelReminder(alarmId);
			} else if (action.equals(RESET_REMINDER)) {
				cancelReminder(alarmId);
				setReminder(alarmId);
			}
		}
		return Service.START_STICKY;
	}

	private void setDailyAlarm() {
		if (SettingsUtil.isDailyAlarmEnable(this)) {
			DateTime dateTime = getDateTime(SettingsUtil
					.getDailyAlarmTime(this));
			PendingIntent pendingIntent = getDailyAlarmPendingIntent();
			AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
			alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
					dateTime.getMillis(), AlarmManager.INTERVAL_DAY,
					pendingIntent);
			// enableBootReceiver();
		}
	}

	private void cancelDailyAlarm() {
		PendingIntent pendingIntent = getDailyAlarmPendingIntent();
		((AlarmManager) this.getSystemService(Context.ALARM_SERVICE))
				.cancel(pendingIntent);
		((NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE))
				.cancel(DAILY_NOTIF_ID);
		// disableBootReceiver();
		pendingIntent.cancel();
	}

	private DateTime getDateTime(String dailyAlarmTime) {
		int hour = Integer.parseInt(dailyAlarmTime.split(":")[0]);
		int minute = Integer.parseInt(dailyAlarmTime.split(":")[1]);

		DateTime dateTime = new DateTime().withHourOfDay(hour)
				.withMinuteOfHour(minute).withSecondOfMinute(0)
				.withMillisOfSecond(0);
		// set to next day if time is before now
		if (dateTime.isBefore(DateTime.now()))
			dateTime = dateTime.plusDays(1);

		return dateTime;
	}

	private PendingIntent getDailyAlarmPendingIntent() {
		Intent intent = new Intent(this, AlarmReceiver.class);
		intent.setAction(AlarmReceiver.SHOW_DAILY_NOTIF);
		intent.putExtra(ALARM_ID, DAILY_NOTIF_ID);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(this,
				DAILY_NOTIF_ID, intent, 0);
		return pendingIntent;
	}

	private void setReminder(long id) {
		Reminder reminder = DatabaseHelper.getDbHelper(this).getReminder(id);
		DateTime dateTime = getReminderDateTime(reminder);
		if (dateTime == null)
			return;

		PendingIntent pendingIntent = getReminderPendingIntent(reminder);
		AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

		alarmManager.set(AlarmManager.RTC_WAKEUP, dateTime.getMillis(),
				pendingIntent);

	}

	private DateTime getReminderDateTime(Reminder reminder) {
		DateTime dateTime = null;
		if (reminder.isFor.equals(Reminder.REMINDER_FOR_NOTE)) {
			dateTime = new DateTime(reminder.dateTime);
			if (dateTime.isBeforeNow())
				return null;
		} else if (reminder.isFor.equals(Reminder.REMINDER_FOR_BIRTHDAY)) {
			dateTime = getBirthdayDateTime(reminder, new SolarCalendar().year);
			if (dateTime.isBeforeNow())
				dateTime = getBirthdayDateTime(reminder,
						new SolarCalendar().year + 1);
		}
		return dateTime;
	}

	private DateTime getBirthdayDateTime(Reminder reminder, int year) {
		DateTime dateTime;
		String[] dateTimePieces = reminder.dateTime.split("-");
		int month = Integer.valueOf(dateTimePieces[0]);
		int day = Integer.valueOf(dateTimePieces[1]);
		int hour = Integer.valueOf(dateTimePieces[2]);
		int minute = Integer.valueOf(dateTimePieces[3]);
		dateTime = SolarCalendar.jalaliToGregorian(year, month, day)
				.toDateTimeAtStartOfDay().withHourOfDay(hour)
				.withMinuteOfHour(minute)
				.minusDays(reminder.getDaysBeforeBirthday());
		return dateTime;
	}

	private void cancelReminder(long id) {
		PendingIntent pendingIntent = getReminderPendingIntent(id);
		((AlarmManager) this.getSystemService(Context.ALARM_SERVICE))
				.cancel(pendingIntent);
		((NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE))
				.cancel((int) id);
		pendingIntent.cancel();
	}

	private PendingIntent getReminderPendingIntent(long reminderId) {
		Intent intent = new Intent(this, AlarmReceiver.class);
		intent.setAction(AlarmReceiver.SHOW_REMINDER_NOTIF);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(this,
				(int) reminderId, intent, 0);
		return pendingIntent;
	}

	private PendingIntent getReminderPendingIntent(Reminder reminder) {
		Intent intent = new Intent(this, AlarmReceiver.class);
		intent.putExtra(ALARM_ID, reminder.id);
		intent.setAction(AlarmReceiver.SHOW_REMINDER_NOTIF);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(this,
				(int) reminder.id, intent, 0);
		return pendingIntent;
	}

	/*
	 * Sets Note and Birthday reminders in case of AlarmService being restared.
	 */
	private void setReminders() {
		List<Reminder> reminders = DatabaseHelper.getDbHelper(this)
				.getAllReminders();
		for (Reminder reminder : reminders) {
			setReminder(reminder.id);
		}
	}

	private void cancelReminders() {
		List<Reminder> reminders = DatabaseHelper.getDbHelper(this)
				.getAllReminders();
		for (Reminder reminder : reminders) {
			cancelReminder(reminder.id);
		}
	}

	public static boolean isRunning() {
		return refrence != null;
	}

	private void enableBootReceiver() {
		ComponentName receiver = new ComponentName(this,
				AlarmBootReceiver.class);
		PackageManager pm = getPackageManager();

		pm.setComponentEnabledSetting(receiver,
				PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
				PackageManager.DONT_KILL_APP);
	}

	private void disableBootReceiver() {
		ComponentName receiver = new ComponentName(this,
				AlarmBootReceiver.class);
		PackageManager pm = getPackageManager();

		pm.setComponentEnabledSetting(receiver,
				PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
				PackageManager.DONT_KILL_APP);
	}

	/******************************************************************
	 * Convenience methods for manipulating alarms easily from outside.
	 ******************************************************************/

	public static void setDailyAlarm(Context context) {
		startServiceWithAction(context, SET_DAILY_ALARM);
	}

	public static void cancelDailyAlarm(Context context) {
		startServiceWithAction(context, CANCEL_DAILY_ALARM);
	}

	public static void resetDailyAlarm(Context context) {
		startServiceWithAction(context, RESET_DAILY_ALARM);
	}

	private static void startServiceWithAction(Context context, String action) {
		Intent service = new Intent(context, AlarmService.class);
		service.setAction(action);
		context.startService(service);
	}

	public static void setReminders(Context context) {
		startServiceWithAction(context, SET_ALL_REMINDERS);
	}

	public static void setReminder(Context context, long id) {
		startServiceWithActionAndId(context, SET_REMINDER, id);
	}

	/*
	 * Cancels the reminde and then deletes it from database
	 */
	public static void cancelReminder(Context context, long id) {
		startServiceWithActionAndId(context, CANCEL_REMINDER, id);
	}

	public static void resetReminder(Context context, long id) {
		startServiceWithActionAndId(context, RESET_REMINDER, id);
	}

	private static void startServiceWithActionAndId(Context context,
			String action, long id) {
		Intent service = new Intent(context, AlarmService.class);
		service.setAction(action);
		service.putExtra(ALARM_ID, id);
		context.startService(service);
	}

	private static void updateAllWidgets(Context context) {

		Log.d("ff", "updateAllWidgets");
		Intent intent3 = new Intent();

		intent3.setAction(Widget1x1.UPDATE_ACTION);
		context.sendBroadcast(intent3);

		intent3.setAction(Widget4x1.UPDATE_ACTION);
		context.sendBroadcast(intent3);

		intent3.setAction(Widget4x2.UPDATE_ACTION);
		context.sendBroadcast(intent3);
	}

	public static void startWidgetUpdateReceiver(Context context) {
		Log.d("ff", "startWidgetUpdateReceiver");

		// because of first update
		updateAllWidgets(context);

		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_TIME_CHANGED);
		filter.addAction(Intent.ACTION_TIME_TICK);
		BroadcastReceiver startAfterForceStop = new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {
				updateAllWidgets(context);
			}
		};
		context.registerReceiver(startAfterForceStop, filter);
		AlarmService.setDailyAlarm(context);
		AlarmService.setReminders(context);
	}
}
