package com.rangi.calendar.alarm;

import org.joda.time.DateTime;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.rangi.calendar.MyDataTypes.Reminder;
import com.rangi.calendar.database.DatabaseHelper;
import com.rangi.calendar.R;

public class ReminderPopup extends Activity {

	public final static String POPUP_MESSAGE = "POPUP_MESSAGE";
	public final static String REMINDER_TIME = "REMINDER_TIME";
	public final static String REMINDER_ID = "REMINDER_ID";
	public final static String NOTIFICATION_ID = "NOTIFICATION_ID";

	private int notificationId = 0;
	private Reminder reminder;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// WindowManager.LayoutParams wlp = getWindow().getAttributes();
		// wlp.gravity = Gravity.BOTTOM;
		// getWindow().setAttributes(wlp);
		setContentView(R.layout.popup_reminder);
		String note = getIntent().getExtras().getString(POPUP_MESSAGE);
		String reminderTime = getIntent().getExtras().getString(REMINDER_TIME);
		long noteId = getIntent().getExtras().getLong(REMINDER_ID);
		DatabaseHelper dbHelper = DatabaseHelper
				.getDbHelper(getApplicationContext());
		reminder = dbHelper.getNoteReminder(noteId);
		dbHelper.close();

		notificationId = getIntent().getExtras().getInt(NOTIFICATION_ID);

		if (note != null)
			((TextView) findViewById(R.id.popup_message)).setText(note);
		if (reminderTime != null)
			((TextView) findViewById(R.id.popup_title_left))
					.setText(reminderTime);

	}

	public void close(View view) {
		if (notificationId != 0) {
			Context ctx = getApplicationContext();
			String ns = Context.NOTIFICATION_SERVICE;
			NotificationManager nMgr = (NotificationManager) ctx
					.getSystemService(ns);
			nMgr.cancel(notificationId);
		}
		finish();
	}

	public void remindLater(View view) {
		if (reminder != null) {
			DateTime dateTime = new DateTime(reminder.dateTime);
			dateTime = dateTime.plusMinutes(5);
			DatabaseHelper.getDbHelper(ReminderPopup.this).updateReminder(
					reminder.id, dateTime.toString());
			AlarmService.setReminder(ReminderPopup.this, reminder.id);
		}
		finish();
	}
}
