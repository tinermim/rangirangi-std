package com.rangi.calendar.alarm;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import com.rangi.calendar.R;
import com.rangi.calendar.DayView;
import com.rangi.calendar.MainActivity;
import com.rangi.calendar.SolarCalendar;
import com.rangi.calendar.MyDataTypes.Reminder;
import com.rangi.calendar.database.DatabaseHelper;
import com.rangi.calendar.note.NoteManager;
import org.joda.time.LocalDate;

public class AlarmReceiver extends BroadcastReceiver {

	public static final String SHOW_DAILY_NOTIF = "ACTION_SHOW_DAILY_NOTIF";
	public static final String SHOW_REMINDER_NOTIF = "ACTION_SHOW_REMINDER_NOTIF";

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		if (action.equals(SHOW_DAILY_NOTIF)) {
			showDailyNotification(context, intent);
		} else if (action.equals(SHOW_REMINDER_NOTIF)) {
			showReminderNotification(context, intent);
		}
	}

	private void showReminderNotification(Context context, Intent intent) {
		int alarmId = (int) intent.getLongExtra(AlarmService.ALARM_ID, -1);
		Reminder reminder = DatabaseHelper.getDbHelper(context).getReminder(alarmId);
		String alarmFor = reminder.isFor;
		int alarmType = reminder.alarmType;
		long alarmRefId = reminder.refId;

		Intent popupIntent = new Intent(context, ReminderPopup.class);
		popupIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_CLEAR_TOP);

		if (alarmFor.equals(Reminder.REMINDER_FOR_NOTE)) {

			String title = context.getResources().getString(
					R.string.notification_note_title);
			String text = NoteManager.getInstance(context).getNoteById(
					alarmRefId).note;

			Intent resultIntent = new Intent(context, DayView.class);
			resultIntent.setAction("" + alarmId);
			PendingIntent resultPendingIntent = PendingIntent.getActivity(
					context, alarmId, resultIntent, 0);

			NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
					context).setSmallIcon(R.drawable.ico_notification_note)
					.setContentTitle(title).setContentText(text)
					.setAutoCancel(true).setContentIntent(resultPendingIntent);

			if (alarmType == Reminder.NOTE_ALARM_NOTIF_SOUND)
				notificationBuilder.setSound(RingtoneManager
						.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

			((NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE)).notify(
					alarmId, notificationBuilder.build());

			popupIntent.putExtra(ReminderPopup.NOTIFICATION_ID, alarmId);
			popupIntent.putExtra(ReminderPopup.POPUP_MESSAGE, text);
			popupIntent.putExtra(ReminderPopup.REMINDER_ID, alarmRefId);
			context.startActivity(popupIntent);

			try {
				Uri notification = RingtoneManager
						.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
				Ringtone r = RingtoneManager.getRingtone(context, notification);
				r.play();
			} catch (Exception e) {
				System.out.println("AlarmReceiver.showReminderNotification()");
				e.printStackTrace();
			}

		} else if (alarmFor.equals(Reminder.REMINDER_FOR_BIRTHDAY)) {
			LocalDate gDate = new LocalDate().plusDays(Reminder
					.getDaysBeforeBirthday(alarmType));

			DatabaseHelper dbHelper = DatabaseHelper.getDbHelper(context);
			String title = "";
			switch (alarmType) {
			case Reminder.BIRTHDAY_ALARM_0:
				title = context.getResources().getString(
						R.string.notification_birthday_today);
				break;
			case Reminder.BIRTHDAY_ALARM_3:
				title = context.getResources().getString(
						R.string.notification_birthday_3days_later);
				break;
			}
			
			String text = dbHelper.getNameForBirthdayReminder(alarmId);

			Intent resultIntent = new Intent(context, DayView.class);
			resultIntent.setAction("" + alarmId);
			resultIntent.putExtra(MainActivity.YEAR, gDate.getYear());
			resultIntent.putExtra(MainActivity.MONTH, gDate.getMonthOfYear());
			resultIntent.putExtra(MainActivity.DAY, gDate.getDayOfMonth());
			PendingIntent resultPendingIntent = PendingIntent.getActivity(
					context, alarmId, resultIntent, 0);

			NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
					context).setSmallIcon(R.drawable.ico_notification_birthday)
					.setContentTitle(title).setContentText(text)
					.setAutoCancel(true).setContentIntent(resultPendingIntent);

			((NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE)).notify(
					alarmId, notificationBuilder.build());

			popupIntent.putExtra(ReminderPopup.NOTIFICATION_ID, alarmId);
			popupIntent.putExtra(ReminderPopup.POPUP_MESSAGE, text);
			context.startActivity(popupIntent);
		}
	}

	private void showDailyNotification(Context context, Intent intent) {
		SolarCalendar now = new SolarCalendar();

		DatabaseHelper dbHelper = DatabaseHelper.getDbHelper(context);
		String eventTitle = dbHelper.getEventTitle(now.date, now.month);
		String eventDescription = dbHelper.getEventDescription(now.date,
				now.month);
		dbHelper.close();

		int notifId = intent.getIntExtra(AlarmService.ALARM_ID, -1);

		/*
		 * The pending indent that will be used when user clicks on
		 * notification.
		 */
		Intent resultIntent = new Intent(context, DayView.class);
		resultIntent.setAction("" + notifId);
		PendingIntent resultPendingIntent = PendingIntent.getActivity(context,
				-2, resultIntent, 0);

		Notification notif = new NotificationCompat.Builder(context)
				.setSmallIcon(R.drawable.ico_notification_event)
				.setContentTitle(eventTitle).setContentText(eventDescription)
				.setAutoCancel(true).setContentIntent(resultPendingIntent)
				.build();

		((NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE)).notify(
				notifId, notif);
	}

}
