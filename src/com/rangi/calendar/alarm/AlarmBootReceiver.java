package com.rangi.calendar.alarm;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.rangi.calendar.widget.Widget1x1;
import com.rangi.calendar.widget.Widget4x1;
import com.rangi.calendar.widget.Widget4x2;

public class AlarmBootReceiver extends BroadcastReceiver {
	PendingIntent service;

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
			AlarmService.setDailyAlarm(context);
			AlarmService.setReminders(context);
			context.sendBroadcast(new Intent(Widget1x1.UPDATE_ACTION).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
			context.sendBroadcast(new Intent(Widget4x1.UPDATE_ACTION).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
			context.sendBroadcast(new Intent(Widget4x2.UPDATE_ACTION).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
		}
	}
}