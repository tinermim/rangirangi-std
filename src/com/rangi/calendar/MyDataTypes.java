package com.rangi.calendar;

public class MyDataTypes {

	public static class Note {
		
		public long id;
		public int year;
		public int month;
		public int day;
		public String note;

		public Note(long id, int year, int month, int day,
				String note) {
			this.id = id;
			this.year = year;
			this.month = month;
			this.day = day;
			this.note = note;
		}

	}

	public static class Birthday {
		public int id;
		public int month;
		public int day;
		public String name;
		public String photoUri;

		public Birthday(int id, int month, int day, String name, String photoUri) {
			this.id = id;
			this.month = month;
			this.day = day;
			this.name = name;
			this.photoUri = photoUri;
		}
	}

	public static class Reminder {

		public static final String REMINDER_FOR_NOTE = "note";
		public static final String REMINDER_FOR_BIRTHDAY = "birthday";

		public static final int NOTE_ALARM_NOTIF = 0;
		public static final int NOTE_ALARM_NOTIF_SOUND = 1;

		public static final int BIRTHDAY_ALARM_0 = 0; // alarm on the day
		public static final int BIRTHDAY_ALARM_3 = 1; // alarm 3 days before

		private static int daysBeforeBirthday[] = { 0, 3 };

		public long id;
		public long refId;
		public String dateTime;
		public String isFor;
		public int alarmType;

		/*
		 * Called when we're retrieving a Reminder from Database.
		 */
		public Reminder(long id, long refId, String dateTime, String isFor, int alarmType) {
			this.id = id;
			this.refId = refId;
			this.dateTime = dateTime;
			this.isFor = isFor;
			this.alarmType = alarmType;
		}

		public int getDaysBeforeBirthday() {
			return daysBeforeBirthday[alarmType];
		}

		public static int getDaysBeforeBirthday(int alarmType) {
			return daysBeforeBirthday[alarmType];
		}
	}

}
