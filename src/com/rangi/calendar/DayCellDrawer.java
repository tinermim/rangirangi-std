package com.rangi.calendar;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.view.View;

import com.rangi.calendar.settings.SettingsUtil;
import com.rangi.calendar.utils.Utils;

public class DayCellDrawer extends View {

	private static final int TODAY_TEXT_COLOR = Color.parseColor("#09b89d");
	private static final int HOLIDAY_CIRCLE_COLOR = Color
			.parseColor("#1f000000");

	private final Paint paint = new Paint();
	private Context context;
	private int width;
	private String mainDay;
	private String topLeftDay;
	private String topRightDay;
	private boolean isToday;
	private boolean isHoliday;
	private boolean isJalali;
	private boolean haveNote;
	private int circleMargin;



	public DayCellDrawer(Context context, int width, String mainDay,
			String topLeftDay, String topRightDay, int color, boolean isToday,
			boolean isHoliday, boolean isJalali, boolean haveNote) {
		super(context);

		//Log.i("DayCellDrawer" , "Created");
		this.context = context;
		this.width = width;
		this.mainDay = mainDay;
		this.topLeftDay = topLeftDay;
		this.topRightDay = topRightDay;
		this.isToday = isToday;
		this.isHoliday = isHoliday;
		this.isJalali = isJalali;
		this.haveNote = haveNote;
		circleMargin = width / 6;

		if (!isToday)
			this.setBackgroundColor(color);
		else
			this.setBackgroundColor(Color.WHITE);

		paint.setAntiAlias(true);
		paint.setTextAlign(Paint.Align.CENTER);
	}

	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		if (isHoliday)
			drawHolidayCircle(canvas);

		if (!isToday) {
			paint.setColor(Color.WHITE);
		} else {
			paint.setColor(TODAY_TEXT_COLOR);
		}

		paint.setTextSize(width / 2);
		if (isJalali) {
			paint.setTypeface(Utils.getCustomFont(context));
			canvas.drawText(mainDay, width / 2, 5 * width / 8, paint);
		} else {
			paint.setTypeface(null);
			canvas.drawText(mainDay, width / 2, 5.5f * width / 8, paint);
		}

		if (isJalali)
			paint.setTypeface(null);
		else
			paint.setTypeface(Utils.getCustomFont(context));
		paint.setTextSize(width / 6);
		
		// condition to show the purchased items
		// if not purchased we won't show other dates(gergorian and hijri) on the cells of Month View(bottom left and bottom right of 
		// the cell)
		if(PurchaseControl.getInstance(context).isPurchased(PurchaseComponent.PURCHASE_GEROGRIAN_HIJRI_CALENDAR) ){
			canvas.drawText(topRightDay, width / 8, width - width / 8.5f, paint);

		if (SettingsUtil.showIslamicCalendar(context)) {
			paint.setTypeface(Utils.getCustomArabicFont(context));
			paint.setTextSize(width / 4);
			canvas.drawText(topLeftDay, width - width / 6, width - width / 10,
					paint);
		}
		if (haveNote)
			drawHaveNoteBackground(canvas);
		}
	}

	private void drawHaveNoteBackground(Canvas canvas) {
		Resources res = getResources();
		Bitmap bitmap = BitmapFactory.decodeResource(res,
				R.drawable.shape_have_note);
		canvas.drawBitmap(bitmap, 0, 0, paint);
	}

	private void drawHolidayCircle(Canvas canvas) {
		paint.setColor(HOLIDAY_CIRCLE_COLOR);
		paint.setStyle(Style.FILL);
		canvas.drawOval(new RectF(circleMargin, circleMargin, width
				- circleMargin, width - circleMargin), paint);
	}

}
