/*
 * File: WeekScrollerFragment.java
 * -------------------------------
 * Creates a fragment of week scroller in day view which
 * is a quick view of the week.
 */

package com.rangi.calendar;

import org.joda.time.LocalDate;

import com.rangi.calendar.customviews.MyTextView;
import com.rangi.calendar.utils.Utils;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class WeekScrollerFragment extends Fragment {

	public static final String OFFSET = "offset";
	public static final String PRIMARY_IS_JALALI = "primary is jalali";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		LinearLayout row = new LinearLayout(getActivity());

		Bundle args = getArguments();
		int offset = args.getInt(OFFSET);
		boolean primaryIsJalali = args.getBoolean(PRIMARY_IS_JALALI);

		if (primaryIsJalali) {
			SolarCalendar weekDay = SolarCalendar.offsetWeek(offset)
					.withFirstDayOfWeek().addDays(6);
			for (int i = 0; i < 7; i++) {
				dayCell day = new dayCell(weekDay);
				day.setLayoutParams(new LayoutParams(0,
						LayoutParams.MATCH_PARENT, 1));
				row.addView(day);
				if (i != 6)
					addSeparator(row);
				weekDay = weekDay.addDays(-1);
			}
		} else {
			LocalDate weekDay = new LocalDate().plusWeeks(offset)
					.withDayOfWeek(7);
			for (int i = 0; i < 7; i++) {
				dayCell day = new dayCell(weekDay);
				day.setLayoutParams(new LayoutParams(0,
						LayoutParams.MATCH_PARENT, 1));
				row.addView(day);
				if (i != 6)
					addSeparator(row);
				weekDay = weekDay.minusDays(1);
			}
		}

		return row;
	}

	private void addSeparator(LinearLayout row) {
		int screenWidth = Utils.getScreenWidth(getActivity());
		View separator = new View(getActivity());
		LayoutParams params = new LayoutParams(1, LayoutParams.MATCH_PARENT);
		params.setMargins(0, screenWidth / 54, 0, screenWidth / 54);
		separator.setLayoutParams(params);
		separator.setBackgroundColor(Color.LTGRAY);
		row.addView(separator);
	}

	private class dayCell extends LinearLayout {

		public dayCell(final SolarCalendar date) {
			super(getActivity());

			int screenWidth = Utils.getScreenWidth(getActivity());

			this.setOrientation(LinearLayout.VERTICAL);
			this.setBackgroundResource(R.drawable.selector_transparent);

			final MyTextView weekDay = new MyTextView(getActivity());
			final MyTextView dayNumber = new MyTextView(getActivity());

			weekDay.setGravity(Gravity.CENTER);
			dayNumber.setGravity(Gravity.CENTER);

			if (DayView.dayView.myViewPager.getCurrentItem() == (int) (DayView.MIDDLE_PAGE - SolarCalendar
					.calculateDayOffset(date))) {
				weekDay.setTextColor(Color.BLACK);
				dayNumber.setTextColor(Color.BLACK);
			} else {
				weekDay.setTextColor(Color.DKGRAY);
				dayNumber.setTextColor(Color.LTGRAY);
			}

			weekDay.setTextSize(TypedValue.COMPLEX_UNIT_DIP,
					Utils.pixelsToDip(getActivity(), screenWidth / 30));
			dayNumber.setTextSize(TypedValue.COMPLEX_UNIT_DIP,
					Utils.pixelsToDip(getActivity(), screenWidth / 25));

			weekDay.setText("" + date.strWeekDay.charAt(0));
			dayNumber.setText("" + date.date);

			LayoutParams params = new LayoutParams(screenWidth / 18,
					screenWidth / 18);
			params.gravity = Gravity.CENTER;
			dayNumber.setLayoutParams(params);
			if (Cache.isHoliday(getActivity(), date))
				dayNumber.setBackgroundResource(R.drawable.circle_gray_filled);

			this.addView(weekDay);
			this.addView(dayNumber);

			this.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					DayView.dayView.myViewPager
							.setCurrentItem((int) (DayView.MIDDLE_PAGE - SolarCalendar
									.calculateDayOffset(date)));
				}
			});
		}

		public dayCell(final LocalDate date) {
			super(getActivity());

			int screenWidth = Utils.getScreenWidth(getActivity());

			this.setOrientation(LinearLayout.VERTICAL);
			this.setBackgroundResource(R.drawable.selector_transparent);

			final MyTextView weekDay = new MyTextView(getActivity());
			final TextView dayNumber = new TextView(getActivity());

			weekDay.setGravity(Gravity.CENTER);
			dayNumber.setGravity(Gravity.CENTER);

			if (DayView.dayView.myViewPager.getCurrentItem() == (int) (DayView.MIDDLE_PAGE - SolarCalendar
					.calculateDayOffset(date))) {
				weekDay.setTextColor(Color.BLACK);
				dayNumber.setTextColor(Color.BLACK);
			} else {
				weekDay.setTextColor(Color.DKGRAY);
				dayNumber.setTextColor(Color.LTGRAY);
			}

			weekDay.setTextSize(TypedValue.COMPLEX_UNIT_DIP,
					Utils.pixelsToDip(getActivity(), screenWidth / 30));
			dayNumber.setTextSize(TypedValue.COMPLEX_UNIT_DIP,
					Utils.pixelsToDip(getActivity(), screenWidth / 25));

			weekDay.setText("" + new SolarCalendar(date).strWeekDay.charAt(0));
			dayNumber.setText("" + date.getDayOfMonth());

			LayoutParams params = new LayoutParams(screenWidth / 18,
					screenWidth / 18);
			params.gravity = Gravity.CENTER;
			dayNumber.setLayoutParams(params);
			if (Cache.isHoliday(getActivity(), date))
				dayNumber.setBackgroundResource(R.drawable.circle_gray_filled);

			this.addView(weekDay);
			this.addView(dayNumber);

			this.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					DayView.dayView.myViewPager
							.setCurrentItem((int) (DayView.MIDDLE_PAGE - SolarCalendar
									.calculateDayOffset(date)));
				}
			});
		}
	}

}