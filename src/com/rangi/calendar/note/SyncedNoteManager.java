package com.rangi.calendar.note;

import com.rangi.calendar.SolarCalendar;
import com.rangi.calendar.MyDataTypes.Note;
import com.rangi.calendar.settings.SettingsUtil;
import com.rangi.calendar.sync.CalendarTools;
import com.rangi.calendar.sync.MyEvent;
import com.rangi.calendar.utils.Utils;

import android.content.Context;
import java.util.ArrayList;
import org.joda.time.LocalDate;

public class SyncedNoteManager extends NoteManager {

	private static NoteManager sInstance;

	public static NoteManager getInstance(Context context) {
		if (sInstance == null)
			sInstance = new SyncedNoteManager(context);
		return sInstance;
	}

	private SyncedNoteManager(Context context) {
		super(context);
	}

	@Override
	public Note[] getNotes(int year, int month, int day) {
		MyEvent[] events = CalendarTools.getEventsOfDay(mContext,
				getCalendarId(),
				SolarCalendar.jalaliToGregorian(year, month - 1, day));
		Note[] notes = new Note[events.length];
		for (int i = 0; i < events.length; i++) {
			notes[i] = extractNoteFromEvent(events[i]);
		}
		return notes;
	}

	@Override
	public Note getNoteById(long id) {
		return extractNoteFromEvent(CalendarTools.getEvent(mContext, id));
	}

	@Override
	public ArrayList<Note> getAllNotes() {
		ArrayList<Note> notes = new ArrayList<Note>();
		for (MyEvent event : CalendarTools.getEvents(mContext, getCalendarId())) {
			notes.add(extractNoteFromEvent(event));
		}
		return notes;
	}

	@Override
	public Note insertNote(int year, int month, int day, String note) {
		long id = CalendarTools.addEvent(mContext, getCalendarId(), note,
				getTimeInMillis(year, month, day));

		if (withNoteDates == null)
			initWithNoteDatesSet(mContext);
		withNoteDates.add(Utils.getDateAsInteger(year, month, day));

		return new Note(id, year, month, day, note);
	}

	@Override
	public void updateNote(long id, int year, int month, int day, String note) {
		CalendarTools.updateEvent(mContext, id, note,
				getTimeInMillis(year, month, day));
	}

	@Override
	protected void deleteNote(long id) {
		CalendarTools.deleteEvent(mContext, id);
	}

	private long getCalendarId() {
		return SettingsUtil.getCalendarId(mContext);
	}

	private static long getTimeInMillis(int year, int month, int day) {
		return SolarCalendar.jalaliToGregorian(year, month - 1, day)
				.toDateTimeAtCurrentTime().getMillis();
	}

	private static Note extractNoteFromEvent(MyEvent event) {
		SolarCalendar date = new SolarCalendar(new LocalDate(
				event.getStartTime()));
		return new Note(event.getEventId(), date.year, date.month, date.date,
				event.getTitle());
	}

}
