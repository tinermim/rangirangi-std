package com.rangi.calendar.note;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.rangi.calendar.DatePickerDialog;
import com.rangi.calendar.DayView;
import com.rangi.calendar.MainActivity;
import com.rangi.calendar.MyDataTypes.Note;
import com.rangi.calendar.MyDataTypes.Reminder;
import com.rangi.calendar.R;
import com.rangi.calendar.SimpleActionBarActivity;
import com.rangi.calendar.SolarCalendar;
import com.rangi.calendar.alarm.AlarmService;
import com.rangi.calendar.database.DatabaseHelper;
import com.rangi.calendar.settings.SettingsUtil;
import com.rangi.calendar.sync.CalendarTools;
import com.rangi.calendar.sync.MyCalendar;
import com.rangi.calendar.widget.Widget4x2;

public class NoteEditor extends SimpleActionBarActivity {

	public static final String MODE_EDIT = "MODE_EDIT";
	public static final String MODE_ADD = "MODE_ADD";
	public static final String NOTE_ID = "NOTE_ID";

	private String mode;

	private int gYear;
	private int gMonth;
	private int gDay;

	private int year;
	private int month;
	private int day;

	private Note noteData;
	private String initialNote = "";
	private EditText notePad;
	private Spinner spReminder;

	DateTime reminderDateTime;

	private boolean isSaved;
	protected Reminder reminder;

	private CheckBox mReminderCheckBox;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_note_editor);
		setupActionBar(R.layout.action_bar_note_editor);
		
		Intent intent = getIntent();
		mode = intent.getAction();
		int offset = intent.getIntExtra(MainActivity.DAY_OFFSET, 0);
		LocalDate gDate = new LocalDate().plusDays(offset);
		long noteID = intent.getLongExtra(NOTE_ID, -1);
		if (noteID != -1) {
			noteData = NoteManager.getInstance(this).getNoteById(noteID);
			gDate = new LocalDate(SolarCalendar.jalaliToGregorian(
					noteData.year, noteData.month - 1, noteData.day));
		}
		notePad = (EditText) findViewById(R.id.notePad);

		mReminderCheckBox = (CheckBox) findViewById(R.id.note_edtior_reminder_chkbx);

		init(gDate);
		if (noteData != null)
			updateNoteEditorViews(getApplicationContext(), noteData.id);
	}

	private void init(LocalDate gDate) {
		SolarCalendar date = new SolarCalendar(gDate);

		gYear = gDate.getYear();
		gMonth = gDate.getMonthOfYear();
		gDay = gDate.getDayOfMonth();

		year = date.year;
		month = date.month;
		day = date.date;

		((TextView) findViewById(R.id.note_edtior_date)).setText(date
				.toString());

		spReminder = (Spinner) findViewById(R.id.note_edtior_reminder);
		TextView tvTime = (TextView) findViewById(R.id.note_edtior_time);

		reminder = null;
		initialNote = "";

		if (noteData != null) {
			reminder = DatabaseHelper.getDbHelper(this).getNoteReminder(
					noteData.id);
			if (reminder != null && reminder.dateTime != null) {
				reminderDateTime = new DateTime(reminder.dateTime);
			}

			initialNote = noteData.note;

			isSaved = true;
		} else {
			isSaved = false;
		}

		notePad.setText(initialNote);

		if (reminder == null || reminder.dateTime == null) {
			mReminderCheckBox.setChecked(false);
			findViewById(R.id.note_editor_reminder_linearlayout).setVisibility(
					View.GONE);
			LocalTime time = new LocalTime();
			tvTime.setText(time.getHourOfDay() + ":" + time.getMinuteOfHour());
		} else {
			mReminderCheckBox.setChecked(true);
			findViewById(R.id.note_editor_reminder_linearlayout).setVisibility(
					View.VISIBLE);
			DateTime time = new DateTime(reminder.dateTime);
			tvTime.setText(time.getHourOfDay() + ":" + time.getMinuteOfHour());
		}

		// refreshReminders();
		String[] remindersTime = getResources().getStringArray(
				R.array.reminder_times);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.note_editor_reminder_spinner_item_fa, remindersTime);
		adapter.setDropDownViewResource(R.layout.note_editor_reminder_spinner_item_fa);
		spReminder.setAdapter(adapter);
		refreshReminderButton();
	}

	public void saveNote(View view) {
		if (saveNote()) {
			Toast.makeText(NoteEditor.this, R.string.note_editor_saved,
					Toast.LENGTH_SHORT).show();

			// Sending broadcast to widgets that show note.
			sendBroadcast(new Intent(Widget4x2.UPDATE_ACTION));

			backToDayView();
		} else {
			Toast.makeText(NoteEditor.this,
					R.string.note_editor_nothing_to_save, Toast.LENGTH_SHORT)
					.show();
		}
	}

	private boolean saveNote() {// TODO
		String note = ((EditText) findViewById(R.id.notePad)).getText()
				.toString();
		// Note string is empty. Don't save it
		if ("".equals(note.trim()))
			return false;

		if (noteData == null)
			noteData = NoteManager.getInstance(this).insertNote(year, month,
					day, note);
		else
			NoteManager.getInstance(this).updateNote(noteData.id, year, month,
					day, note);

		TextView tvTime = (TextView) findViewById(R.id.note_edtior_time);
		String reminderTime = tvTime.getText().toString();
		if (mReminderCheckBox.isChecked()) {
			try {
				if (reminderDateTime == null)
					reminderDateTime = new DateTime(gYear, gMonth, gDay,
							Integer.parseInt(reminderTime.split(":")[0]),
							Integer.parseInt(reminderTime.split(":")[1]));
			} catch (Exception e) {
				System.out.println(e.toString());
			}
			changeTimeOfReminder(reminderTime);
			try {
				reminder = DatabaseHelper.getDbHelper(getApplicationContext())
						.getNoteReminder(noteData.id);
				if (reminder == null) {
					System.out.println("reminder is null");
					reminder = new Reminder(-1, noteData.id,
							reminderDateTime.toString(),
							Reminder.REMINDER_FOR_NOTE,
							Reminder.NOTE_ALARM_NOTIF);
					reminder.id = DatabaseHelper.getDbHelper(NoteEditor.this)
							.addReminder(noteData.id, reminder);
				} else {
					System.out.println("reminder is not null");
					if (reminder.dateTime != null) {
						System.out.println("reminder.dateTime is not null");
						DatabaseHelper.getDbHelper(NoteEditor.this)
								.updateReminder(reminder.id,
										reminderDateTime.toString());
					}
				}
				AlarmService.setReminder(NoteEditor.this, reminder.id);
			} catch (Exception e) {
				System.out.println(e.toString());
			}
		} else {
			try {
				List<Reminder> reminders = DatabaseHelper.getDbHelper(this)
						.getNoteReminders(noteData.id);
				for (Reminder reminder : reminders) {
					NoteManager.getInstance(getApplicationContext())
							.deleteReminder(reminder.id);
				}
			} catch (Exception e) {
				System.out.println(e.toString());
			}
		}

		// Note has beed successfully saved.
		return true;
	}

	public void deleteNote(View view) {
		if (isSaved)
			new AlertDialog.Builder(this)
					.setTitle(R.string.note_editor_delete_title)
					.setMessage(R.string.note_editor_delete_message)
					.setPositiveButton(R.string.note_editor_delete_ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									deleteNote();
									backToDayView();
								}
							})
					.setNegativeButton(R.string.note_editor_delete_cancel,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									// do nothing
								}
							}).show();
		else
			Toast.makeText(this, R.string.note_editor_nothing_to_delete,
					Toast.LENGTH_SHORT).show();
	}

	private void deleteNote() {
		NoteManager.getInstance(this).deleteNoteAndItsReminders(noteData.id);
	}

	public void changeDate(View view) {
		final TextView tv = (TextView) view;
		final DatePickerDialog dialog = new DatePickerDialog(this, year,
				month - 1, day);
		dialog.setOnOkClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// if (mode != null && mode.equals(MODE_EDIT))
				updateNoteDate(SolarCalendar.jalaliToGregorian(
						dialog.getYear(), dialog.getMonth(), dialog.getDay()));

				// init(SolarCalendar.jalaliToGregorian(dialog.getYear(),
				// dialog.getMonth(), dialog.getDay()));
				dialog.dismiss();
				LocalDate ld = SolarCalendar.jalaliToGregorian(
						dialog.getYear(), dialog.getMonth(), dialog.getDay());
				SolarCalendar date = new SolarCalendar(ld);
				tv.setText(date.toString());
			}
		});
		dialog.show();
	}

	protected void updateNoteDate(LocalDate gDate) {
		SolarCalendar date = new SolarCalendar(gDate);

		gYear = gDate.getYear();
		gMonth = gDate.getMonthOfYear();
		gDay = gDate.getDayOfMonth();

		year = date.year;
		month = date.month;
		day = date.date;

		if (noteData != null)
			NoteManager.getInstance(this).updateNote(noteData.id, year, month,
					day, noteData.note);
		if (reminder != null) {
			DateTime alarmDateTime = new DateTime(reminder.dateTime);
			alarmDateTime = alarmDateTime.withYear(gYear)
					.withMonthOfYear(gMonth).withDayOfMonth(gDay);
			DatabaseHelper.getDbHelper(this).updateReminder(reminder.id,
					alarmDateTime.toString());
			AlarmService.resetReminder(this, reminder.id);
		}
	}

	public void addOrEditReminder(View view) {
		if (reminder == null)
			addReminder();
		else
			editReminder();
	}

	private void addReminder() {
		LocalTime time = new LocalTime();
		final TimePicker timePicker = new TimePicker(this);
		timePicker.setIs24HourView(false);
		timePicker.setCurrentHour(time.getHourOfDay());
		timePicker
				.setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
		timePicker.setCurrentMinute(time.getMinuteOfHour());

		new AlertDialog.Builder(this)
				.setPositiveButton(android.R.string.ok,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								reminderDateTime = new DateTime(gYear, gMonth,
										gDay, timePicker.getCurrentHour(),
										timePicker.getCurrentMinute());
								refreshReminderButton();
							}

						})
				.setNegativeButton(android.R.string.cancel,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
							}
						}).setView(timePicker).show();
	}

	private void editReminder() {
		reminderDateTime = new DateTime(reminder.dateTime);
		final TimePicker timePicker = new TimePicker(this);
		timePicker.setIs24HourView(false);
		timePicker.setCurrentHour(reminderDateTime.getHourOfDay());
		timePicker
				.setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
		timePicker.setCurrentMinute(reminderDateTime.getMinuteOfHour());

		new AlertDialog.Builder(this)
				.setPositiveButton(android.R.string.ok,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								reminderDateTime = new DateTime(gYear, gMonth,
										gDay, timePicker.getCurrentHour(),
										timePicker.getCurrentMinute());
								DatabaseHelper.getDbHelper(NoteEditor.this)
										.updateReminder(reminder.id,
												reminderDateTime.toString());
								reminder.dateTime = reminderDateTime.toString();
								refreshReminderButton();
							}

						})
				.setNegativeButton(android.R.string.cancel,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
							}
						}).setView(timePicker).show();
	}

	public void deleteReminder(View view) {
		NoteManager.getInstance(this).deleteReminder(reminder.id);
		reminder = null;
		refreshReminderButton();
	}

	public void setReminder(View view) {
		CheckBox chb = (CheckBox) view;
		if (!chb.isChecked()) {
			spReminder.setEnabled(false);
			spReminder.setBackgroundResource(R.drawable.spinner_noteeditor);
			spReminder.setSelection(0);
			findViewById(R.id.note_editor_reminder_linearlayout).setVisibility(
					View.GONE);
		} else {
			findViewById(R.id.note_edtior_reminder).setEnabled(true);
			findViewById(R.id.note_edtior_reminder).setBackgroundResource(
					R.drawable.selector_spinner);
			findViewById(R.id.note_editor_reminder_linearlayout).setVisibility(
					View.VISIBLE);
		}
	}

	protected void refreshReminderButton() {
		if (reminderDateTime == null) {
			reminderDateTime = new DateTime();
		}
		((TextView) findViewById(R.id.note_edtior_time))
				.setText(reminderDateTime.toString("HH:mm"));

	}

	@Override
	public void onBackPressed() {
		if (notePad.getText().toString().equals(initialNote)) {
			backToDayView();
		} else
			new AlertDialog.Builder(this)
					.setTitle(R.string.note_editor_close_title)
					.setMessage(R.string.note_editor_close_message)
					.setPositiveButton(R.string.note_editor_close_ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									saveNote(null);
								}
							})
					.setNegativeButton(R.string.note_editor_close_cancel,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									backToDayView();
								}
							}).show();

	}

	private void backToDayView() {
		Intent intent = new Intent(this, DayView.class);
		intent.putExtra(MainActivity.YEAR, gYear);
		intent.putExtra(MainActivity.MONTH, gMonth);
		intent.putExtra(MainActivity.DAY, gDay);
		finish();
		startActivity(intent);
	}

	private void updateNoteEditorViews(Context context, long id) {
		Reminder reminder = DatabaseHelper.getDbHelper(context)
				.getNoteReminder(id);
		if (reminder != null) {
			DateTime dateTime = new DateTime(reminder.dateTime);
			TextView tvTime = (TextView) findViewById(R.id.note_edtior_time);
			tvTime.setText(dateTime.getHourOfDay() + ":"
					+ dateTime.getMinuteOfHour());
			spReminder.setSelection(0);
		}
	}

	private void changeTimeOfReminder(String reminderTime) {
		int selectedTime = spReminder.getSelectedItemPosition();
		switch (selectedTime) {
		case 1:
			reminderDateTime = new DateTime(gYear, gMonth, gDay,
					Integer.parseInt(reminderTime.split(":")[0]),
					Integer.parseInt(reminderTime.split(":")[1]));
			reminderDateTime = reminderDateTime.minusMinutes(1);
			break;
		case 2:
			reminderDateTime = new DateTime(gYear, gMonth, gDay,
					Integer.parseInt(reminderTime.split(":")[0]),
					Integer.parseInt(reminderTime.split(":")[1]));
			reminderDateTime = reminderDateTime.minusMinutes(5);
			break;
		case 3:
			reminderDateTime = new DateTime(gYear, gMonth, gDay,
					Integer.parseInt(reminderTime.split(":")[0]),
					Integer.parseInt(reminderTime.split(":")[1]));
			reminderDateTime = reminderDateTime.minusMinutes(10);
			break;
		case 4:
			reminderDateTime = new DateTime(gYear, gMonth, gDay,
					Integer.parseInt(reminderTime.split(":")[0]),
					Integer.parseInt(reminderTime.split(":")[1]));
			reminderDateTime = reminderDateTime.minusMinutes(15);
			break;
		case 5:
			reminderDateTime = new DateTime(gYear, gMonth, gDay,
					Integer.parseInt(reminderTime.split(":")[0]),
					Integer.parseInt(reminderTime.split(":")[1]));
			reminderDateTime = reminderDateTime.minusMinutes(30);
			break;
		case 6:
			reminderDateTime = new DateTime(gYear, gMonth, gDay,
					Integer.parseInt(reminderTime.split(":")[0]),
					Integer.parseInt(reminderTime.split(":")[1]));
			reminderDateTime = reminderDateTime.minusHours(1);
			break;
		default:
			break;
		}
		// TODO
	}

	public long getSyncCalId(Context context) {
		ArrayList<MyCalendar> cals = CalendarTools.getCalendars(context);
		SharedPreferences prefs = getSharedPreferences(
				"com.rangi.calendar", Context.MODE_PRIVATE);
		for (int i = 0; i < cals.size(); i++) {
			boolean restoredText = prefs.getBoolean("check_"
					+ cals.get(i).getCalName(), false);
			if (restoredText) {
				return cals.get(i).getCalId();
			}
		}
		return SettingsUtil.VALUE_NO_CALENDAR;
	}

}
