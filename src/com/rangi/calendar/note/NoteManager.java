package com.rangi.calendar.note;

import android.content.Context;

import com.rangi.calendar.SolarCalendar;
import com.rangi.calendar.MyDataTypes.Note;
import com.rangi.calendar.MyDataTypes.Reminder;
import com.rangi.calendar.alarm.AlarmService;
import com.rangi.calendar.database.DatabaseHelper;
import com.rangi.calendar.settings.SettingsUtil;
import com.rangi.calendar.utils.Utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import org.joda.time.LocalDate;

public abstract class NoteManager {

	public static NoteManager getInstance(Context context) {
		if (SettingsUtil.getCalendarId(context) == SettingsUtil.VALUE_NO_CALENDAR)
			return LocalNoteManager.getInstance(context);
		else
			return SyncedNoteManager.getInstance(context);
	}

	protected Context mContext;

	public NoteManager(Context context) {
		mContext = context.getApplicationContext();
	}

	public abstract Note[] getNotes(int year, int month, int day);

	public abstract Note getNoteById(long id);

	public abstract ArrayList<Note> getAllNotes();

	public Note insertNote(Note note) {
		return insertNote(note.year, note.month, note.day, note.note);
	}

	public abstract Note insertNote(int year, int month, int day, String note);

	public abstract void updateNote(long id, int year, int month, int day,
			String note);

	public void deleteNoteAndItsReminders(long id) {
		deleteNote(id);

		List<Reminder> reminders = DatabaseHelper.getDbHelper(mContext)
				.getNoteReminders(id);
		for (Reminder reminder : reminders) {
			deleteReminder(reminder.id);
		}
	}

	protected abstract void deleteNote(long id);

	public void deleteReminder(long id) {
		DatabaseHelper.getDbHelper(mContext).deleteAlarm(id);
		AlarmService.cancelReminder(mContext, id);
	}

	protected static HashSet<Integer> withNoteDates;

	public static boolean hasNote(Context context, SolarCalendar date) {
		if (withNoteDates == null) {
			initWithNoteDatesSet(context);
		}
		return withNoteDates.contains(Utils.getDateAsInteger(date));
	}

	public static boolean hasNote(Context context, LocalDate date) {
		return hasNote(context, new SolarCalendar(date));
	}

	protected static void initWithNoteDatesSet(Context context) {
		withNoteDates = new HashSet<Integer>();

		for (Note note : getInstance(context).getAllNotes()) {
			withNoteDates.add(Utils.getDateAsInteger(note));
		}
	}

	public static void invalidateWithNoteDatesSet(Context context) {
		initWithNoteDatesSet(context);
	}

}
