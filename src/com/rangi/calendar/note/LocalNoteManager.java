package com.rangi.calendar.note;

import com.rangi.calendar.MyDataTypes.Note;
import com.rangi.calendar.database.DatabaseHelper;
import com.rangi.calendar.utils.Utils;


import android.database.sqlite.SQLiteDatabase;

import android.content.ContentValues;

import android.database.Cursor;
import java.util.ArrayList;

import android.content.Context;

public class LocalNoteManager extends NoteManager {

	private static final String NOTE_TABLE = DatabaseHelper.NOTE_TABLE;
	private static NoteManager sInstance;

	public static NoteManager getInstance(Context context) {
		if (sInstance == null)
			sInstance = new LocalNoteManager(context);
		return sInstance;
	}

	private LocalNoteManager(Context context) {
		super(context);
	}

	private SQLiteDatabase getDb() {
		return DatabaseHelper.getDbHelper(mContext).getDb();
	}

	@Override
	public Note[] getNotes(int year, int month, int day) {
		Cursor cursor = getDb().rawQuery(
				"SELECT * FROM " + NOTE_TABLE + " WHERE year=" + year
						+ " and month=" + month + " and day=" + day, null);
		Note[] notes = new Note[cursor.getCount()];
		int i = 0;
		while (cursor.moveToNext()) {
			notes[i] = new Note(cursor.getInt(cursor.getColumnIndex("_id")),
					cursor.getInt(cursor.getColumnIndex("year")),
					cursor.getInt(cursor.getColumnIndex("month")),
					cursor.getInt(cursor.getColumnIndex("day")),
					cursor.getString(cursor.getColumnIndex("note")));
			i++;
		}
		cursor.close();
		return notes;
	}

	@Override
	public Note getNoteById(long id) {
		Cursor cursor = getDb().rawQuery(
				"SELECT * FROM " + NOTE_TABLE + " WHERE _id=" + id, null);
		Note note = null;
		if (cursor.moveToNext())
			note = new Note(cursor.getInt(cursor.getColumnIndex("_id")),
					cursor.getInt(cursor.getColumnIndex("year")),
					cursor.getInt(cursor.getColumnIndex("month")),
					cursor.getInt(cursor.getColumnIndex("day")),
					cursor.getString(cursor.getColumnIndex("note")));
		cursor.close();
		return note;
	}

	@Override
	public ArrayList<Note> getAllNotes() {
		ArrayList<Note> list = new ArrayList<Note>();
		Cursor cursor = getDb().rawQuery(
				"SELECT * FROM " + NOTE_TABLE + " ORDER BY year, month, day",
				null);
		while (cursor.moveToNext()) {
			Note note = new Note(cursor.getInt(cursor.getColumnIndex("_id")),
					cursor.getInt(cursor.getColumnIndex("year")),
					cursor.getInt(cursor.getColumnIndex("month")),
					cursor.getInt(cursor.getColumnIndex("day")),
					cursor.getString(cursor.getColumnIndex("note")));
			list.add(note);
		}
		cursor.close();
		return list;
	}

	@Override
	public Note insertNote(int year, int month, int day, String note) {
		ContentValues values = new ContentValues();
		values.put("year", year);
		values.put("month", month);
		values.put("day", day);
		values.put("note", note);
		long id = getDb().insert(NOTE_TABLE, null, values);

		if (withNoteDates == null)
			initWithNoteDatesSet(mContext);
		withNoteDates.add(Utils.getDateAsInteger(year, month, day));

		return new Note(id, year, month, day, note);
	}

	@Override
	public void updateNote(long id, int year, int month, int day, String note) {
		String where = "_id=" + id;
		ContentValues values = new ContentValues();
		values.put("year", year);
		values.put("month", month);
		values.put("day", day);
		values.put("note", note);
		getDb().update(NOTE_TABLE, values, where, null);
	}

	@Override
	protected void deleteNote(long id) {
		getDb().delete(NOTE_TABLE, "_id=" + id, null);
	}

}
