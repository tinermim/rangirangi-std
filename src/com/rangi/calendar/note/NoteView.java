package com.rangi.calendar.note;

import java.util.ArrayList;
import java.util.Iterator;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.rangi.calendar.RangiActionBarActivity;
import com.rangi.calendar.SolarCalendar;
import com.rangi.calendar.MyDataTypes.Note;
import com.rangi.calendar.MyDataTypes.Reminder;
import com.rangi.calendar.customviews.MyTextView;
import com.rangi.calendar.database.DatabaseHelper;
import com.rangi.calendar.utils.ColorUtils;
import com.rangi.calendar.R;

public class NoteView extends RangiActionBarActivity {

	private static Context context;

	ArrayList<Note> listItems = new ArrayList<Note>();
	MyNoteArrayAdapter noteListAdapter;
	ListView listView;
	private ArrayList<Long> noteIdsToDelete = null;
	private static boolean deleteMode = false;

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		Log.i("inNotes", "On Create");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_note_view);
		setupActionBar(R.layout.action_bar_note_view);		
		
		new populateListTask().execute();
		context = this;

		noteListAdapter = new MyNoteArrayAdapter(this, listItems);
		listView = (ListView) findViewById(R.id.note_list);
		listView.setAdapter(noteListAdapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (deleteMode) {
					maintainDeleteList(view, id, position);
				} else {
					openViewNoteDialog(listItems.get(position));
				}
			}

			private void maintainDeleteList(View view, long id, int position) {
				CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox);
				checkBox.toggle();
				if (checkBox.isChecked()) {
					/* Add this item to delete list */
					noteIdsToDelete.add(id);
				} else {
					/* Remove this item to delete list */
					noteIdsToDelete.remove(id);
				}
				MyTextView actionBarNumber = (MyTextView) findViewById(R.id.num_of_deletes);
				actionBarNumber.setText("" + noteIdsToDelete.size());
			}

		});

		setupDrawer();
	}

	private void openViewNoteDialog(final Note note) {
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_note_view_note_content);
		dialog.setCanceledOnTouchOutside(true);

		((TextView) dialog.findViewById(R.id.title)).setText(SolarCalendar
				.noteDateToString(note));
		((TextView) dialog.findViewById(R.id.note_body)).setText(note.note);

		TextView reminderTime = ((TextView) dialog
				.findViewById(R.id.remind_time));
		Reminder reminder = DatabaseHelper.getDbHelper(getApplicationContext())
				.getNoteReminder(note.id);
		if (reminder != null) {
			reminderTime.setText(new DateTime(reminder.dateTime)
					.toString("HH:mm"));
		} else {
			((View) reminderTime.getParent()).setVisibility(View.GONE);
		}

		dialog.findViewById(R.id.btn_close).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						dialog.dismiss();
					}
				});
		dialog.findViewById(R.id.btn_edit).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent intent = new Intent(getApplicationContext(),
								NoteEditor.class);
						intent.setAction(NoteEditor.MODE_EDIT);
						intent.putExtra(NoteEditor.NOTE_ID, note.id);
						startActivity(intent);
					}
				});
		dialog.findViewById(R.id.btn_delete).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						NoteManager.getInstance(getApplicationContext())
								.deleteNoteAndItsReminders(note.id);
						listItems.remove(note);
						noteListAdapter.notifyDataSetChanged();
						dialog.dismiss();
					}
				});
		dialog.show();
	}

	private void setHintVisibility() {
		if (listItems.size() == 0)
			findViewById(R.id.adding_hint).setVisibility(View.VISIBLE);
		else
			findViewById(R.id.adding_hint).setVisibility(View.GONE);
	}

	public void openMenu(View view) {
		toggleSideDrawer();
	}

	public void deleteNotes(View view) {
		if (!deleteMode) {
			deleteMode = true;
			noteIdsToDelete = new ArrayList<Long>();
			listView.invalidateViews(); /* Causes to add check box to each row */
			bringUpDeletePanel();
			setupActionBar(R.layout.action_bar_note_view_delete_mode);
			((MyTextView) findViewById(R.id.num_of_deletes)).setText("0");
		}
	}

	public void deleteSelectedNotes(View view) {
		for (long id : noteIdsToDelete) {
			Note note = NoteManager.getInstance(this).getNoteById(id);
			NoteManager.getInstance(this).deleteNoteAndItsReminders(note.id);

			Iterator<Note> i = listItems.iterator();
			while (i.hasNext()) {
				Note item = i.next();
				if (item.id == id)
					i.remove();
			}
		}
		noteListAdapter.notifyDataSetChanged();
		finishDeleting();
	}

	private void finishDeleting() {
		cancelDeleting(null);
		setHintVisibility();
	}

	public void cancelDeleting(View view) {
		deleteMode = false;
		noteIdsToDelete = null;
		listView.invalidateViews(); /* Causes to remove check boxes from rows */
		takeDownDeletePanel();
		setupActionBar(R.layout.action_bar_note_view);
	}

	private void bringUpDeletePanel() {
		View deletePanel = findViewById(R.id.delete_panel);
		Animation upFromBottom = AnimationUtils.loadAnimation(this,
				R.anim.up_from_bottom);
		deletePanel.startAnimation(upFromBottom);
		deletePanel.setVisibility(View.VISIBLE);
	}

	private void takeDownDeletePanel() {
		View deletePanel = findViewById(R.id.delete_panel);
		Animation downFromTop = AnimationUtils.loadAnimation(this,
				R.anim.down_from_top);
		deletePanel.startAnimation(downFromTop);
		deletePanel.setVisibility(View.GONE);
	}

	@Override
	public void onBackPressed() {
		if (deleteMode)
			finishDeleting();
		finish();
	}

	private class populateListTask extends
			AsyncTask<Void, Void, ArrayList<Note>> {

		@Override
		protected ArrayList<Note> doInBackground(Void... params) {
			ArrayList<Note> list = NoteManager.getInstance(NoteView.this)
					.getAllNotes();
			return list;
		}

		@Override
		protected void onPostExecute(ArrayList<Note> notes) {
			for (Note note : notes) {
				listItems.add(note);
			}
			noteListAdapter.notifyDataSetChanged();
			setHintVisibility();
		}

	}

	private static class MyNoteArrayAdapter extends BaseAdapter {
		private static String[] months = SolarCalendar.getMonthNames();
		private static final int LAYOUT_COUNT = 1;
		private Activity activity;
		private ArrayList<Note> data;
		private static LayoutInflater inflater = null;

		// private boolean[] isChecked;

		public MyNoteArrayAdapter(Activity activity, ArrayList<Note> data) {
			this.activity = activity;
			this.data = data;
			// isChecked = new boolean[data.size()];
			// for (int i = 0; i < isChecked.length; i++) {
			// isChecked[i] = false;
			// }
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		public int getCount() {
			return data.size();
		}

		public Object getItem(int position) {
			return data.get(position);
		}

		public long getItemId(int position) {
			return data.get(position).id;
		}

		@Override
		public int getViewTypeCount() {
			return LAYOUT_COUNT;
		}

		@Override
		public int getItemViewType(int position) {
			return position % LAYOUT_COUNT;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View vi = convertView;
			if (convertView == null) {
				vi = inflater.inflate(R.layout.note_view_list_row, null);
			}

			CheckBox checkBox = (CheckBox) vi.findViewById(R.id.checkbox);
			if (deleteMode) {
				checkBox.setVisibility(View.VISIBLE);
				// checkBox.setChecked(isChecked[position]);
				vi.findViewById(R.id.reminder).setVisibility(View.GONE);
				vi.findViewById(R.id.note_reminder_view).setVisibility(
						View.GONE);
			} else if (checkBox.getVisibility() == View.VISIBLE) {
				checkBox.setVisibility(View.GONE);
				vi.findViewById(R.id.reminder).setVisibility(View.VISIBLE);
				vi.findViewById(R.id.note_reminder_view).setVisibility(
						View.VISIBLE);
				vi.setVisibility(View.VISIBLE);
				checkBox.setChecked(false);
			}

			// int checkedPosition=position;
			// checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener()
			// {
			//
			// @Override
			// public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
			// isChecked[checkedPosition] = arg1;
			// }
			// });

			// TextView year = (TextView) vi.findViewById(R.id.year);
			TextView month = (TextView) vi.findViewById(R.id.month);
			TextView day = (TextView) vi.findViewById(R.id.day);
			TextView weekDay = (TextView) vi.findViewById(R.id.dayWeek);
			TextView note = (TextView) vi.findViewById(R.id.note);

			Note item = data.get(position);

			vi.findViewById(R.id.date).setVisibility(View.VISIBLE);
			vi.findViewById(R.id.note_view_divider).setVisibility(View.GONE);
			if (position > 0) {
				Note pastItem = data.get(position - 1);
				if (item.year == pastItem.year && item.month == pastItem.month
						&& item.day == pastItem.day) {
					System.out.println("item: " + item.day + "/" + item.month
							+ "/" + item.year);
					System.out.println("pastItem: " + pastItem.day + "/"
							+ pastItem.month + "/" + pastItem.year);

					vi.findViewById(R.id.date).setVisibility(View.GONE);
					vi.findViewById(R.id.note_view_divider).setVisibility(
							View.VISIBLE);
				}
			}

			Resources r = activity.getResources();
			Drawable[] layers = new Drawable[2];
			layers[0] = new ColorDrawable(ColorUtils.getDayBGColor(
					activity,
					new SolarCalendar(new LocalDate(SolarCalendar
							.jalaliToGregorian(item.year, item.month - 1,
									item.day)))));
			layers[1] = r.getDrawable(R.drawable.shadow_noteview);
			LayerDrawable layerDrawable = new LayerDrawable(layers);

			vi.setBackgroundColor(Color.WHITE);
			vi.findViewById(R.id.date).setBackgroundColor(
					Color.parseColor("#e5e5e5"));
			vi.findViewById(R.id.day).setBackgroundDrawable(layerDrawable);
			// year.setText("" + item.year);
			month.setText(months[item.month - 1]);
			day.setText("" + item.day);
			note.setText(item.note);

			LocalDate ld = SolarCalendar.jalaliToGregorian(item.year,
					item.month, item.day);
			SolarCalendar sc = new SolarCalendar(ld);

			Reminder reminder = DatabaseHelper.getDbHelper(context)
					.getNoteReminder(item.id);
			vi.findViewById(R.id.note_reminder_view)
					.setVisibility(View.VISIBLE);
			if (!deleteMode) {
				vi.findViewById(R.id.reminder).setVisibility(View.VISIBLE);
				if (reminder == null) {
					vi.findViewById(R.id.note_reminder_view).setVisibility(
							View.GONE);
					vi.findViewById(R.id.reminder).setVisibility(View.GONE);
				} else {
					DateTime dateTime = new DateTime(reminder.dateTime);
					((TextView) vi.findViewById(R.id.reminder))
							.setText(dateTime.toString("HH:mm"));
				}
			}

			weekDay.setText(sc.strWeekDay);

			return vi;
		}
	}

	public void addNote(View view) {
		startActivity(new Intent(this, NoteEditor.class));
	}


}