package com.rangi.calendar;

import org.joda.time.LocalDate;

import com.rangi.calendar.customviews.MyTextView;
import com.rangi.calendar.customviews.WeekViewDay;
import com.rangi.calendar.settings.SettingsUtil;
import com.rangi.calendar.utils.ColorUtils;
import com.rangi.calendar.utils.Utils;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

// Caution! there's some weird (-)s when it comes t working with pager. It's just because of
// different way of changing pages in Iran. It's complicated though.

public class WeekView extends CalendarActivity {

	private static final int TODAY_BG_COLOR = Color.WHITE;
	public static final int NUM_OF_PAGES = 10000;
	public static final int MIDDLE_PAGE = 5000;
	private static final int CELL_MARGIN = 1;
	private static final int TODAY_DATE_COLOR = Color.BLACK;
	private static final int TODAY_WEEKDAY_COLOR = Color
			.parseColor("#7f000000");
	private static final int TODAY_EVENT_COLOR = Color.parseColor("#e5000000");
	WeeksPagerAdapter myWeeksPagerAdapter;
	ViewPager myViewPager;
	private static Context context;

	protected int ROW_WIDTH;
	protected int ROW_HEIGHT;
	protected static View monthShape;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ViewStub stub = (ViewStub) findViewById(R.id.stub);
		stub.setLayoutResource(R.layout.activity_week_view);
		stub.inflate();
		context = this;

		Intent intent = getIntent();
		long offset = intent.getLongExtra(MainActivity.WEEK_OFFSET, 0);

		myWeeksPagerAdapter = new WeeksPagerAdapter(getSupportFragmentManager());

		myViewPager = (ViewPager) findViewById(R.id.week_pager);
		myViewPager.setAdapter(myWeeksPagerAdapter);
		myViewPager.setCurrentItem((int) (MIDDLE_PAGE - offset));
		myViewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int currentPage) {
				setCustomTitle(-(currentPage - MIDDLE_PAGE));
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});

		setupActionBar();
	}

	@Override
	protected void onResume() {
		setupActionBar();
		// Reloading the pages on resume
		myWeeksPagerAdapter.notifyDataSetChanged();
		myViewPager.invalidate();
		super.onResume();
	}

	// making the title for week pager.
	protected void setCustomTitle(int offset) {
		LocalDate gDate = new LocalDate().plusWeeks(offset).withDayOfWeek(1);
		SolarCalendar date = SolarCalendar.offsetWeek(offset);
		final SolarCalendar firstDayOfWeek = date
				.addDays(-((date.weekDay + 1) % 7));
		int weekNumber;
		String otherMonth;
		int otherYear;
		String weekTitle;
		String monthYearTitle;
		SolarCalendar lastDayOfWeek = firstDayOfWeek.addDays(6);

		if (SettingsUtil.primaryCalendarIsJalali(this)) {
			if (firstDayOfWeek.month != date.month) {
				otherMonth = firstDayOfWeek.strMonth;
				otherYear = firstDayOfWeek.year;
				weekTitle = "هفته آخر - هفته 1";
				monthYearTitle = otherMonth + " " + otherYear % 100 + " - "
						+ date.strMonth + " " + date.year % 100;
			} else if (lastDayOfWeek.month != firstDayOfWeek.month) {
				otherMonth = lastDayOfWeek.strMonth;
				otherYear = lastDayOfWeek.year;
				weekTitle = "هفته آخر - هفته 1";
				monthYearTitle = date.strMonth + " " + date.year % 100 + " - "
						+ otherMonth + " " + otherYear % 100;
			} else {
				weekNumber = (int) Math.ceil((firstDayOfWeek.date - 1) / 7.0) + 1;
				weekTitle = "هفته " + weekNumber;
				monthYearTitle = date.strMonth + " " + date.year % 100;
			}
		} else {
			weekTitle = "هفته " + gDate.getWeekOfWeekyear() + " سال";
			monthYearTitle = SolarCalendar.getFarsiMonthName(gDate) + " "
					+ gDate.getYear();
		}
		TextView title = (TextView) findViewById(R.id.actionbar_title);
		title.setText(weekTitle + "\n" + monthYearTitle);
		title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
		title.setTypeface(Utils.getCustomFont(this));
	}

	public class WeeksPagerAdapter extends FragmentStatePagerAdapter {

		public WeeksPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int biasedOffset) {
			Fragment fragment = new WeekFragment();
			Bundle args = new Bundle();
			args.putInt(WeekFragment.OFFSET, -(biasedOffset - MIDDLE_PAGE));
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			return NUM_OF_PAGES;
		}

		// To reload the page once a change occured
		@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE;
		}
	}

	public static class WeekFragment extends Fragment {

		public static final String OFFSET = "offset";

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			Bundle args = getArguments();
			// (-) is for making pager suitable for Persian paging
			int offset = args.getInt(OFFSET);
			return drawWeek(offset);
		}

	}

	protected void setupActionBar() {
		super.setupActionBar();
		int offset = -(myViewPager.getCurrentItem() - MIDDLE_PAGE);
		setCustomTitle(offset);

		// adding action to month button
		findViewById(R.id.month_view).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
						final int currentItem = WeekView.this.myViewPager
								.getCurrentItem();
						gotoMonthView(SolarCalendar
								.offsetWeek(-(currentItem - MIDDLE_PAGE)));
					}
				});

		// adding action to week button
		findViewById(R.id.day_view).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
						final int currentItem = WeekView.this.myViewPager
								.getCurrentItem();
						goToDayEvent(SolarCalendar
								.offsetWeek(-(currentItem - MIDDLE_PAGE)));
					}
				});

	}

	public static View drawWeek(int offset) {
		final boolean jalaliIsPrimary = SettingsUtil.primaryCalendarIsJalali(context);

		final LocalDate gDate = new LocalDate().plusWeeks(offset)
				.withDayOfWeek(1);
		SolarCalendar week = SolarCalendar.offsetWeek(offset);
		final SolarCalendar firstDayOfWeek = jalaliIsPrimary ? week
				.withFirstDayOfWeek() : new SolarCalendar(gDate);
		SolarCalendar today = new SolarCalendar();

		LinearLayout mainLayout = new LinearLayout(context);
		mainLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		int color;

		for (int i = 0; i < 7; i += 2) {
			// prepare layout for putting two cells in it
			final LinearLayout twoDays = new LinearLayout(context);
			twoDays.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
					0, 1));
			twoDays.setOrientation(LinearLayout.HORIZONTAL);
			mainLayout.addView(twoDays);

			// preparing two cells and corresponding dates
			WeekViewDay leftDay = new WeekViewDay(context);
			final WeekViewDay rightDay = new WeekViewDay(context);
			final SolarCalendar leftDate = firstDayOfWeek.addDays(i + 1);
			final SolarCalendar rightDate = firstDayOfWeek.addDays(i);

			// setting up the cells parameters
			final LayoutParams params = new LayoutParams(0,
					LayoutParams.MATCH_PARENT, 1);
			params.setMargins(CELL_MARGIN, CELL_MARGIN, CELL_MARGIN,
					CELL_MARGIN);
			leftDay.setLayoutParams(params);
			rightDay.setLayoutParams(params);

			// fetching the event titles of days
			String leftEventTitle = Cache.getDailyEventTitle(context,
					leftDate.month, leftDate.date);
			String rightEventTitle = Cache.getDailyEventTitle(context,
					rightDate.month, rightDate.date);

			// setting content of left day
			TextView leftDayNumber = (MyTextView) leftDay
					.findViewById(R.id.week_view_day_date);
			if (jalaliIsPrimary)
				leftDayNumber.setText("" + leftDate.date);
			else {
				leftDayNumber.setTypeface(null);
				leftDayNumber.setText(""
						+ gDate.plusDays(i + 1).getDayOfMonth());
			}
			((MyTextView) leftDay.findViewById(R.id.week_view_day_weekday))
					.setText(leftDate.strWeekDay);
			((MyTextView) leftDay.findViewById(R.id.week_view_day_event))
					.setText(leftEventTitle);

			// setting content of right day
			TextView rightDayNumber = (MyTextView) rightDay
					.findViewById(R.id.week_view_day_date);
			if (jalaliIsPrimary)
				rightDayNumber.setText("" + rightDate.date);
			else {
				rightDayNumber.setTypeface(null);
				rightDayNumber.setText("" + gDate.plusDays(i).getDayOfMonth());
			}
			((MyTextView) rightDay.findViewById(R.id.week_view_day_weekday))
					.setText(rightDate.strWeekDay);
			((MyTextView) rightDay.findViewById(R.id.week_view_day_event))
					.setText(rightEventTitle);

			// drawing circle around a day which is holiday
			String leftHoliday = Cache.getHolidayEvent(context, leftDate.year,
					leftDate.month, leftDate.date);
			String rightHoliday = Cache.getHolidayEvent(context,
					rightDate.year, rightDate.month, rightDate.date);
			if (leftHoliday != null && !leftHoliday.equals("nothing")
					&& SettingsUtil.showHolidays(context))
				drawHolidayCircle(leftDay);
			if (rightHoliday != null && !rightHoliday.equals("nothing")
					&& SettingsUtil.showHolidays(context))
				drawHolidayCircle(rightDay);

			// coloring the cells

			// shanbe is 0 and so on
			int leftFirstDayWeekDay = (leftDate.addDays(-(leftDate.date - 1)).weekDay + 1) % 7;
			int rightFirstDayWeekDay = (rightDate
					.addDays(-(rightDate.date - 1)).weekDay + 1) % 7;

			if (jalaliIsPrimary)
				color = ColorUtils.getColor(context, leftDate.month,
						leftDate.date + leftFirstDayWeekDay - 1);
			else
				color = ColorUtils
						.getDayBGColor(context, gDate.plusDays(i + 1));
			if (leftDate.equals(today)) {
				color = TODAY_BG_COLOR;
				colorizeTodayText(leftDay);
			}
			leftDay.setBackgroundColor(color);

			if (jalaliIsPrimary)
				color = ColorUtils.getColor(context, rightDate.month,
						rightDate.date + rightFirstDayWeekDay - 1);
			else
				color = ColorUtils.getDayBGColor(context, gDate.plusDays(i));
			if (rightDate.equals(today)) {
				color = TODAY_BG_COLOR;
				colorizeTodayText(rightDay);
			}
			rightDay.setBackgroundColor(color);

			// make cells clickable
			leftDay.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					goToDayEvent(leftDate);
				}
			});
			rightDay.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					goToDayEvent(rightDate);
				}
			});

			if (i == 6) {
				// for accessing width and height of the layout at runtime
				ViewTreeObserver observer = twoDays.getViewTreeObserver();
				observer.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

					@Override
					public void onGlobalLayout() {
						if (jalaliIsPrimary)
							monthShape = new DrawableMonth(WeekView.context,
									firstDayOfWeek, twoDays.getWidth() / 2,
									twoDays.getHeight(), true);
						else
							monthShape = new DrawableGregorianMonth(
									WeekView.context, gDate,
									twoDays.getWidth() / 2,
									twoDays.getHeight(), true);

						twoDays.getViewTreeObserver()
								.removeGlobalOnLayoutListener(this);
						monthShape.setLayoutParams(params);
						monthShape
								.setOnClickListener(new View.OnClickListener() {

									@Override
									public void onClick(View arg0) {
										gotoMonthView(firstDayOfWeek);
									}
								});
						twoDays.addView(monthShape);
						twoDays.addView(rightDay);
					}
				});

			} else {
				twoDays.addView(leftDay);
				twoDays.addView(rightDay);
			}
		}

		return mainLayout;
	}

	private static void drawHolidayCircle(WeekViewDay day) {
		day.findViewById(R.id.week_view_day_date).setBackgroundResource(
				R.drawable.circle_gray_filled);
	}

	private static void colorizeTodayText(WeekViewDay day) {
		((MyTextView) day.findViewById(R.id.week_view_day_date))
				.setTextColor(TODAY_DATE_COLOR);
		((MyTextView) day.findViewById(R.id.week_view_day_weekday))
				.setTextColor(TODAY_WEEKDAY_COLOR);
		((MyTextView) day.findViewById(R.id.week_view_day_event))
				.setTextColor(TODAY_EVENT_COLOR);
	}

	protected static void gotoMonthView(SolarCalendar jDate) {
		Intent intent = new Intent(context, MonthView.class);
		intent.putExtra(MainActivity.MONTH_OFFSET,
				SolarCalendar.calculateMonthOffset(jDate));
		context.startActivity(intent);
	}

	private static void goToDayEvent(SolarCalendar date) {
		Intent intent = new Intent(context, DayView.class);
		intent.putExtra(MainActivity.DAY, date.miladiDate);
		intent.putExtra(MainActivity.MONTH, date.miladiMonth);
		intent.putExtra(MainActivity.YEAR, date.miladiYear);
		context.startActivity(intent);
	}

}
