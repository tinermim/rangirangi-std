package com.rangi.calendar;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;


/*
 * Here is the implementation of the dialog which is shown any where that we should ask the customer to buy
 * the related component(package). any pro feature which is not purchased will show this dialog, through which we handle the purchase 
 * process and do IAP
 */
public class DialogPurchase extends Dialog implements
android.view.View.OnClickListener {

	public Activity c;
	public Dialog d;
	public Button yes, no;
	private PurchaseComponent item = PurchaseComponent.PURCHASE_UNKNOWN;

	public DialogPurchase(Activity a , PurchaseComponent item) {
		super(a);
		this.item = item;
		this.c = a;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_purchase);
		yes = (Button) findViewById(R.id.btn_yes);
		no = (Button) findViewById(R.id.btn_no);
		yes.setOnClickListener(this);
		no.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {


		Log.e("item saved in dialoge" , ""+item);
		switch (v.getId()) {
		case R.id.btn_yes:
			 c.startActivity(new Intent(c,
					Store.class));
				dismiss();
			break;
		case R.id.btn_no:
//			PurchaseControl.setPurchaseSituation(PurchaseComponent.PURCHASE_GEROGRIAN_HIJRI_CALENDAR,false);
//			PurchaseControl.setPurchaseSituation(PurchaseComponent.PURCHASE_NOTES,false);
//			PurchaseControl.setPurchaseSituation(PurchaseComponent.PURCHASE_SYNC_WITH_GOOGLE,false);
//			Toast.makeText(c, "pref cleared", Toast.LENGTH_SHORT).show();
			
			dismiss();
			break;
		default:
			break;
		}
	
	}
}


