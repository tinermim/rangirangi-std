package com.rangi.calendar;

import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.rangi.calendar.MyDataTypes.Birthday;
import com.rangi.calendar.MyDataTypes.Note;
import com.rangi.calendar.MyDataTypes.Reminder;
import com.rangi.calendar.alarm.AlarmService;
import com.rangi.calendar.database.DatabaseHelper;
import com.rangi.calendar.note.NoteEditor;
import com.rangi.calendar.note.NoteManager;
import com.rangi.calendar.settings.SettingsUtil;
import com.rangi.calendar.utils.ColorUtils;
import com.rangi.calendar.utils.Utils;

public class DayView extends CalendarActivity {

	public static final int NUM_OF_PAGES = 10000;
	public static final int MIDDLE_PAGE = NUM_OF_PAGES / 2;
	public static final int MARGIN = 1;
	private static String HOLIDAY;
	private CalendarActivity a = DayView.this;

	/*
	 * A static reference to this object
	 */
	public static DayView dayView;

	DaysPagerAdapter myDaysPagerAdapter;
	ViewPager myViewPager;

	DayViewDateRow dateRow;

	ViewPager weekScroller;
	WeekScrollerPagerAdapter myWeekScrollerPagerAdapter;
	protected int trueOffset;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		AlarmService.startWidgetUpdateReceiver(getApplicationContext());

		ViewStub stub = (ViewStub) findViewById(R.id.stub);
		stub.setLayoutResource(R.layout.activity_display_day_event);
		stub.inflate();

		dayView = this;

		Intent intent = getIntent();
		int gDay = intent.getIntExtra(MainActivity.DAY, 0);
		int gMonth = intent.getIntExtra(MainActivity.MONTH, 0);
		int gYear = intent.getIntExtra(MainActivity.YEAR, 0);
		LocalDate date;
		if (gDay == 0 && gMonth == 0 && gYear == 0)
			date = new LocalDate();
		else
			date = new LocalDate(gYear, gMonth, gDay);
		trueOffset = (int) SolarCalendar.calculateDayOffset(date);

		setupTopRow(trueOffset);
		setupActionBar();
	}

	/*
	 * Adds upper row of day view which includes today's date in Jalali, Islamic
	 * and Gregorian and also a pager for navigating between weeks.
	 */
	private void setupTopRow(int offset) {
		LinearLayout topRow = (LinearLayout) findViewById(R.id.top_row);
		addDateRow(topRow, offset);
		addHorizontalBar(topRow);
		addWeekScroller(topRow, getWeekScrollerOffset(offset));
		addDayPager(offset);
	}

	private void addHorizontalBar(LinearLayout topRow) {
		View bar = new View(this);
		bar.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 1));
		bar.setBackgroundColor(Color.LTGRAY);
		topRow.addView(bar);
	}

	private int getWeekScrollerOffset(int dayOffset) {
		int dayOffsetOfFirstDayOfWeek;
		if (SettingsUtil.primaryCalendarIsJalali(this))
			dayOffsetOfFirstDayOfWeek = (int) SolarCalendar
			.calculateDayOffset(SolarCalendar.offsetDay(dayOffset)
					.withFirstDayOfWeek());
		else
			dayOffsetOfFirstDayOfWeek = Days.daysBetween(LocalDate.now(),
					new LocalDate().plusDays(dayOffset).withDayOfWeek(1))
					.getDays();

		if (dayOffsetOfFirstDayOfWeek >= 0)
			return (dayOffsetOfFirstDayOfWeek + 6) / 7;
		else
			return dayOffsetOfFirstDayOfWeek / 7;
	}

	private void addDayPager(int offset) {
		myDaysPagerAdapter = new DaysPagerAdapter(getSupportFragmentManager());
		myViewPager = (ViewPager) findViewById(R.id.day_pager);
		myViewPager.setAdapter(myDaysPagerAdapter);
		myViewPager.setCurrentItem(MIDDLE_PAGE - offset);
		myViewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int currentPage) {
				trueOffset = -(currentPage - MIDDLE_PAGE);

				updateWeekScroller();

				setDateRowProperties(trueOffset);
				weekScroller.setCurrentItem(MIDDLE_PAGE / 7
						- getWeekScrollerOffset(trueOffset));
				// setCustomTitle(trueOffset);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});

	}

	@Override
	public void onBackPressed() {
		int trueOffset = -(myViewPager.getCurrentItem() - MIDDLE_PAGE);
		SolarCalendar date = SolarCalendar.offsetDay(trueOffset);
		gotoMonthView(date);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		
		// condition to show the purchased items
		// if not purchased we won't show other dates on top left corner of Day view
		case R.id.action_add_note:
			if(!PurchaseControl.getInstance(this).isPurchased(PurchaseComponent.PURCHASE_NOTES) )
			{
				DialogPurchase cdd=new DialogPurchase( this ,PurchaseComponent.PURCHASE_NOTES);
				cdd.show();  
				return false;
			}
			else{
				Intent intent = new Intent(this, NoteEditor.class);
				intent.setAction(NoteEditor.MODE_ADD);
				intent.putExtra(MainActivity.DAY_OFFSET, trueOffset);
				startActivity(intent);
				return true;
			}
		}
		return super.onOptionsItemSelected(item);
	}

	private void addWeekScroller(LinearLayout topRow, int offset) {
		int screenWidth = Utils.getScreenWidth(this);
		weekScroller = new ViewPager(this);
		weekScroller.setId(1);
		weekScroller.setLayoutParams(new LayoutParams(screenWidth,
				screenWidth / 8));
		myWeekScrollerPagerAdapter = new WeekScrollerPagerAdapter(
				getSupportFragmentManager());
		weekScroller.setAdapter(myWeekScrollerPagerAdapter);
		weekScroller.setCurrentItem(MIDDLE_PAGE / 7 - offset);
		topRow.addView(weekScroller);
	}

	private void addDateRow(LinearLayout topRow, int offset) {
		int screenWidth = Utils.getScreenWidth(this);
		dateRow = new DayViewDateRow(this, screenWidth, screenWidth / 5);
		topRow.addView(dateRow);
		setDateRowProperties(offset);
	}

	@Override
	protected void onResume() {
		setupActionBar();
		setDateRowProperties(-(myViewPager.getCurrentItem() - MIDDLE_PAGE));
		// Reloading the pages on resume
		myDaysPagerAdapter.notifyDataSetChanged();
		myViewPager.invalidate();
		myWeekScrollerPagerAdapter.notifyDataSetChanged();
		weekScroller.invalidate();
		super.onResume();
	}

	private void setCustomTitle(int offset) {
		LocalDate gDate = new LocalDate().plusDays(offset);
		SolarCalendar date = SolarCalendar.offsetDay(offset);
		String weekday = date.strWeekDay;
		String month;
		int day;

		if (SettingsUtil.primaryCalendarIsJalali(this)) {
			month = date.strMonth;
			day = date.date;
		} else {
			month = SolarCalendar.getFarsiMonthName(gDate);
			day = gDate.getDayOfMonth();
		}

		TextView title = (TextView) findViewById(R.id.actionbar_title);
		title.setText(weekday + "\n" + day + " " + month);
		title.setTypeface(Utils.getCustomFont(this));
	}

	public class DaysPagerAdapter extends FragmentStatePagerAdapter {

		public DaysPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int biasedOffset) {
			int trueOffset = -(biasedOffset - MIDDLE_PAGE);
			Fragment fragment = new DayFragment();
			Bundle args = new Bundle();
			args.putInt(DayFragment.OFFSET, trueOffset);
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			return NUM_OF_PAGES;
		}

		// To reload the page once a change occured
		@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE;
		}
	}

	public static class DayFragment extends Fragment {

		public static final String OFFSET = "offset";

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			Bundle args = getArguments();
			int offset = args.getInt(OFFSET);
			return drawDay(offset);
		}

	}

	public class WeekScrollerPagerAdapter extends FragmentStatePagerAdapter {

		public WeekScrollerPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int biasedOffset) {
			int trueOffset = -(biasedOffset - MIDDLE_PAGE / 7);
			Fragment fragment = new WeekScrollerFragment();
			Bundle args = new Bundle();
			args.putInt(WeekScrollerFragment.OFFSET, trueOffset);
			args.putBoolean(WeekScrollerFragment.PRIMARY_IS_JALALI,
					SettingsUtil.primaryCalendarIsJalali(DayView.this));
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			return NUM_OF_PAGES / 7;
		}

		// To reload the page once a change occured
		@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE;
		}
	}

	protected void setupActionBar() {
		super.setupActionBar();

		int offset = -(myViewPager.getCurrentItem() - MIDDLE_PAGE);
		// setCustomTitle(offset);

		// adding action to week button
		findViewById(R.id.week_view).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
						final int currentItem = DayView.this.myViewPager
								.getCurrentItem();
						gotoWeekView(SolarCalendar
								.offsetDay(-(currentItem - MIDDLE_PAGE)));
					}
				});

	}

	public void setDateRowProperties(int trueOffset) {
		boolean jalaliIsPrimary = SettingsUtil.primaryCalendarIsJalali(this);
		LocalDate date = new LocalDate().plusDays(trueOffset);
		dateRow.setDate(date);
		dateRow.setPrimaryDate(jalaliIsPrimary ? DayViewDateRow.JALALI
				: DayViewDateRow.GREGORIAN);
		dateRow.setTopSecondaryDate(DayViewDateRow.ISLAMIC);
		dateRow.setBottomSecondaryDate(jalaliIsPrimary ? DayViewDateRow.GREGORIAN
				: DayViewDateRow.JALALI);
		dateRow.setIsHoliday(Cache.isHoliday(this, date));
		dateRow.invalidate();
	}

	public static View drawDay(final int offset) {
		final LocalDate gDate = new LocalDate().plusDays(offset);
		final SolarCalendar jDate = SolarCalendar.offsetDay(offset);

		final boolean jalaliIsPrimary = SettingsUtil
				.primaryCalendarIsJalali(dayView);

		int year = jDate.year;
		int month = jDate.month;
		int day = jDate.date;

		LayoutInflater inflater = LayoutInflater.from(dayView);
		View mainView = inflater.inflate(R.layout.dayviewpage, null);

		final String eventTitle = Cache.getDailyEventTitle(dayView,
				jDate.month, jDate.date);
		final String eventDescription = Cache.getDailyEventDescription(dayView,
				jDate.month, jDate.date);
		HOLIDAY = Cache.getHolidayEvent(dayView, jDate.year, jDate.month,
				jDate.date);

		// changing the background of main part, based on month
		int bgColor;
		if (jalaliIsPrimary)
			bgColor = ColorUtils.getDayBGColor(dayView, jDate);
		else
			bgColor = ColorUtils.getDayBGColor(dayView, gDate);
		(mainView.findViewById(R.id.dayviewpage_sv_day))
		.setBackgroundColor(bgColor);

		setupEventSection(mainView, eventTitle, eventDescription);

		setupHolidaySection(mainView);

		setupShareButton(jDate, mainView, eventTitle, eventDescription);

		setupNoteList(offset, year, month, day, mainView, bgColor);

		setupBirthdayList(month, day, mainView, bgColor);

		return mainView;
	}

	private static void setupEventSection(View mainView,
			final String eventTitle, final String eventDescription) {
		TextView dayEvent = (TextView) mainView
				.findViewById(R.id.dayviewpage_dayevent);
		dayEvent.setText(eventDescription);
		TextView dayTitle = (TextView) mainView
				.findViewById(R.id.dayviewpage_daytitle);
		dayTitle.setText(eventTitle);
	}

	private static void setupShareButton(final SolarCalendar jDate,
			View mainView, final String eventTitle,
			final String eventDescription) {
		mainView.findViewById(R.id.dayviewpage_share).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
						Intent sharingIntent = new Intent(
								android.content.Intent.ACTION_SEND);
						sharingIntent.setType("text/plain");
						String shareBody = eventTitle + "\n" + eventDescription
								+ "\n"
								+ "مجله آنلاین رنگی رنگی - RangiRangi.com";
						sharingIntent.putExtra(
								android.content.Intent.EXTRA_SUBJECT,
								"تقویم رنگی رنگی - مناسبت " + jDate.date + " "
										+ jDate.strMonth);
						sharingIntent.putExtra(
								android.content.Intent.EXTRA_TEXT, shareBody);
						dayView.startActivity(Intent.createChooser(
								sharingIntent, "Share via"));
					}
				});
	}

	private static void setupHolidaySection(View mainView) {
		if (HOLIDAY != null && !HOLIDAY.equals("nothing")
				&& SettingsUtil.showHolidays(dayView)) {
			((TextView) mainView.findViewById(R.id.dayviewpage_holiday))
			.setText(HOLIDAY);
		} else
			(mainView.findViewById(R.id.holiday_section))
			.setVisibility(View.GONE);
	}

	private static void setupNoteList(final int offset, int year, int month,
			int day, View mainView, int bgColor) {
		Note[] noteData = NoteManager.getInstance(dayView).getNotes(year,
				month, day);

		LinearLayout ll = (LinearLayout) mainView.findViewById(R.id.notes_list);

		if (noteData != null) {

			TextView notesCount = ((TextView) mainView
					.findViewById(R.id.notes_count));
			notesCount.setText("" + noteData.length);
			notesCount.setTextColor(bgColor);

			for (int i = 0; i < noteData.length; i++) {
				if (noteData[i] != null) {
					final int index = i;
					final Note noteData1 = noteData[index];

					View row = dayView.getLayoutInflater().inflate(
							R.layout.day_view_page_note_list_row, null);
					TextView reminderTv = (TextView) row.findViewById(R.id.reminder);
					TextView noteTv = (TextView) row.findViewById(R.id.note);

					Reminder reminder = DatabaseHelper.getDbHelper(dayView)
							.getNoteReminder(noteData1.id);
					if (reminder != null) {
						reminderTv.setText(new DateTime(reminder.dateTime)
						.toString("HH:mm"));
					} else {
						reminderTv.setVisibility(View.GONE);
					}

					noteTv.setText(noteData[index].note);
					row.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View arg0) {
							
							// condition to show the purchased items
							// if not purchased we won't show let the customer go to Notes View
							Log.i("DayView" , "purchased is: " + PurchaseControl.getInstance(dayView).isPurchased(PurchaseComponent.PURCHASE_NOTES));
							if(!PurchaseControl.getInstance(dayView).isPurchased(PurchaseComponent.PURCHASE_NOTES) )
							{
								DialogPurchase cdd=new DialogPurchase( (Activity)arg0.getContext() ,PurchaseComponent.PURCHASE_NOTES);
								cdd.show();  
							}
							else{
								Intent intent = new Intent(dayView,
										NoteEditor.class);
								intent.setAction(NoteEditor.MODE_EDIT);
								intent.putExtra(NoteEditor.NOTE_ID, noteData1.id);
								intent.putExtra(MainActivity.DAY_OFFSET, offset);
								dayView.startActivity(intent);
							}
						}
					});

					ll.addView(row);

					ImageView divider = new ImageView(dayView);
					LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
							LayoutParams.MATCH_PARENT, 1);
					lp.setMargins(20, 0, 20, 0);
					divider.setLayoutParams(lp);
					divider.setBackgroundColor(Color.WHITE);
					if (i + 1 < noteData.length)
						ll.addView(divider);
					if (noteData.length > 0) {
						mainView.findViewById(R.id.note_section).setVisibility(
								View.VISIBLE);
					}
				}
			}
		}
	}

	private static void setupBirthdayList(int month, int day, View mainView,
			int bgColor) {
		List<Birthday> birthdays = DatabaseHelper.getDbHelper(dayView)
				.getBirthdays(month - 1, day);
		TextView birthdayView = ((TextView) mainView
				.findViewById(R.id.today_birthdays));
		if (!birthdays.isEmpty()) {
			String birthdayList = "";
			for (Birthday birthday : birthdays)
				birthdayList += birthday.name + " / ";
			// to remove last entered \n
			birthdayList = birthdayList.substring(0, birthdayList.length() - 2);
			birthdayView.setText(birthdayList);
			mainView.findViewById(R.id.birthday_section).setVisibility(
					View.VISIBLE);
		}

		TextView birthdaysCount = ((TextView) mainView
				.findViewById(R.id.birthdays_count));
		birthdaysCount.setText("" + birthdays.size());
		birthdaysCount.setTextColor(bgColor);

		setAddOrEditBirthdayClickListener(mainView, R.id.birthday_section);
	}

	private static void setAddOrEditBirthdayClickListener(View mainView,
			int buttonId) {
		mainView.findViewById(buttonId).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						Intent intent = new Intent(dayView, BirthdayView.class);
						dayView.startActivity(intent);
					}
				});
	}

	protected void gotoWeekView(SolarCalendar jDate) {
		Intent intent = new Intent(this, WeekView.class);
		intent.putExtra(MainActivity.WEEK_OFFSET,
				SolarCalendar.calculateWeekOffset(jDate));
		startActivity(intent);
	}

	protected void gotoMonthView(SolarCalendar jDate) {
		Intent intent = new Intent(this, MonthView.class);
		intent.putExtra(MainActivity.MONTH_OFFSET,
				SolarCalendar.calculateMonthOffset(jDate));
		startActivity(intent);
	}

	private void updateWeekScroller() {
		final Handler mHandler = new Handler();

		Thread mThread = new Thread(new Runnable() {
			@Override
			public void run() {
				mHandler.post(new Runnable() {
					@Override
					public void run() {
						myWeekScrollerPagerAdapter.notifyDataSetChanged();
					}
				});
			}
		});
		mThread.setPriority(Thread.MIN_PRIORITY);
		mThread.start();
	}

}