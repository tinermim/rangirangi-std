package com.rangi.calendar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.rangi.calendar.customviews.MyButton;
import com.rangi.calendar.customviews.MyTextView;
import com.rangi.calendar.note.NoteEditor;
import com.rangi.calendar.note.NoteView;
import com.rangi.calendar.settings.SettingsActivity;
import com.rangi.calendar.settings.SettingsUtil;
import com.rangi.calendar.utils.ColorUtils;
import com.rangi.calendar.utils.Utils;
import org.joda.time.LocalDate;
import org.joda.time.chrono.IslamicChronology;

public class CalendarActivity extends ActionBarActivity {

	private static final int HOME_PAGE = 0;
	private static final int STORE = 1;
	private static final int DATE_CONVERTER = 2;
	private static final int NOTES = 3;
	private static final int BIRTHDAYS = 4;
	private static final int SETTINGS = 5;
	private static final int HELP = 6;

	private static final int DRAWER_BG_COLOR = Color.parseColor("#e8e8e8");
	private String[] menuItems;
	private TypedArray menuItemsIcons;
	private ListView mDrawerList;
	private LinearLayout mDrawer;
	private DrawerLayout mDrawerLayout;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_calendar);
		setupDrawer();
	
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// Pressing physical menu button should toggle the side drawer.
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			toggleSideDrawer(null);
			return true;
		}
		// Other keys are handled by system.
		return super.onKeyDown(keyCode, event);
	}

	protected void setupDrawer() {
		// setDrawerBackground();
		menuItems = getResources().getStringArray(R.array.menu_items);
		menuItemsIcons = getResources().obtainTypedArray(
				R.array.menu_items_icons);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawer = (LinearLayout) findViewById(R.id.right_drawer);
		mDrawerList = (ListView) findViewById(R.id.right_drawer_list);

		// Set the adapter for the list view
		mDrawerList.setAdapter(new DrawerArrayAdapter(this, menuItems,
				menuItemsIcons));
		// Set the list's click listener
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		addTodaysDate();
	}

	private void setDrawerBackground() {
		Drawable[] layers = new Drawable[2];
		layers[0] = new ColorDrawable(DRAWER_BG_COLOR);
		layers[1] = getResources().getDrawable(R.drawable.pattern1);
		Utils.repeatBackground(layers[1]);
		LayerDrawable bg = new LayerDrawable(layers);
		View background = findViewById(R.id.right_drawer);
		background.setBackgroundDrawable(bg);
	}

	private void addTodaysDate() {
		SolarCalendar jDate = new SolarCalendar();
		LocalDate gDate = new LocalDate();
		LocalDate iDate = new LocalDate(gDate.toDate(),
				IslamicChronology.getInstance());

		String jalali = jDate.date + " " + jDate.strMonth + " " + jDate.year;
		String gregorian = gDate.toString("d MMM yyyy");
		String islamic = iDate.getDayOfMonth() + " "
				+ SolarCalendar.getIslamicMonthName(iDate) + " "
				+ iDate.getYear();
		TextView primaryDate = (TextView) findViewById(R.id.primary_date);
		TextView rightSecondaryDate = (TextView) findViewById(R.id.right_secondary_date);
		if (SettingsUtil.primaryCalendarIsJalali(this)) {
			primaryDate.setTypeface(Utils.getCustomFont(this));
			primaryDate.setText(jalali);
			rightSecondaryDate.setText(gregorian);
		} else {
			primaryDate.setText(gregorian);
			rightSecondaryDate.setTypeface(Utils.getCustomFont(this));
			rightSecondaryDate.setText(jalali);
		}
		((TextView) findViewById(R.id.left_secondary_date)).setText(islamic);
	}

	protected void setupActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.bottom_bar)));
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setDisplayShowHomeEnabled(false);

		// putting a custom layout in middle of action bar
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayShowTitleEnabled(false);
		LayoutInflater inflator = (LayoutInflater) this
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View title = inflator.inflate(R.layout.titleview, null);

		addTodayButton(title);

		actionBar.setCustomView(title);

		// clicking on rangirani logo should take user to its website
		findViewById(R.id.rangirangi_logo).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
						Utils.goToUrl(CalendarActivity.this,
								"http://www.rangirangi.com");
					}
				});

		supportInvalidateOptionsMenu();
	}

	/*
	 * Today button is a button with today's date written on it. The background
	 * shape color will change by changing of date.
	 */
	private void addTodayButton(View actionBar) {
		MyButton todayButton = (MyButton) actionBar
				.findViewById(R.id.action_bar_today);
		int bgColor;
		if (SettingsUtil.getSettings(this).getString("primary_calendar", "0")
				.equals("1")) {
			SolarCalendar date = new SolarCalendar();
			todayButton.setText("" + date.date);
			bgColor = ColorUtils.getDayBGColor(this, date);
		} else {
			LocalDate date = new LocalDate();
			todayButton.setTypeface(null);
			todayButton.setText("" + date.getDayOfMonth());
			bgColor = ColorUtils.getDayBGColor(this, date);
		}
		View bg = actionBar.findViewById(R.id.action_bar_today_frame);
		bg.setBackgroundDrawable(ColorUtils.setIconColor(getResources(),
				bgColor));
	}

	public void toggleSideDrawer(View view) {
		if (!mDrawerLayout.isDrawerOpen(mDrawer))
			mDrawerLayout.openDrawer(mDrawer);
		else
			mDrawerLayout.closeDrawer(mDrawer);
	}

	public void gotoToday(View view) {
		Intent intent = new Intent(this, DayView.class);
		startActivity(intent);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		switch (item.getItemId()) {
		case android.R.id.home:
			intent = new Intent(this, DayView.class);
			startActivity(intent);
			return true;
		case R.id.action_menu:
			toggleSideDrawer(null);
			break;
		case R.id.action_add_note:
			goToNoteEditor();
			break;
		// TODO For drop down calendar uncomment
		// case R.id.action_today_year:
		// intent = new Intent(this, YearView.class);
		// startActivity(intent);
		// return true;
		// case R.id.action_today_month:
		// intent = new Intent(this, MonthView.class);
		// startActivity(intent);
		// return true;
		// case R.id.action_today_week:
		// intent = new Intent(this, WeekView.class);
		// startActivity(intent);
		// return true;
		// case R.id.action_today_day:
		// intent = new Intent(this, DayView.class);
		// startActivity(intent);
		// return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void goToNoteView() {
		Log.i("calendarActivity","gotoNoteView, purchased: "+ PurchaseControl.getInstance(this).isPurchased(PurchaseComponent.PURCHASE_NOTES));
		if(!PurchaseControl.getInstance(this).isPurchased(PurchaseComponent.PURCHASE_NOTES) )
		{
			DialogPurchase cdd=new DialogPurchase( this ,PurchaseComponent.PURCHASE_NOTES);
			cdd.show();  
			
		}
		else
			startActivity(new Intent(this, NoteView.class));
	}

	private void goToNoteEditor() {
		Log.i("calendarActivity","gotoNoteEditor, purchase is: " + PurchaseControl.getInstance(this).isPurchased(PurchaseComponent.PURCHASE_NOTES));
		if(!PurchaseControl.getInstance(this).isPurchased(PurchaseComponent.PURCHASE_NOTES) )
		{
			DialogPurchase cdd=new DialogPurchase( this ,PurchaseComponent.PURCHASE_NOTES);
			cdd.show();  
		}
		else
			startActivity(new Intent(this, NoteEditor.class));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);

		int year;
		String month;
		int weekNumber;
		int day;

		if (SettingsUtil.primaryCalendarIsJalali(this)) {
			SolarCalendar today = new SolarCalendar();
			year = today.year;
			month = today.strMonth;
			final SolarCalendar firstDayOfWeek = today
					.addDays(-((today.weekDay + 1) % 7));
			weekNumber = (int) Math.ceil((firstDayOfWeek.date - 1) / 7.0) + 1;
			day = today.date;
		} else {
			LocalDate date = new LocalDate();
			year = date.getYear();
			month = SolarCalendar.getFarsiMonthName(date);
			weekNumber = date.getWeekOfWeekyear();
			day = date.getDayOfMonth();
		}

		// TODO For drop down calendar uncomment
		// menu.findItem(R.id.action_today_year).setTitle("سال " + year);
		// menu.findItem(R.id.action_today_month).setTitle("ماه " + month);
		// menu.findItem(R.id.action_today_week).setTitle("هفته " + weekNumber);
		// menu.findItem(R.id.action_today_day).setTitle("روز " + day);

		return super.onCreateOptionsMenu(menu);
	}

	private class DrawerArrayAdapter extends BaseAdapter {
		private Activity activity;
		private String[] titles;
		private TypedArray icons;
		private LayoutInflater inflater = null;

		public DrawerArrayAdapter(Activity activity, String[] titles,
				TypedArray icons) {
			this.activity = activity;
			this.titles = titles;
			this.icons = icons;
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		public int getCount() {
			return titles.length;
		}

		public Object getItem(int position) {
			return titles[position];
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View vi = convertView;
			if (convertView == null) {
				vi = inflater.inflate(R.layout.drawer_item, null);
				if (position != 5 && position != 4)
					vi.setBackgroundResource(R.drawable.selector_list);
				else
					vi.setBackgroundResource(R.drawable.selector_list_bottom);
			}

			MyTextView title = (MyTextView) vi.findViewById(R.id.title);
			ImageView icon = (ImageView) vi.findViewById(R.id.icon);

			title.setText(titles[position]);
			switch (position) {
			case HOME_PAGE:
				title.setTextColor(getResources().getColor(
						R.color.menu_home_title));
				break;

// TODO

			case STORE:
				title.setTextColor(getResources().getColor(
						R.color.menu_store_title));
				break;
				

			case DATE_CONVERTER:
				title.setTextColor(getResources().getColor(
						R.color.menu_converter_title));
				break;

			case NOTES:
				title.setTextColor(getResources().getColor(
						R.color.menu_notes_title));
				break;
			case BIRTHDAYS:
				title.setTextColor(getResources().getColor(
						R.color.menu_birthdays_title));
				break;

			default:
				title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
				title.setTextColor(Color.parseColor("#575757"));
				break;
			}
			icon.setImageDrawable(icons.getDrawable(position));

			return vi;
		}
	}

	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {

		@Override
		public void onItemClick(AdapterView parent, View view, int position,
				long id) {
			mDrawerLayout.closeDrawer(mDrawer);

			Intent intent;
			switch (position) {
			case HOME_PAGE:
				startActivity(new Intent(CalendarActivity.this, MonthView.class)
						.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
				break;
// TODO
			case STORE:
				startActivity(new Intent(CalendarActivity.this,
						Store.class));
				break;
				
			case DATE_CONVERTER:
				startActivity(new Intent(CalendarActivity.this,
						DateConverterActivity.class));
				break;
			case NOTES:
				goToNoteView();
				break;
			case BIRTHDAYS:
				startActivity(new Intent(CalendarActivity.this,
						BirthdayView.class));
				break;
			case SETTINGS:
				startActivity(new Intent(CalendarActivity.this,
						SettingsActivity.class));
				break;
			case HELP:
				startActivity(new Intent(CalendarActivity.this,
						IntroActivity.class));
				break;
			}
		}
	}
}
