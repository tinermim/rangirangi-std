package com.rangi.calendar;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.util.ByteArrayBuffer;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.util.Log;

public class CheckUpdate extends AsyncTask<Activity, Void, Boolean> {

	Activity context;
	@Override
	protected Boolean doInBackground(Activity... arg0) {

		context = arg0[0];
		Log.e("checkFOrUpdate", "Thread started");

		URL updateURL = null;
		try {
			updateURL = new URL(
					context.getString( R.string.update_version_file_url));
		} catch (MalformedURLException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		URLConnection conn = null;
		try {
			conn = updateURL.openConnection();
		} catch (IOException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		InputStream is = null;
		try {
			is = conn.getInputStream();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		BufferedInputStream bis = new BufferedInputStream(is);
		ByteArrayBuffer baf = new ByteArrayBuffer(50);

		int current = 0;
		try {
			while ((current = bis.read()) != -1) {
				baf.append((byte) current);
			}
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		/* Convert the Bytes read to a String. */
		final String s = new String(baf.toByteArray());
		Log.e("checkFOrUpdate", "bufferShow: " + s);
		/* Get current Version Number */
		int curVersion = 0;
		try {
			curVersion = arg0[0].getPackageManager().getPackageInfo(
					arg0[0].getPackageName(), 0).versionCode;
		} catch (NameNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int newVersion = 0;
		try {
			newVersion = Integer.valueOf(s);
		} catch (Exception e) {
			Log.i("checkForUpdate", "not integer");
		}

		/* Is a higher version than the current already out? */
		if (newVersion > curVersion) 
			return new Boolean(true);
		else
			return new Boolean(false);
		
	}

	@Override
	protected void onPostExecute(Boolean result) {

		if(result.booleanValue() == true)
		{
			Log.e("AsyncUpdate", "newVersionFound");
			DialogUpdate cdd =  new DialogUpdate( context);
			cdd.show(); 
		}
		else
		Log.e("AsyncUpdate", "NoNewVersion");
	}

}
