package com.rangi.calendar.sync;

public class MyEvent {

	private long eventId;
	private long calendarId;
	private long startTime;
	private String title;

	public MyEvent(long calendarId, String title, long eventId) {
		setCalendarId(calendarId);
		setTitle(title);
		setEventId(eventId);
	}

	public void setEventId(long eventId) {
		this.eventId = eventId;
	}

	public long getEventId() {
		return eventId;
	}

	private void setTitle(String title) {
		this.title = title;
	}

	private void setCalendarId(long calendarId) {
		this.calendarId = calendarId;
	}

	public String getTitle() {
		return title;
	}

	public long getCalendarId() {
		return calendarId;
	}

	public void setStartTime(long l) {
		this.startTime = l;
	}

	public long getStartTime() {
		return startTime;
	}

	@Override
	public String toString() {
		return getTitle() + " | " + getCalendarId();
	}

}
