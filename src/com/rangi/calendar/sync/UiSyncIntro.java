package com.rangi.calendar.sync;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.rangi.calendar.SimpleActionBarActivity;
import com.rangi.calendar.settings.SettingsUtil;
import com.rangi.calendar.R;

public class UiSyncIntro extends SimpleActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setupActionBar(R.layout.action_bar_sync);
		setContentView(R.layout.activity_sync_intro);
		
		if (SettingsUtil.getCalendarId(this) != SettingsUtil.VALUE_NO_CALENDAR) {
			finish();
			goForSync(null);
		}
	}

	public void goForSync(View view) {
		Intent intent = new Intent(getApplicationContext(),
				UiSyncCalendar.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}
	
	public void goBack(View view) {
		onBackPressed();
	}

}
