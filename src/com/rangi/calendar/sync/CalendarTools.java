package com.rangi.calendar.sync;

import org.joda.time.LocalDate;

import android.accounts.Account;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Events;
import android.provider.CalendarContract.Reminders;
import java.util.ArrayList;
import java.util.Calendar;

public class CalendarTools {

	public static final String AUTHORITY = "rangirangi.calendar.synccalendar.provider";
	public static final String ACCOUNT_TYPE = "www.rangirangi.com";
	public static final String ACCOUNT_NAME = "user@rangirangi.com";
	public static final String CAL_NAME = "RANGI RANGI";
	public static String rangiRangiCalId = null;

	public static long addEvent(Context context, long calendarId, String title,
			long timeInMillis) {
		ContentValues event = new ContentValues();
		Uri eventUri = getEventUri();
		if (android.os.Build.VERSION.SDK_INT <= 7) {
			// the old way
			event.put("calendar_id", calendarId);
			event.put("title", title);
			event.put("description", "Event Desc");
			event.put("eventLocation", "Event Location");
			event.put("dtstart", timeInMillis);
			event.put("dtend", timeInMillis + 60 * 60 * 1000);
			event.put("allDay", 0);
			event.put("eventStatus", 1);
			// event.put(Events.EVENT_TIMEZONE,
			// cal.getTimeZone().getDisplayName());
			// event.put(Events.EVENT_END_TIMEZONE,
			// cal.getTimeZone().getDisplayName());
			event.put("hasAlarm", 0); // 0 for false, 1 for true
		} else {
			// the new way
			event.put(Events.CALENDAR_ID, calendarId);
			event.put(Events.TITLE, title);
			// event.put(Events.DESCRIPTION, "Event Desc");
			event.put(Events.DTSTART, timeInMillis);
			event.put(Events.DTEND, timeInMillis + 60 * 60 * 1000);
			event.put(Events.ALL_DAY, 0);
			event.put(Events.EVENT_TIMEZONE, Calendar.getInstance()
					.getTimeZone().getID());
			// event.put(Events.EVENT_END_TIMEZONE, cal.getTimeZone().getID());
			event.put(Events.STATUS, 1);
			event.put(Events.HAS_ALARM, 0); // 0 for false, 1 for true
		}
		Uri uri = context.getContentResolver().insert(eventUri, event);
		// if (android.os.Build.VERSION.SDK_INT > 8)
		try {
			syncCalendar(context, calendarId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ContentUris.parseId(uri);
	}

	private static final String AND_NOT_DELETED = " AND (deleted != 1)";

	public static MyEvent getEvent(Context context, long id) {
		String selection = "(" + CalendarContract.Events._ID + " = ?)"
				+ AND_NOT_DELETED;
		String[] selectionArgs = new String[] { "" + id };

		ContentResolver contentResolver = context.getContentResolver();
		Uri eventUri = getEventUri();
		Cursor cursr = contentResolver.query(eventUri,
				new String[] { "calendar_id", "title",
						CalendarContract.Events._ID, "dtstart" }, selection,
				selectionArgs, null);
		cursr.moveToFirst();
		MyEvent event = new MyEvent(cursr.getLong(0), cursr.getString(1),
				cursr.getLong(2));
		event.setStartTime(cursr.getLong(3));
		cursr.close();
		return event;
	}

	public static MyEvent[] getEventsOfDay(Context context, long calId,
			LocalDate date) {
		String selection = "((" + "calendar_id" + " = ?) AND (" + "dtstart"
				+ " >= ?) AND (" + "dtstart" + " < ?))" + AND_NOT_DELETED;
		String[] selectionArgs = new String[] { "" + calId,
				"" + date.toDateTimeAtStartOfDay().getMillis(),
				"" + date.plusDays(1).toDateTimeAtStartOfDay().getMillis() };

		ContentResolver contentResolver = context.getContentResolver();
		Uri eventUri = getEventUri();
		Cursor cursr = contentResolver.query(eventUri,
				new String[] { "calendar_id", "title",
						CalendarContract.Events._ID, "dtstart" }, selection,
				selectionArgs, null);
		cursr.moveToFirst();
		MyEvent[] events = new MyEvent[cursr.getCount()];
		for (int i = 0; i < events.length; i++) {
			events[i] = new MyEvent(cursr.getLong(0), cursr.getString(1),
					cursr.getLong(2));
			events[i].setStartTime(cursr.getLong(3));
			cursr.moveToNext();
		}
		cursr.close();
		return events;
	}

	public static MyEvent[] getEvents(Context context, long calId) {
		String selection = "((" + "calendar_id" + " = ?))" + AND_NOT_DELETED;
		String[] selectionArgs = new String[] { "" + calId };

		ContentResolver contentResolver = context.getContentResolver();
		Uri eventUri = getEventUri();
		Cursor cursr = contentResolver.query(eventUri,
				new String[] { "calendar_id", "title",
						CalendarContract.Events._ID, "dtstart" }, selection,
				selectionArgs, /* ORDER BY */"dtstart");
		cursr.moveToFirst();
		MyEvent[] events = new MyEvent[cursr.getCount()];
		for (int i = 0; i < events.length; i++) {
			events[i] = new MyEvent(cursr.getLong(0), cursr.getString(1),
					cursr.getLong(2));
			events[i].setStartTime(cursr.getLong(3));
			cursr.moveToNext();
		}
		cursr.close();
		return events;
	}

	public static void updateEvent(Context context, long id, String newTitle,
			long newTimeInMillis) {
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues();
		Uri updateUri = null;
		// values.put(Events.CALENDAR_ID, mEvent.getCalendarId());
		values.put(Events.TITLE, newTitle);
		values.put(Events.DTSTART, newTimeInMillis);
		values.put(Events.DTEND, newTimeInMillis + 60 * 60 * 1000);
		updateUri = ContentUris.withAppendedId(getEventUri(), id);
		cr.update(updateUri, values, null, null);
	}

	public static void deleteEvent(Context context, long id) {
		ContentResolver cr = context.getContentResolver();
		Uri deleteUri = ContentUris.withAppendedId(getEventUri(), id);
		cr.delete(deleteUri, null, null);
	}

	public static Uri getCalendarUri() {
		Uri eventUri;
		if (android.os.Build.VERSION.SDK_INT <= 7) {
			// the old way
			eventUri = Uri.parse("content://calendar/calendars");
		} else {
			// the new way
			eventUri = Uri.parse("content://com.android.calendar/calendars");
		}

		return eventUri;
	}

	public static Uri getEventUri() {
		Uri eventUri;
		if (android.os.Build.VERSION.SDK_INT <= 7) {
			// the old way
			eventUri = Uri.parse("content://calendar/events");
		} else {
			// the new way
			eventUri = Uri.parse("content://com.android.calendar/events");
		}
		return eventUri;
	}

	public static Uri getReminderUri() {
		Uri CALENDAR_REMINDER_URI;
		if (android.os.Build.VERSION.SDK_INT <= 7) {
			// the old way
			CALENDAR_REMINDER_URI = Uri.parse("content://calendar/reminders");
		} else {
			// the new way
			CALENDAR_REMINDER_URI = Uri
					.parse("content://com.android.calendar/reminders");
		}
		return CALENDAR_REMINDER_URI;
	}

	public static void addReminder(Context context, MyEvent myEvent) {
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues();
		values.put(Reminders.MINUTES, 15);
		values.put(Reminders.EVENT_ID, myEvent.getEventId());
		values.put(Reminders.METHOD, Reminders.METHOD_ALERT);
		Uri uri = cr.insert(getReminderUri(), values);
	}

	public static void syncCalendar(Context context, long calendarId) {
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues();
		values.put(CalendarContract.Calendars.SYNC_EVENTS, 1);
		values.put(CalendarContract.Calendars.VISIBLE, 1);

		cr.update(ContentUris.withAppendedId(getCalendarUri(), calendarId),
				values, null, null);
	}

	public static ArrayList<MyCalendar> getCalendars(Context mContext) {

		ArrayList<MyCalendar> mCalendars = new ArrayList<MyCalendar>();

		Uri calendars = getCalendarUri();
		String projection[] = { "_id", Calendars.CALENDAR_DISPLAY_NAME,
				Calendars.SYNC_EVENTS };

		ContentResolver contentResolver = mContext.getContentResolver();
		Cursor managedCursor;
		try {
			managedCursor = contentResolver.query(calendars, projection, null,
					null, null);

		} catch (Exception e) {
			projection[1] = "displayName";
			managedCursor = contentResolver.query(calendars, null, null, null,
					null);
			managedCursor.moveToFirst();
		}

		if (managedCursor.moveToFirst()) {
			String calName;
			int nameCalId = managedCursor.getColumnIndex(projection[0]);
			int nameCol = managedCursor.getColumnIndex(projection[1]);
			int nameSync = managedCursor.getColumnIndex(projection[2]);
			do {
				calName = managedCursor.getString(nameCol);
				String isSync = managedCursor.getString(nameSync);
				long calId = managedCursor.getLong(nameCalId);
				boolean synced;
				if (isSync.equals("0"))
					synced = false;
				else
					synced = true;

				mCalendars.add(new MyCalendar(calId, calName, synced));
			} while (managedCursor.moveToNext());
			managedCursor.close();
		}
		return mCalendars;
	}

	public static void deleteMyCalendar(Context mContext) {
		Account acc = new Account(ACCOUNT_NAME, ACCOUNT_TYPE);
		Uri calUri = getCalendarUri();
		Uri deleteUri = asSyncAdapter(calUri, acc.name, acc.type);
		mContext.getContentResolver().delete(deleteUri, null, null);
	}

	public static void createCalendar(Context mContext, String calendarName) {
		Account acc = new Account(ACCOUNT_NAME, ACCOUNT_TYPE);
		ContentResolver cr = mContext.getContentResolver();

		ContentValues values = new ContentValues();

		Uri calUri;
		if (android.os.Build.VERSION.SDK_INT <= 7) {
			// the old way
			calUri = Uri.parse("content://calendar/calendars");
			values.put("_sync_account", acc.name);
			values.put("_sync_account_type", acc.type);
			values.put("name", "rangirangi");
			values.put("displayName", calendarName);
			values.put("color", Color.GREEN);
			// values.put("access_level", Calendars.CAL_ACCESS_OWNER);
			values.put("ownerAccount", acc.name);

			values.put("sync_events", 1);
			values.put("hidden", 1);
		} else {
			// the new way
			calUri = Uri.parse("content://com.android.calendar/calendars");
			values.put(Calendars.ACCOUNT_NAME, acc.name);
			values.put(Calendars.ACCOUNT_TYPE, acc.type);
			values.put(Calendars.NAME, "rangirangi");
			values.put(Calendars.CALENDAR_DISPLAY_NAME, calendarName);
			values.put(Calendars.CALENDAR_COLOR, Color.GREEN);
			values.put(Calendars.CALENDAR_ACCESS_LEVEL,
					Calendars.CAL_ACCESS_OWNER);
			values.put(Calendars.OWNER_ACCOUNT, acc.name);

			values.put(Calendars.SYNC_EVENTS, 1);
			values.put(Calendars.VISIBLE, 1);
		}

		Uri creationUri = asSyncAdapter(calUri, acc.name, acc.type);
		Uri created = cr.insert(creationUri, values);
		Long.parseLong(created.getLastPathSegment());

		// Log.i("log_tag", "Calendar created: " + id + " - " + created);
	}

	public static boolean checkCalendarCreated(Context context, String calName) {
		ArrayList<MyCalendar> calList = getCalendars(context);
		for (int i = 0; i < calList.size(); i++) {
			if (calName.equals(calList.get(i).getCalName())) {
				// rangiRangiCalId = calList.get(i).getCalId();
				return true;
			}
		}
		return false;
	}

	public static Uri asSyncAdapter(Uri uri, String account, String accountType) {
		if (android.os.Build.VERSION.SDK_INT <= 7) {
			return uri.buildUpon()
					.appendQueryParameter("caller_is_syncadapter", "true")
					.appendQueryParameter("_sync_account", account)
					.appendQueryParameter("_sync_account_type", accountType)
					.build();
		} else {
			return uri
					.buildUpon()
					.appendQueryParameter(
							CalendarContract.CALLER_IS_SYNCADAPTER, "true")
					.appendQueryParameter(Calendars.ACCOUNT_NAME, account)
					.appendQueryParameter(Calendars.ACCOUNT_TYPE, accountType)
					.build();
		}
	}

}
