package com.rangi.calendar.sync;

import android.app.Service;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;

public class CalendarChangeService extends Service {

	public final static String CALENDAR_CHANGE = "rangirangi.calendar.synccalendar.CALENDAR_CHANGE";

	private final ContentObserver calendarsObserver = new ContentObserver(null) {

		@Override
		public void onChange(boolean selfChange) {
			super.onChange(selfChange);
			Log.d("LOG", "detected change in calendars: " + selfChange);
			sendBroadcast(new Intent(CALENDAR_CHANGE));
		}
	};

	@Override
	public void onCreate() {

		super.onCreate();
		Uri calUri;
		if (android.os.Build.VERSION.SDK_INT <= 7) {
			// the old way
			calUri = Uri.parse("content://calendar/calendars");
		} else {
			// the new way
			calUri = Uri.parse("content://com.android.calendar/calendars");
		}

		getContentResolver().registerContentObserver(calUri, true,
				calendarsObserver);
	}

	@Override
	public void onDestroy() {

		getContentResolver().unregisterContentObserver(calendarsObserver);

		super.onDestroy();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
}