package com.rangi.calendar.sync;

public class MyCalendar {

	private long calId;
	private String calName;
	private boolean shouldSync;

	public MyCalendar(long calId, String calName, boolean shouldSync) {
		setCalId(calId);
		setCalName(calName);
		setShouldSync(shouldSync);
	}

	public void setCalId(long calId) {
		this.calId = calId;
	}

	public long getCalId() {
		return calId;
	}

	public String getCalName() {
		return calName;
	}

	public boolean isShouldSync() {
		return shouldSync;
	}

	public void setCalName(String calName) {
		this.calName = calName;
	}

	public void setShouldSync(boolean shouldSync) {
		this.shouldSync = shouldSync;
	}

}
