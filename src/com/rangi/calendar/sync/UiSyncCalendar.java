package com.rangi.calendar.sync;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.rangi.calendar.SimpleActionBarActivity;
import com.rangi.calendar.MyDataTypes.Note;
import com.rangi.calendar.note.NoteManager;
import com.rangi.calendar.settings.SettingsUtil;
import com.rangi.calendar.utils.Utils;
import com.rangi.calendar.R;
import java.util.ArrayList;

public class UiSyncCalendar extends SimpleActionBarActivity {

	private long mCalendarId;

	private ListView calList;
	private CalendarsArrayAdapter mCalListAdapter;
	private ArrayList<MyCalendar> cals;

	private View mActionBarIcon;
	private Animation mRotateAnimation;

	private Handler mHandler = new Handler();

	private static final long FAKE_SYNCING_DURATION = 2000;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setupActionBar(R.layout.action_bar_sync);
		setContentView(R.layout.activity_sync_calendar);

		mActionBarIcon = findViewById(R.id.icon);
		mRotateAnimation = new RotateAnimation(0, 360,
				Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
				0.5f);
		mRotateAnimation.setDuration(FAKE_SYNCING_DURATION);

		mCalendarId = SettingsUtil.getCalendarId(this);

		calList = (ListView) findViewById(R.id.sync_calendar_list);
		cals = CalendarTools.getCalendars(this);
		mCalListAdapter = new CalendarsArrayAdapter(getApplicationContext(),
				cals);
		calList.setAdapter(mCalListAdapter);

		if (cals.size() == 0)
			showNoCalendarDialog();
	}

	public void syncAccount(View view) {
		mActionBarIcon.startAnimation(mRotateAnimation);
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				showCalendarSyncedDialog();
			}
		}, FAKE_SYNCING_DURATION);

		ArrayList<Note> notes = null;

		if (SettingsUtil.getCalendarId(getApplicationContext()) == SettingsUtil.VALUE_NO_CALENDAR) {
			notes = NoteManager.getInstance(getApplicationContext())
					.getAllNotes();
		}

		SettingsUtil.setCalendarId(getApplicationContext(), mCalendarId);

		if (notes != null)
			Utils.copyNotesToCalendar(getApplicationContext(), notes);

		NoteManager.invalidateWithNoteDatesSet(getApplicationContext());
	}

	private void showNoCalendarDialog() {
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_no_calendars);
		dialog.setCanceledOnTouchOutside(false);
		dialog.findViewById(R.id.btn_cancel).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						finish();
					}
				});
		dialog.findViewById(R.id.btn_ok).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						dialog.dismiss();
						startActivityForResult(new Intent(
								Settings.ACTION_ADD_ACCOUNT), 1);
					}
				});
		dialog.show();
	}

	private void showCalendarSyncedDialog() {
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_calendar_synced);
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 1 && resultCode == RESULT_OK) {
			ArrayList<MyCalendar> cals = CalendarTools.getCalendars(this);
			calList.setAdapter(new CalendarsArrayAdapter(
					getApplicationContext(), cals));
		} else if (cals.size() == 0) {
			finish();
		}
	}

	public void goBack(View view) {
		onBackPressed();
	}

	private class CalendarsArrayAdapter extends BaseAdapter {
		private ArrayList<MyCalendar> calendars;
		private LayoutInflater inflater = null;

		public CalendarsArrayAdapter(Context activity,
				ArrayList<MyCalendar> calendars) {
			this.calendars = calendars;
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		public int getCount() {
			return calendars.size();
		}

		public Object getItem(int position) {
			return calendars.get(position);
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View vi = convertView;
			if (convertView == null) {
				vi = inflater.inflate(R.layout.calendar_list_item, null);
			}

			ImageView selectedCalTick = (ImageView) vi
					.findViewById(R.id.calendar_list_item_img);

			if (mCalendarId == calendars.get(position).getCalId()) {
				selectedCalTick.setVisibility(View.VISIBLE);
			} else {
				selectedCalTick.setVisibility(View.INVISIBLE);
			}

			TextView title = (TextView) vi
					.findViewById(R.id.calendar_list_item_name);

			title.setText(calendars.get(position).getCalName());

			vi.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					mCalendarId = calendars.get(position).getCalId();
					mCalListAdapter.notifyDataSetChanged();
				}

			});

			return vi;
		}
	}

}
