package com.rangi.calendar.utils;

import com.rangi.calendar.SolarCalendar;
import com.rangi.calendar.MyDataTypes.Note;
import com.rangi.calendar.MyDataTypes.Reminder;
import com.rangi.calendar.database.DatabaseHelper;
import com.rangi.calendar.note.NoteManager;

import java.util.ArrayList;



import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;

public class Utils {

	private static Typeface FONT_YEKAN = null;
	private static Typeface FONT_TRADO = null;
	private static int SCREEN_WIDTH = 0;
	private static int SCREEN_HEIGHT = 0;

	public static float dipToPixels(Context context, float dipValue) {
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue,
				metrics);
	}

	public static float pixelsToDip(Context context, float px) {
		Resources resources = context.getResources();
		DisplayMetrics metrics = resources.getDisplayMetrics();
		float dp = px / (metrics.densityDpi / 160f);
		return dp;
	}

	public static void goToUrl(Context context, String url) {
		Uri uriUrl = Uri.parse(url);
		Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
		context.startActivity(launchBrowser);
	}

	public static int getScreenWidth(Context context) {
		if (SCREEN_WIDTH == 0) {
			WindowManager wm = (WindowManager) context
					.getSystemService(Context.WINDOW_SERVICE);
			Display display = wm.getDefaultDisplay();
			DisplayMetrics outMetrics = new DisplayMetrics();
			display.getMetrics(outMetrics);
			SCREEN_WIDTH = outMetrics.widthPixels;
		}
		return SCREEN_WIDTH;
	}

	public static double getScreenHeight(Context context) {
		if (SCREEN_HEIGHT == 0) {
			WindowManager wm = (WindowManager) context
					.getSystemService(Context.WINDOW_SERVICE);
			Display display = wm.getDefaultDisplay();
			DisplayMetrics outMetrics = new DisplayMetrics();
			display.getMetrics(outMetrics);
			SCREEN_HEIGHT = outMetrics.heightPixels;
		}
		return SCREEN_HEIGHT;
	}

	public static Typeface getCustomFont(Context context) {
		if (FONT_YEKAN == null)
			FONT_YEKAN = Typeface.createFromAsset(context.getAssets(),
					"fonts/Yekan.ttf");
		return FONT_YEKAN;
	}

	public static Typeface getCustomArabicFont(Context context) {
		if (FONT_TRADO == null)
			FONT_TRADO = Typeface.createFromAsset(context.getAssets(),
					"fonts/trado.ttf");
		return FONT_TRADO;
	}

	public static void repeatBackground(Drawable bg) {
		if (bg != null) {
			if (bg instanceof BitmapDrawable) {
				BitmapDrawable bmp = (BitmapDrawable) bg;
				bmp.mutate(); // make sure that we aren't sharing state anymore
				bmp.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
			}
		}
	}

	/**
	 * Does mapping from year, month and day to a single integer. For example if
	 * date of the note is 1393/2/29 it returns 13930229.
	 * 
	 * @return specified date as a single integer
	 */
	public static int getDateAsInteger(int year, int month, int day) {
		return year * 10000 + month * 100 + day;
	}

	/**
	 * See getDateAsInteger(int year, int month, int day)
	 * 
	 * @return specified month and day as a single integer
	 */
	public static int getDateAsInteger(int month, int day) {
		return month * 100 + day;
	}

	/**
	 * See getDateAsInteger(int year, int month, int day)
	 * 
	 * @return specified SolarCalendar as a single integer
	 */
	public static int getDateAsInteger(SolarCalendar date) {
		return getDateAsInteger(date.year, date.month, date.date);
	}

	/**
	 * See getDateAsInteger(int year, int month, int day)
	 * 
	 * @return specified note's date as a single integer
	 */
	public static int getDateAsInteger(Note note) {
		return getDateAsInteger(note.year, note.month, note.day);
	}

	/**
	 * Copies the notes into the calendar which is specified in
	 * SharedPreferences.
	 */
	public static void copyNotesToCalendar(Context context,
			ArrayList<Note> notes) {
		for (Note note : notes) {
			long newNoteId = NoteManager.getInstance(context).insertNote(note).id;

			Reminder reminder = DatabaseHelper.getDbHelper(context)
					.getNoteReminder(note.id);
			if (reminder != null)
				DatabaseHelper.getDbHelper(context).updateReminder(reminder.id,
						newNoteId);
		}
	}

}
