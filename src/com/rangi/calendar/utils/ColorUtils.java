package com.rangi.calendar.utils;

import org.joda.time.LocalDate;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import com.rangi.calendar.SolarCalendar;
import com.rangi.calendar.database.DatabaseHelper;
import com.rangi.calendar.R;

public class ColorUtils {

	private static int[][] dayColor = null;

	public static int getColor(Context context, int month, int position) {
		if (dayColor == null) {
			dayColor = DatabaseHelper.getDbHelper(context).getColors();
		}
		return dayColor[month][position];
	}

	public static int getDayBGColor(Context context, SolarCalendar date) {
		// shanbe is 0 and so on
		int firstDayWeekDay = (date.addDays(-(date.date - 1)).weekDay + 1) % 7;
		return getColor(context, date.month, date.date + firstDayWeekDay - 1);
	}

	public static int getDayBGColor(Context context, LocalDate date) {
		// shanbe is 0 and so on
		int firstDayWeekDay = date.withDayOfMonth(1).getDayOfWeek() - 1;
		return getColor(context, (date.getMonthOfYear() + 9) % 12 + 1,
				date.getDayOfMonth() + firstDayWeekDay - 1);
	}

	/*
	 * Sets icon color of today button in action bar. It can be modified and
	 * reused easily by specifying a mask which is black.
	 */
	public static BitmapDrawable setIconColor(Resources res, int color) {
		if (color == 0) {
			color = 0xffffffff;
		}
		Drawable maskDrawable = res.getDrawable(R.drawable.ic_mask_today);

		Bitmap maskBitmap = ((BitmapDrawable) maskDrawable).getBitmap();
		final int width = maskBitmap.getWidth();
		final int height = maskBitmap.getHeight();

		Bitmap outBitmap = Bitmap.createBitmap(width, height,
				Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(outBitmap);
		canvas.drawBitmap(maskBitmap, 0, 0, null);

		Paint maskedPaint = new Paint();
		maskedPaint.setColor(color);
		maskedPaint
				.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP));

		canvas.drawRect(0, 0, width, height, maskedPaint);

		BitmapDrawable outDrawable = new BitmapDrawable(res, outBitmap);
		return outDrawable;
	}

}
