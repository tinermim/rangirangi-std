package com.rangi.calendar;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.ViewGroup;

public abstract class SimpleActionBarActivity extends ActionBarActivity {

	protected void setupActionBar(int actionBarLayoutId) {
		final ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);

		final ViewGroup actionBarLayout = (ViewGroup) getLayoutInflater()
				.inflate(actionBarLayoutId, null);
		actionBar.setCustomView(actionBarLayout);
	}

	public void close(View view) {
		onBackPressed();
	}

}
