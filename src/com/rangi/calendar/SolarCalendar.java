package com.rangi.calendar;

import com.rangi.calendar.MyDataTypes.Note;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.Weeks;

public class SolarCalendar {

	public int weekDay;
	public String strWeekDay = "";
	public String strMonth = "";

	public int date;
	public int month;
	public int year;

	int miladiDate;
	int miladiMonth;
	int miladiYear;

	private static byte[] DAYS_OF_MONTH = { 31, 31, 31, 31, 31, 31, 30, 30, 30,
			30, 30, 29 };
	private static int GREGORIAN_DAYS_OF_MONTH[] = { 31, 28, 31, 30, 31, 30,
			31, 31, 30, 31, 30, 31 };
	private static String[] MONTHS = { "فروردین", "ارديبهشت", "خرداد", "تير",
			"مرداد", "شهريور", "مهر", "آبان", "آذر", "دي", "بهمن", "اسفند" };
	private static String[] GREGORIAN_MONTHS = { "ژانویه", "فوریه", "مارس",
			"آپریل", "می", "ژوئن", "جولای", "آگوست", "سپتامبر", "اکتبر",
			"نوامبر", "دسامبر" };
	private static String[] ISLAMIC_MONTHS = { "محرم", "صفر", "ربیع الاول",
			"ربیع الثانی", "جمادی الاول", "جمادی الثانی", "رجب", "شعبان",
			"رمضان", "شوال", "ذو القعده", "ذو الحجه" };
	private static String[] WEEKDAYS = { "دوشنبه", "سه شنبه", "چهارشنبه",
			"پنج شنبه", "جمعه", "شنبه", "يکشنبه" };

	@Override
	public String toString() {
		return date + " " + strMonth + " " + year;
	}

	public String toString(String format) {
		return format.replaceAll("y", "" + year).replaceAll("m", strMonth)
				.replaceAll("d", "" + date);
	}

	public boolean equals(SolarCalendar other) {
		return this.year == other.year && this.month == other.month
				&& this.date == other.date;
	}

	private static boolean isLeapYear(int year) {
		if ((year % 33 == 1 || year % 33 == 5 || year % 33 == 9
				|| year % 33 == 13 || year % 33 == 17 || year % 33 == 22
				|| year % 33 == 26 || year % 33 == 30)) {
			return true;
		} else
			return false;
	}

	public byte daysOfMonth() {
		if (month == 12 && isLeapYear(year))
			return 30;
		else
			return DAYS_OF_MONTH[month - 1];
	}

	public byte daysOfMonth(int month) {
		int year = this.year;
		if (month == 0) {
			year = this.year - 1;
			month = 12;
		}
		if (month == 12 && isLeapYear(year))
			return 30;
		else
			return DAYS_OF_MONTH[month - 1];
	}

	public static byte daysOfMonth(int year, int month) {
		if (month == 12 && isLeapYear(year))
			return 30;
		else
			return DAYS_OF_MONTH[month - 1];
	}

	public SolarCalendar() {
		LocalDate MiladiDate = new LocalDate();
		calcSolarCalendar(MiladiDate);
	}

	public SolarCalendar(LocalDate MiladiDate) {
		calcSolarCalendar(MiladiDate);
	}

	private void calcSolarCalendar(LocalDate MiladiDate) {

		int ld;

		miladiYear = MiladiDate.getYear();
		miladiMonth = MiladiDate.getMonthOfYear();
		miladiDate = MiladiDate.getDayOfMonth();
		weekDay = MiladiDate.getDayOfWeek();

		int[] buf1 = new int[12];
		int[] buf2 = new int[12];

		buf1[0] = 0;
		buf1[1] = 31;
		buf1[2] = 59;
		buf1[3] = 90;
		buf1[4] = 120;
		buf1[5] = 151;
		buf1[6] = 181;
		buf1[7] = 212;
		buf1[8] = 243;
		buf1[9] = 273;
		buf1[10] = 304;
		buf1[11] = 334;

		buf2[0] = 0;
		buf2[1] = 31;
		buf2[2] = 60;
		buf2[3] = 91;
		buf2[4] = 121;
		buf2[5] = 152;
		buf2[6] = 182;
		buf2[7] = 213;
		buf2[8] = 244;
		buf2[9] = 274;
		buf2[10] = 305;
		buf2[11] = 335;

		if ((miladiYear % 4) != 0) {
			date = buf1[miladiMonth - 1] + miladiDate;

			if (date > 79) {
				date = date - 79;
				if (date <= 186) {
					switch (date % 31) {
					case 0:
						month = date / 31;
						date = 31;
						break;
					default:
						month = (date / 31) + 1;
						date = (date % 31);
						break;
					}
					year = miladiYear - 621;
				} else {
					date = date - 186;

					switch (date % 30) {
					case 0:
						month = (date / 30) + 6;
						date = 30;
						break;
					default:
						month = (date / 30) + 7;
						date = (date % 30);
						break;
					}
					year = miladiYear - 621;
				}
			} else {
				if ((miladiYear > 1996) && (miladiYear % 4) == 1) {
					ld = 11;
				} else {
					ld = 10;
				}
				date = date + ld;

				switch (date % 30) {
				case 0:
					month = (date / 30) + 9;
					date = 30;
					break;
				default:
					month = (date / 30) + 10;
					date = (date % 30);
					break;
				}
				year = miladiYear - 622;
			}
		} else {
			date = buf2[miladiMonth - 1] + miladiDate;

			if (miladiYear >= 1996) {
				ld = 79;
			} else {
				ld = 80;
			}
			if (date > ld) {
				date = date - ld;

				if (date <= 186) {
					switch (date % 31) {
					case 0:
						month = (date / 31);
						date = 31;
						break;
					default:
						month = (date / 31) + 1;
						date = (date % 31);
						break;
					}
					year = miladiYear - 621;
				} else {
					date = date - 186;

					switch (date % 30) {
					case 0:
						month = (date / 30) + 6;
						date = 30;
						break;
					default:
						month = (date / 30) + 7;
						date = (date % 30);
						break;
					}
					year = miladiYear - 621;
				}
			}

			else {
				date = date + 10;

				switch (date % 30) {
				case 0:
					month = (date / 30) + 9;
					date = 30;
					break;
				default:
					month = (date / 30) + 10;
					date = (date % 30);
					break;
				}
				year = miladiYear - 622;
			}

		}

		strMonth = MONTHS[month - 1];
		strWeekDay = WEEKDAYS[weekDay - 1];

	}

	public static SolarCalendar offsetMonth(int offset) {
		return offsetMonth(new SolarCalendar(), offset);
	}

	public static SolarCalendar offsetMonth(SolarCalendar refrence, int offset) {
		if (offset == 0)
			return refrence;
		LocalDate date = new LocalDate(refrence.miladiYear,
				refrence.miladiMonth, refrence.miladiDate);
		if (refrence.date > 26)
			date = date.minusDays(4);
		if (refrence.date < 4)
			date = date.plusDays(4);
		int yearOffset = 0;
		while (date.getMonthOfYear() + offset > 11) {
			offset -= 12;
			yearOffset++;
		}
		while (date.getMonthOfYear() + offset < 0) {
			offset += 12;
			yearOffset--;
		}
		date = date.plusYears(yearOffset);
		date = date.plusMonths(offset);
		return new SolarCalendar(date);
	}

	public static SolarCalendar offsetYear(int offset) {
		return offsetYear(new SolarCalendar(), offset);
	}

	public static SolarCalendar offsetYear(SolarCalendar refrence, int offset) {
		if (offset == 0)
			return refrence;
		LocalDate date = new LocalDate(refrence.miladiYear,
				refrence.miladiMonth, refrence.miladiDate);
		if (refrence.month == 12)
			date = date.minusMonths(1);
		if (refrence.month == 1)
			date = date.plusMonths(1);

		date = date.plusYears(offset);
		return new SolarCalendar(date);
	}

	public static SolarCalendar offsetWeek(int offset) {
		LocalDate date = new LocalDate();
		date = date.plusWeeks(offset);
		return new SolarCalendar(date);
	}

	public static SolarCalendar offsetDay(int i) {
		LocalDate date = new LocalDate();
		date = date.plusDays(i);
		return new SolarCalendar(date);
	}

	public static int calculateYearOffset(SolarCalendar jDate) {
		SolarCalendar now = new SolarCalendar();
		return jDate.year - now.year;
	}

	public static int calculateMonthOffset(SolarCalendar jDate) {
		SolarCalendar now = new SolarCalendar();
		return (jDate.year - now.year) * 12 + (jDate.month - now.month);
	}

	public static long calculateWeekOffset(SolarCalendar STarget) {
		LocalDate target = new LocalDate(STarget.miladiYear,
				STarget.miladiMonth, STarget.miladiDate);
		LocalDate now = new LocalDate();
		return Weeks.weeksBetween(now, target).getWeeks();
	}

	public static long calculateDayOffset(LocalDate target) {
		LocalDate now = new LocalDate();
		return Days.daysBetween(now, target).getDays();
	}

	public static long calculateDayOffset(SolarCalendar date) {
		return calculateDayOffset(new LocalDate(date.miladiYear,
				date.miladiMonth, date.miladiDate));
	}

	public SolarCalendar addDays(int i) {
		if (i == 0)
			return this; // no need to add days
		LocalDate date = new LocalDate(this.miladiYear, this.miladiMonth,
				this.miladiDate);
		return new SolarCalendar(date.plusDays(i));
	}

	public SolarCalendar withFirstDayOfWeek() {
		return addDays(-((weekDay + 1) % 7));
	}

	public static String[] getMonthNames() {
		return MONTHS;
	}

	public static String[] getGregorianMonthNames() {
		return GREGORIAN_MONTHS;
	}

	public static String getFarsiMonthName(LocalDate date) {
		return GREGORIAN_MONTHS[date.getMonthOfYear() - 1];
	}

	public static String getIslamicMonthName(LocalDate date) {
		return ISLAMIC_MONTHS[date.getMonthOfYear() - 1];
	}

	/**
	 * Converts a Jalali date to the corresponding Gregorian date.
	 * 
	 * @jalaliMonth must be zero-based. FARVARDIN == 0, ORDIBEHESHT == 1, etc
	 */
	public static LocalDate jalaliToGregorian(int jalaliYear, int jalaliMonth,
			int jalaliDay) {
		int gregorianYear;
		int gregorianMonth;
		int gregorianDay;

		int gregorianDayNo, jalaliDayNo;
		int leap;

		int i;
		jalaliYear -= 979;
		jalaliDay -= 1;

		jalaliDayNo = 365 * jalaliYear + (int) (jalaliYear / 33.) * 8
				+ (int) Math.floor(((jalaliYear % 33) + 3) / 4);
		for (i = 0; i < jalaliMonth; ++i) {
			jalaliDayNo += DAYS_OF_MONTH[i];
		}

		jalaliDayNo += jalaliDay;

		gregorianDayNo = jalaliDayNo + 79;

		gregorianYear = 1600 + 400 * (int) Math.floor(gregorianDayNo / 146097);
		gregorianDayNo = gregorianDayNo % 146097;

		leap = 1;
		if (gregorianDayNo >= 36525) /* 36525 = 365*100 + 100/4 */{
			gregorianDayNo--;
			gregorianYear += 100 * (int) Math.floor(gregorianDayNo / 36524);
			gregorianDayNo = gregorianDayNo % 36524;

			if (gregorianDayNo >= 365) {
				gregorianDayNo++;
			} else {
				leap = 0;
			}
		}

		gregorianYear += 4 * (int) Math.floor(gregorianDayNo / 1461);
		gregorianDayNo = gregorianDayNo % 1461;

		if (gregorianDayNo >= 366) {
			leap = 0;

			gregorianDayNo--;
			gregorianYear += (int) Math.floor(gregorianDayNo / 365);
			gregorianDayNo = gregorianDayNo % 365;
		}

		for (i = 0; gregorianDayNo >= GREGORIAN_DAYS_OF_MONTH[i]
				+ ((i == 1 && leap == 1) ? i : 0); i++) {
			gregorianDayNo -= GREGORIAN_DAYS_OF_MONTH[i]
					+ ((i == 1 && leap == 1) ? i : 0);
		}
		gregorianMonth = i + 1;
		gregorianDay = gregorianDayNo + 1;

		return new LocalDate(gregorianYear, gregorianMonth, gregorianDay);
	}

	public static String getWeekday(int index) {
		return WEEKDAYS[(index + 5) % 7];
	}

	public int getRealWeekday() {
		return (weekDay + 1) % 7;
	}

	public static String noteDateToString(Note note) {
		return note.day + " " + MONTHS[note.month - 1] + " " + note.year;
	}

}
