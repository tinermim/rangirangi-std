package com.rangi.calendar;

import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.ArrayWheelAdapter;
import kankan.wheel.widget.adapters.NumericWheelAdapter;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.rangi.calendar.utils.Utils;

public class DatePickerDialog extends Dialog {

	private static final int WHEEL_YEAR_MIN = 1350;
	private static final int WHEEL_YEAR_MAX = 1450;
	public static final int A_LEAP_YEAR = 1391;

	private WheelView year;
	private WheelView month;
	private WheelView day;

	private int dateYear;
	private int dateMonth;
	private int dateDay;

	private boolean onlyMonthAndDay = false;

	private View.OnClickListener okButtonListener;
	// Default behavior for cancel button
	private View.OnClickListener cancelButtonListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			cancel();
		}
	};

	public DatePickerDialog(Context context, int year, int month, int day) {
		super(context);
		dateYear = year;
		dateMonth = month;
		dateDay = day;
	}

	public DatePickerDialog(Context context, int month, int day) {
		this(context, A_LEAP_YEAR, month, day);
		onlyMonthAndDay = true;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_date_picker);
		setCanceledOnTouchOutside(true);

		year = (WheelView) findViewById(R.id.year);
		month = (WheelView) findViewById(R.id.month);
		day = (WheelView) findViewById(R.id.day);

		OnWheelChangedListener listener = new OnWheelChangedListener() {
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				updateDays(year, month, day);
			}
		};

		year.setViewAdapter(new DateWheelAdapter(getContext(), WHEEL_YEAR_MIN,
				WHEEL_YEAR_MAX));
		year.setCurrentItem(dateYear - WHEEL_YEAR_MIN);
		year.addChangingListener(listener);

		month.setViewAdapter(new MonthWheelAdapter(getContext(), SolarCalendar
				.getMonthNames()));
		month.setCurrentItem(dateMonth);
		month.addChangingListener(listener);

		updateDays(year, month, day);
		day.setCurrentItem(dateDay - 1);

		findViewById(R.id.btn_cancel).setOnClickListener(cancelButtonListener);
		findViewById(R.id.btn_ok).setOnClickListener(okButtonListener);

		if (onlyMonthAndDay)
			year.setVisibility(View.GONE);
	}

	/**
	 * Updates day wheel. Sets max days according to selected month and year
	 */
	void updateDays(WheelView year, WheelView month, WheelView day) {
		int maxDays = SolarCalendar.daysOfMonth(year.getCurrentItem()
				+ WHEEL_YEAR_MIN, month.getCurrentItem() + 1);
		day.setViewAdapter(new DateWheelAdapter(getContext(), 1, maxDays));
		int curDay = Math.min(maxDays, day.getCurrentItem() + 1);
		day.setCurrentItem(curDay - 1, true);
	}

	private class DateWheelAdapter extends NumericWheelAdapter {

		public DateWheelAdapter(Context context, int minValue, int maxValue) {
			super(context, minValue, maxValue);
		}

		@Override
		protected void configureTextView(TextView view) {
			super.configureTextView(view);
			view.setTypeface(Utils.getCustomFont(context));
		}

	}

	private class MonthWheelAdapter extends ArrayWheelAdapter<String> {

		public MonthWheelAdapter(Context context, String[] items) {
			super(context, items);
		}

		@Override
		protected void configureTextView(TextView view) {
			super.configureTextView(view);
			view.setTypeface(Utils.getCustomFont(context));
		}

	}

	public void setOnCancelClickListener(View.OnClickListener listener) {
		cancelButtonListener = listener;
	}

	public void setOnOkClickListener(View.OnClickListener listener) {
		okButtonListener = listener;
	}

	public int getYear() {
		return year.getCurrentItem() + WHEEL_YEAR_MIN;
	}

	public int getMonth() {
		return month.getCurrentItem();
	}

	public int getDay() {
		return day.getCurrentItem() + 1;
	}

}
