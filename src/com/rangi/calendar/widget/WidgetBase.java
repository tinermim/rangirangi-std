package com.rangi.calendar.widget;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import android.widget.RemoteViews;

import com.rangi.calendar.settings.SettingsUtil;
import com.rangi.calendar.widget.config.WidgetConfigBase;

public abstract class WidgetBase extends AppWidgetProvider {

	private static final long INTERVAL_MINUTE = 1 * 60 * 1000; /* Milliseconds */

	/*
	 * Abstract methods:
	 */
	protected abstract String getUpdateAction();

	protected Calendar getFirstUpdateCalendar() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal;
	}

	protected long getUpdateInterval() {
		return INTERVAL_MINUTE;
	}

	protected abstract RemoteViews getRemoteViews(Context context, int widgetId);

	@Override
	public void onEnabled(Context context) {
		setUpdateAlarm(context);
		super.onEnabled(context);
	}

	@Override
	public void onDisabled(Context context) {
		removeUpdateAlarm(context);
		super.onDisabled(context);
	}

	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		Editor editor = SettingsUtil.getSettings(context).edit();
		for (int appWidgetId : appWidgetIds) {
			editor.remove(WidgetConfigBase.WIDGET_LAYOUT + appWidgetId);
			editor.remove(WidgetConfigBase.WIDGET_PRESET + appWidgetId);
		}
		editor.commit();
		super.onDeleted(context, appWidgetIds);
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		Log.d("RRP_WIDGET", this.getClass().getSimpleName() + " action: "
				+ action);
		if (action.equals(getUpdateAction())) {
			ComponentName thisWidget = new ComponentName(context,
					this.getClass());
			AppWidgetManager appWidgetManager = AppWidgetManager
					.getInstance(context);
			onUpdate(context, appWidgetManager,
					appWidgetManager.getAppWidgetIds(thisWidget));
		} else
			super.onReceive(context, intent);
	}

	private void setUpdateAlarm(Context context) {
		Calendar cal = getFirstUpdateCalendar();
		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(getUpdateAction());
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
				intent, 0);
		alarmManager.setRepeating(AlarmManager.RTC, cal.getTimeInMillis(),
				getUpdateInterval(), pendingIntent);
	}

	private void removeUpdateAlarm(Context context) {
		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(getUpdateAction());
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
				intent, 0);
		alarmManager.cancel(pendingIntent);
		pendingIntent.cancel();
	}

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		for (int widgetId : appWidgetIds) {
			RemoteViews remoteViews = getRemoteViews(context, widgetId);
			appWidgetManager.updateAppWidget(widgetId, remoteViews);
		}
	}

}