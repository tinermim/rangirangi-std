package com.rangi.calendar.widget;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.RemoteViews;

import com.rangi.calendar.DayView;
import com.rangi.calendar.settings.SettingsUtil;
import com.rangi.calendar.widget.config.WidgetConfigBase;
import com.rangi.calendar.widget.layout.Layout1x1;
import com.rangi.calendar.R;

public class Widget1x1 extends WidgetBase {

	public static final String UPDATE_ACTION = "com.rangi.calendar.WIDGET_1x1_UPDATE";

	@Override
	protected String getUpdateAction() {
		return UPDATE_ACTION;
	}

	@Override
	public RemoteViews getRemoteViews(Context context, int widgetId) {
		SharedPreferences prefs = SettingsUtil.getSettings(context);
		int layoutIndex = prefs.getInt(WidgetConfigBase.WIDGET_LAYOUT
				+ widgetId, 0);
		int presetId = prefs.getInt(WidgetConfigBase.WIDGET_PRESET + widgetId,
				0);
		return getRemoteViews(context, layoutIndex, presetId);
	}

	public static RemoteViews getRemoteViews(Context context, int layoutIndex,
			int presetId) {
		RemoteViews remoteViews = Layout1x1.applyLayout(context, layoutIndex,
				presetId);
		// Register an onClickListener
		Intent intent = new Intent(context, DayView.class);
		PendingIntent configPendingIntent = PendingIntent.getActivity(context,
				0, intent, 0);
		remoteViews.setOnClickPendingIntent(R.id.custom_part1,
				configPendingIntent);
		return remoteViews;
	}

}