package com.rangi.calendar.widget.config;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout.LayoutParams;
import android.widget.RemoteViews;

import com.rangi.calendar.utils.Utils;
import com.rangi.calendar.widget.Widget1x1;
import com.rangi.calendar.widget.layout.Layout1x1;

public class Config1x1 extends WidgetConfigBase {

	@Override
	protected int getLayoutsCount() {
		return Layout1x1.getLayoutsCount();
	}

	@Override
	protected String getUpdateAction() {
		return Widget1x1.UPDATE_ACTION;
	}

	@Override
	protected int getIcon(int position) {
		return Layout1x1.getIcon(position);
	}

	@Override
	protected RemoteViews getRemoteViews(int layoutIndex, int presetIndex) {
		return Layout1x1.applyLayout(this, layoutIndex, presetIndex);
	}

	@Override
	protected void setPreviewParams(Context context, View widgetPreview) {
		int width = (int) Utils.dipToPixels(context, 70);
		int height = (int) Utils.dipToPixels(context, 70);
		LayoutParams params = new LayoutParams(width, height);
		params.gravity = Gravity.CENTER;
		widgetPreview.setLayoutParams(params);
	}

}
