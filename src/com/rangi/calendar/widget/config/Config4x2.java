package com.rangi.calendar.widget.config;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout.LayoutParams;
import android.widget.RemoteViews;

import com.rangi.calendar.utils.Utils;
import com.rangi.calendar.widget.Widget4x2;
import com.rangi.calendar.widget.layout.Layout4x2;

public class Config4x2 extends WidgetConfigBase {

	@Override
	protected int getLayoutsCount() {
		return Layout4x2.getLayoutsCount();
	}

	@Override
	protected String getUpdateAction() {
		return Widget4x2.UPDATE_ACTION;
	}

	@Override
	protected int getIcon(int position) {
		return Layout4x2.getIcon(position);
	}

	@Override
	protected RemoteViews getRemoteViews(int layoutIndex, int presetIndex) {
		return Layout4x2.applyLayout(this, layoutIndex, presetIndex);
	}

	@Override
	protected void setPreviewParams(Context context, View widgetPreview) {
		int width = (int) Utils.dipToPixels(context, 250);
		int height = (int) Utils.dipToPixels(context, 140);
		LayoutParams params = new LayoutParams(width, height);
		params.gravity = Gravity.CENTER;
		widgetPreview.setLayoutParams(params);
	}
}
