package com.rangi.calendar.widget.config;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RemoteViews;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabContentFactory;

import com.rangi.calendar.customviews.MyTextView;
import com.rangi.calendar.settings.SettingsUtil;
import com.rangi.calendar.R;

public abstract class WidgetConfigBase extends ActionBarActivity implements
		OnTabChangeListener, OnPageChangeListener {

	/*
	 * Prefixes for putting indices into shared preferences.
	 */
	public static final String WIDGET_LAYOUT = "WIDGET_LAYOUT_INDEX|ID=";
	public static final String WIDGET_PRESET = "WIDGET_PRESET_INDEX|ID=";

	/*
	 * Keys for sending indices to fragment.
	 */
	public static final String LAYOUT_INDEX = "LAYOUT_INDEX";
	public static final String PRESET_INDEX = "PRESET_INDEX";

	MyPageAdapter pageAdapter;
	private ViewPager mViewPager;
	private TabHost mTabHost;
	private int tabTag;

	private static WidgetConfigBase config;
	private static int layoutIndex;
	private static int presetIndex;

	private int appWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;

	protected abstract int getLayoutsCount();

	protected abstract int getIcon(int position);

	protected abstract RemoteViews getRemoteViews(int layoutIndex,
			int presetIndex);

	protected abstract String getUpdateAction();

	protected abstract void setPreviewParams(Context context, View widgetPreview);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setResult(RESULT_CANCELED);
		setContentView(R.layout.activity_widget_config);
		setupActionBar();

		layoutIndex = 0;
		presetIndex = 4;

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		if (extras != null) {
			appWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID,
					AppWidgetManager.INVALID_APPWIDGET_ID);
		}
		if (appWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID)
			finish();

		mViewPager = (ViewPager) findViewById(R.id.viewpager);
		config = this;

		// Tab Initialization
		initialiseTabHost();

		// Fragments and ViewPager Initialization
		List<Fragment> fragments = getFragments();
		pageAdapter = new MyPageAdapter(getSupportFragmentManager(), fragments);
		mViewPager.setAdapter(pageAdapter);
		mViewPager.setOnPageChangeListener(this);
		mViewPager.setCurrentItem(pageAdapter.getCount() - 1);
		refreshPreview();
	}

	private void setupActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayShowTitleEnabled(false);
		View customActionBar = getLayoutInflater().inflate(
				R.layout.action_bar_widget_config, null);
		actionBar.setCustomView(customActionBar);
	}

	// Tabs Creation
	private void initialiseTabHost() {
		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup();

		addTab(R.string.widget_config_tab_0, R.drawable.ic_widgetsettings_color);
		addTab(R.string.widget_config_tab_1,
				R.drawable.ic_widgetsettings_layout);

		mTabHost.setOnTabChangedListener(this);
	}

	// Method to add a TabHost
	private void addTab(int titleId, int iconId) {
		TabHost.TabSpec tabSpec = mTabHost.newTabSpec("" + tabTag++);
		View tabIndicator = getLayoutInflater().inflate(
				R.layout.tab_indicator_holo, null);
		MyTextView title = (MyTextView) tabIndicator.findViewById(R.id.title);
		title.setText(titleId);
		title.setCompoundDrawablesWithIntrinsicBounds(0, 0, iconId, 0);
		tabSpec.setIndicator(tabIndicator);
		tabSpec.setContent(new MyTabFactory(this));
		mTabHost.addTab(tabSpec);
	}

	// Manages the Tab changes, synchronizing it with Pages
	public void onTabChanged(String tag) {
		int pos = this.mTabHost.getCurrentTab();
		this.mViewPager.setCurrentItem(pos);
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
	}

	// Manages the Page changes, synchronizing it with Tabs
	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		int pos = this.mViewPager.getCurrentItem();
		this.mTabHost.setCurrentTab(pos);
	}

	@Override
	public void onPageSelected(int arg0) {
	}

	private class MyPageAdapter extends FragmentPagerAdapter {
		private List<Fragment> fragments;

		public MyPageAdapter(FragmentManager fm, List<Fragment> fragments) {
			super(fm);
			this.fragments = fragments;
		}

		@Override
		public Fragment getItem(int position) {
			return this.fragments.get(position);
		}

		@Override
		public int getCount() {
			return this.fragments.size();
		}
	}

	private static class MyTabFactory implements TabContentFactory {

		private final Context mContext;

		public MyTabFactory(Context context) {
			mContext = context;
		}

		public View createTabContent(String tag) {
			View v = new View(mContext);
			v.setMinimumWidth(0);
			v.setMinimumHeight(0);
			return v;
		}
	}

	private List<Fragment> getFragments() {
		List<Fragment> fList = new ArrayList<Fragment>();

		Fragment f1 = new presetListFragment();
		Fragment f2 = new layoutListFragment();
		fList.add(f1);
		fList.add(f2);

		return fList;
	}

	public static class layoutListFragment extends Fragment {

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {

			ListView list = new ListView(getActivity());
			list.setAdapter(new layoutListAdapter(getActivity()));
			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					layoutIndex = position;
					config.refreshPreview();
				}
			});

			return list;
		}
	}

	public static class presetListFragment extends Fragment {

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {

			ListView list = new ListView(getActivity());
			list.setAdapter(new presetListAdapter(getActivity()));
			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					presetIndex = position;
					config.refreshPreview();
				}
			});

			return list;
		}
	}

	public static class layoutListAdapter extends BaseAdapter {

		private LayoutInflater inflater;

		public layoutListAdapter(Activity activity) {
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			return config.getLayoutsCount();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			if (convertView == null) {
				view = inflater.inflate(R.layout.widget_config_layout_list_row,
						null);
			}
			view.findViewById(R.id.icon).setBackgroundResource(
					config.getIcon(position));
			String title = config.getResources().getString(
					R.string.widget_config_layout_prefix)
					+ " " + (position + 1);
			((MyTextView) view.findViewById(R.id.title)).setText(title);
			return view;
		}

	}

	public static class presetListAdapter extends BaseAdapter {

		private static final int[] ICON_IDS = {
				R.drawable.img_widgetsetting_color1,
				R.drawable.img_widgetsetting_color2,
				R.drawable.img_widgetsetting_color3,
				R.drawable.img_widgetsetting_color4,
				R.drawable.img_widgetsetting_color5,
				R.drawable.img_widgetsetting_color6,
				R.drawable.img_widgetsetting_color7,
				R.drawable.img_widgetsetting_color8,
				R.drawable.img_widgetsetting_color9,
				R.drawable.img_widgetsetting_color10,
				R.drawable.img_widgetsetting_color11,
				R.drawable.img_widgetsetting_color12 };

		private LayoutInflater inflater;

		public presetListAdapter(Activity activity) {
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			return ICON_IDS.length;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			if (convertView == null) {
				view = inflater.inflate(R.layout.widget_config_layout_list_row,
						null);
			}
			view.findViewById(R.id.icon).setBackgroundResource(
					ICON_IDS[position]);
			String title = config.getResources().getString(
					R.string.widget_config_preset_prefix)
					+ " " + (position + 1);
			((MyTextView) view.findViewById(R.id.title)).setText(title);
			return view;
		}
	}

	public void refreshPreview() {
		FrameLayout previewFrame = (FrameLayout) findViewById(R.id.preview_frame);
		previewFrame.removeAllViews();
		RemoteViews remoteViews = getRemoteViews(layoutIndex, presetIndex);
		View widgetPreview = remoteViews.apply(this, null);
		setPreviewParams(this, widgetPreview);
		previewFrame.addView(widgetPreview);
	}

	public void buildWidget(View view) {
		Editor editor = SettingsUtil.getSettings(this).edit();
		editor.putInt(WIDGET_LAYOUT + appWidgetId, layoutIndex);
		editor.putInt(WIDGET_PRESET + appWidgetId, presetIndex);
		editor.commit();

		Intent resultValue = new Intent();
		resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
		setResult(RESULT_OK, resultValue);
		sendBroadcast(new Intent(getUpdateAction()));
		finish();
	}

}
