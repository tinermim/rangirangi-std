package com.rangi.calendar.widget.layout;

import java.util.HashMap;

import android.annotation.SuppressLint;
import com.rangi.calendar.R;
public class Preset {

	// @formatter:off
	private static final int[] customPartsIds = { R.id.custom_part1, R.id.custom_part2, R.id.custom_part3 };

	/* 
	 * A Preset initializer receives 2 int array.
	 * First array corresponds to background colors of customPartsIds. 
	 * For first preset:
	 * R.id.custom_part1 -> Color.WHITE
	 * R.id.custom_part2 -> 0xff5ba7dc
	 * R.id.custom_part3 -> 0xff82ca9c
	 * ---
	 * Second array corresponds to 2 changable text color of each widget.
	 * For first preset:
	 * Primary text color (and also today's weekday indicator) -> 0xff09b89d
	 * Secondary text color 								   -> 0xff818285
	 */
	
	private static final Preset[] presets = { 
		new Preset(new int[] { 0xffe7e7e7, 0xff5aa6da, 0xff83cb9d }, new int[] { 0xff66b49e, 0xff58585a }),
		new Preset(new int[] { 0xffe7e7e7, 0xfff89a1c, 0xffee3942 }, new int[] { 0xffc7232a, 0xff565658 }), 
		new Preset(new int[] { 0xffe9e2d0, 0xff9d8054, 0xff715d38 }, new int[] { 0xff4d360d, 0xff58595d }), 
		new Preset(new int[] { 0xffe9e2d0, 0xffdf8a75, 0xffea5d6e }, new int[] { 0xffb04654, 0xff5a5a58 }), 
		new Preset(new int[] { 0xffede9de, 0xff5fb8b2, 0xffed4880 }, new int[] { 0xffba296c, 0xff595959 }), 
		new Preset(new int[] { 0xff1b242d, 0xff368187, 0xff306064 }, new int[] { 0xff1c4549, 0xfffcfef9 }), 
		new Preset(new int[] { 0xffe7e7e7, 0xff1a6896, 0xffbf2e69 }, new int[] { 0xff991a4d, 0xff58585a }), 
		new Preset(new int[] { 0xff505c6a, 0xff1a9a67, 0xfff04d48 }, new int[] { 0xffd0231d, 0xfffffeff }), 
		new Preset(new int[] { 0xfffaf2cd, 0xfff9a126, 0xffe25925 }, new int[] { 0xffc2400f, 0xff565a5b }), 
		new Preset(new int[] { 0xffffffff, 0xffb0cd4d, 0xff14af9f }, new int[] { 0xff078e80, 0xff565658 }), 
		new Preset(new int[] { 0xffffffff, 0xff9bc73e, 0xff45a663 }, new int[] { 0xff337947, 0xff575759 }), 
		new Preset(new int[] { 0xfffaf2cd, 0xfffcc03e, 0xff1f90bc }, new int[] { 0xff196e93, 0xff555651 }), 
		};

	// @formatter:on
	public static Preset getPreset(int presetIndex) {
		return presets[presetIndex];
	}

	public static int getPresetsCount() {
		return presets.length;
	}

	@SuppressLint("UseSparseArrays")
	private HashMap<Integer, Integer> bgMap = new HashMap<Integer, Integer>();
	private int[] txtColors;

	private Preset(int[] bgColors, int[] txtColors) {
		for (int i = 0; i < customPartsIds.length; i++)
			bgMap.put(customPartsIds[i], bgColors[i]);
		this.txtColors = txtColors;
	}

	public HashMap<Integer, Integer> getBgMap() {
		return bgMap;
	}

	public int[] getTextColors() {
		return txtColors;
	}
}
