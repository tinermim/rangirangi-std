package com.rangi.calendar.widget.layout;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import android.content.Context;
import android.graphics.Color;
import android.widget.RemoteViews;

import com.rangi.calendar.SolarCalendar;
import com.rangi.calendar.R;

public class Layout1x1 extends LayoutBase {

	private static final int LAYOUT_A = R.layout.widget_1x1a_layout;
	private static final int LAYOUT_B = R.layout.widget_1x1b_layout;
	private static final int LAYOUT_C = R.layout.widget_1x1c_layout;

	private static final int[] layouts = { LAYOUT_A, LAYOUT_B, LAYOUT_C };
	
	private static final int[] ICON_IDS = {
		R.drawable.img_widgetsettings_layout1_1x1,
		R.drawable.img_widgetsettings_layout2_1x1,
		R.drawable.img_widgetsettings_layout3_1x1 };

	public static int getLayoutsCount() {
		return layouts.length;
	}

	public static RemoteViews applyLayout(Context context, int layoutIndex,
			int presetId) {
		int layoutId = layouts[layoutIndex];
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
				layoutId);
		Preset preset = Preset.getPreset(presetId);
		applyCustomBgColors(remoteViews, preset.getBgMap());
		int[] textColors = preset.getTextColors();

		SolarCalendar today = new SolarCalendar();
		LocalDate gToday = new LocalDate();

		switch (layoutId) {
		case LAYOUT_A:
		case LAYOUT_B:
			remoteViews.setTextViewText(R.id.gregorian_date,
					gToday.toString("dd MMM yyyy"));
			remoteViews.setTextColor(R.id.gregorian_date, textColors[1]);
			remoteViews.setImageViewBitmap(R.id.day,
					string2bitmap(context, "" + today.date, textColors[0], 40));
			remoteViews.setImageViewBitmap(R.id.month,
					string2bitmap(context, today.strMonth, Color.WHITE, 10));
			remoteViews.setImageViewBitmap(
					R.id.weekday,
					string2bitmap(context,
							String.valueOf(today.strWeekDay.charAt(0)),
							Color.WHITE, 10));
			break;
		case LAYOUT_C:
			remoteViews.setTextViewText(R.id.gday, "" + gToday.getDayOfMonth());
			remoteViews.setTextViewText(R.id.gmonth, gToday.monthOfYear()
					.getAsShortText().toUpperCase());
			remoteViews.setImageViewBitmap(R.id.jday,
					string2bitmap(context, "" + today.date, Color.WHITE, 20));
			remoteViews.setImageViewBitmap(R.id.jmonth,
					string2bitmap(context, today.strMonth, Color.WHITE, 10));
			remoteViews.setImageViewBitmap(
					R.id.clock,
					string2bitmap(context, LocalTime.now().toString("hh:mm"),
							textColors[1], 20));
			break;
		}

		return remoteViews;
	}

	public static int getIcon(int position) {
		return ICON_IDS[position];
	}

}
