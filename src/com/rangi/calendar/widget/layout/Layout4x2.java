package com.rangi.calendar.widget.layout;

import com.rangi.calendar.SolarCalendar;
import com.rangi.calendar.MyDataTypes.Note;
import com.rangi.calendar.database.DatabaseHelper;
import com.rangi.calendar.note.NoteManager;
import com.rangi.calendar.widget.WidgetComponents;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import android.content.Context;
import android.graphics.Color;
import android.widget.RemoteViews;

import com.rangi.calendar.R;

public class Layout4x2 extends LayoutBase {

	private static final int LAYOUT_A = R.layout.widget_4x2a_layout;
	private static final int LAYOUT_B = R.layout.widget_4x2b_layout;
	private static final int LAYOUT_C = R.layout.widget_4x2c_layout;

	private static final int[] layouts = { LAYOUT_A, LAYOUT_B, LAYOUT_C };

	private static final int[] ICON_IDS = {
			R.drawable.img_widgetsettings_layout1_4x2,
			R.drawable.img_widgetsettings_layout2_4x2,
			R.drawable.img_widgetsettings_layout3_4x2 };

	public static int getLayoutsCount() {
		return layouts.length;
	}

	public static RemoteViews applyLayout(Context context, int layoutIndex,
			int presetId) {
		int layoutId = layouts[layoutIndex];
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
				layoutId);
		Preset preset = Preset.getPreset(presetId);
		applyCustomBgColors(remoteViews, preset.getBgMap());
		int[] textColors = preset.getTextColors();

		SolarCalendar today = new SolarCalendar();
		LocalDate gToday = new LocalDate();

		DatabaseHelper dbHelper = DatabaseHelper.getDbHelper(context);
		Note[] notes = NoteManager.getInstance(context).getNotes(today.year,
				today.month, today.date);
		String eventTitle = dbHelper.getEventTitle(today.date, today.month);
		dbHelper.close();

		switch (layoutId) {
		case LAYOUT_A:
			remoteViews.setTextViewText(R.id.gday, "" + gToday.getDayOfMonth());
			remoteViews.setTextViewText(R.id.gweekday, gToday.dayOfWeek()
					.getAsText().toUpperCase());
			remoteViews.setTextViewText(R.id.gmonth, gToday.monthOfYear()
					.getAsText());
			remoteViews.setImageViewBitmap(R.id.jday,
					string2bitmap(context, "" + today.date, Color.WHITE, 40));
			remoteViews.setImageViewBitmap(R.id.jweekday,
					string2bitmap(context, today.strWeekDay, Color.WHITE, 20));
			remoteViews.setImageViewBitmap(R.id.jmonth,
					string2bitmap(context, today.strMonth, Color.WHITE, 15));

			if (notes == null || notes.length == 0)
				remoteViews.setImageViewBitmap(R.id.note, EMPTY_BITMAP);
			else
				remoteViews.setImageViewBitmap(R.id.note,
						notes2bitmap(context, notes, textColors[1], 15));
			remoteViews.setImageViewBitmap(R.id.event,
					string2bitmap(context, eventTitle, textColors[1], 15, 16));
			break;
		case LAYOUT_B:
			WidgetComponents.WeekdayBar.drawWeekdayInitials(context,
					remoteViews);
			remoteViews.setImageViewBitmap(R.id.widget_day,
					string2bitmap(context, "" + today.date, textColors[0], 40));
			remoteViews.setImageViewBitmap(R.id.widget_month,
					string2bitmap(context, today.strMonth, textColors[1], 10));
			remoteViews.setImageViewBitmap(R.id.year,
					string2bitmap(context, "" + today.year, textColors[1], 10));
			remoteViews.setImageViewBitmap(
					R.id.time,
					string2bitmap(context,
							"" + LocalTime.now().toString("hh:mm"),
							textColors[1], 40));

			remoteViews.setImageViewBitmap(R.id.widget_event,
					string2bitmap(context, eventTitle, Color.WHITE, 15, 32));
			WidgetComponents.WeekdayBar.refreshWeekdayBar(remoteViews, today,
					textColors[0]);
			break;
		case LAYOUT_C:
			remoteViews.setTextViewText(R.id.gday, "" + gToday.getDayOfMonth());
			remoteViews.setTextViewText(R.id.gmonthyear, gToday.monthOfYear()
					.getAsShortText().toUpperCase()
					+ "/" + gToday.getYear());
			remoteViews.setImageViewBitmap(R.id.jday,
					string2bitmap(context, "" + today.date, Color.WHITE, 35));
			remoteViews.setImageViewBitmap(R.id.jweekday,
					string2bitmap(context, today.strWeekDay, Color.WHITE, 15));
			remoteViews.setImageViewBitmap(R.id.jmonth,
					string2bitmap(context, today.strMonth, Color.WHITE, 10));

			if (notes == null || notes.length == 0)
				remoteViews.setImageViewBitmap(R.id.note, EMPTY_BITMAP);
			else
				remoteViews.setImageViewBitmap(R.id.note,
						notes2bitmap(context, notes, textColors[1], 15));
			remoteViews.setImageViewBitmap(R.id.event,
					string2bitmap(context, eventTitle, textColors[1], 15, 16));
			break;
		}

		return remoteViews;
	}

	public static int getIcon(int position) {
		return ICON_IDS[position];
	}

}
