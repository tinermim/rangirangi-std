package com.rangi.calendar.widget.layout;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import android.content.Context;
import android.graphics.Color;
import android.widget.RemoteViews;

import com.rangi.calendar.SolarCalendar;
import com.rangi.calendar.database.DatabaseHelper;
import com.rangi.calendar.widget.WidgetComponents;
import com.rangi.calendar.R;

public class Layout4x1 extends LayoutBase {

	private static final int LAYOUT_A = R.layout.widget_4x1a_layout;
	private static final int LAYOUT_B = R.layout.widget_4x1b_layout;
	private static final int LAYOUT_C = R.layout.widget_4x1c_layout;

	private static final int[] layouts = { LAYOUT_A, LAYOUT_B, LAYOUT_C };

	private static final int[] ICON_IDS = {
			R.drawable.img_widgetsettings_layout1_4x1,
			R.drawable.img_widgetsettings_layout2_4x1,
			R.drawable.img_widgetsettings_layout3_4x1 };

	public static int getLayoutsCount() {
		return layouts.length;
	}

	public static RemoteViews applyLayout(Context context, int layoutIndex,
			int presetId) {
		int layoutId = layouts[layoutIndex];
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
				layoutId);
		Preset preset = Preset.getPreset(presetId);
		applyCustomBgColors(remoteViews, preset.getBgMap());
		int[] textColors = preset.getTextColors();

		SolarCalendar today = new SolarCalendar();
		LocalDate gToday = new LocalDate();

		switch (layoutId) {
		case LAYOUT_A:
			remoteViews.setImageViewBitmap(R.id.jalali_day,
					string2bitmap(context, "" + today.date, Color.WHITE, 40));
			remoteViews.setImageViewBitmap(R.id.jalali_weekday,
					string2bitmap(context, today.strWeekDay, Color.WHITE, 9));
			remoteViews.setImageViewBitmap(R.id.jalali_month,
					string2bitmap(context, today.strMonth, Color.WHITE, 9));
			remoteViews.setImageViewBitmap(R.id.jalali_year,
					string2bitmap(context, "" + today.year, Color.WHITE, 10));
			remoteViews.setTextViewText(R.id.gregorian_day,
					"" + gToday.getDayOfMonth());
			remoteViews.setTextViewText(R.id.gregorian_weekday, gToday
					.dayOfWeek().getAsText());
			remoteViews.setTextViewText(R.id.gregorian_month, gToday
					.monthOfYear().getAsText());
			remoteViews.setTextViewText(R.id.gregorian_year,
					"" + gToday.getYear());
			break;
		case LAYOUT_B:
			WidgetComponents.WeekdayBar.drawWeekdayInitials(context,
					remoteViews);
			WidgetComponents.WeekdayBar.refreshWeekdayBar(remoteViews, today,
					textColors[0]);
			remoteViews.setImageViewBitmap(
					R.id.clock,
					string2bitmap(context,
							"" + LocalTime.now().toString("hh:mm"),
							Color.WHITE, 40));
			remoteViews.setImageViewBitmap(R.id.day,
					string2bitmap(context, "" + today.date, textColors[0], 40));
			remoteViews
					.setImageViewBitmap(
							R.id.weekday,
							string2bitmap(context, today.strWeekDay,
									textColors[1], 15));
			remoteViews.setImageViewBitmap(R.id.month,
					string2bitmap(context, today.strMonth, textColors[1], 15));
			break;
		case LAYOUT_C:
			WidgetComponents.WeekdayBar.drawWeekdayInitials(context,
					remoteViews);
			// updating the day card (day number and month name)
			remoteViews.setImageViewBitmap(R.id.widget_day,
					string2bitmap(context, "" + today.date, textColors[0], 40));
			remoteViews.setImageViewBitmap(R.id.widget_month,
					string2bitmap(context, today.strMonth, textColors[1], 20));

			// updating the event title
			DatabaseHelper dbHelper = DatabaseHelper.getDbHelper(context);
			String eventTitle = dbHelper.getEventTitle(today.date, today.month);
			dbHelper.close();
			remoteViews.setImageViewBitmap(R.id.widget_event,
					string2bitmap(context, eventTitle, Color.WHITE, 15, 20));
			WidgetComponents.WeekdayBar.refreshWeekdayBar(remoteViews, today,
					textColors[0]);
			break;
		}

		return remoteViews;
	}

	public static int getIcon(int position) {
		return ICON_IDS[position];
	}

}
