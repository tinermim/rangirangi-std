package com.rangi.calendar.widget.layout;

import com.rangi.calendar.MyDataTypes.Note;
import com.rangi.calendar.utils.Utils;

import java.util.HashMap;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Bitmap.Config;
import android.graphics.Paint.Align;
import android.widget.RemoteViews;

public abstract class LayoutBase {

	protected static final Bitmap EMPTY_BITMAP = Bitmap.createBitmap(1, 1,
			Config.ALPHA_8);
	private static final int MAX_NUM_LINES = 3;

	protected static void applyCustomBgColors(RemoteViews remoteViews,
			HashMap<Integer, Integer> bgMap) {
		for (Integer id : bgMap.keySet()) {
			remoteViews.setInt(id, "setBackgroundColor", bgMap.get(id));
		}
	}

	public static Bitmap string2bitmap(Context context, String line, int color,
			int dipSize) {
		return string2bitmap(context, new String[] { line }, color, dipSize);
	}

	public static Bitmap string2bitmap(Context context, String[] lines,
			int color, int dipSize) {
		float paddingX = 0.1f;
		float paddingY = 0.3f;
		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setSubpixelText(true);
		paint.setTypeface(Utils.getCustomFont(context));
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(color);
		paint.setTextAlign(Align.CENTER);
		paint.setTextSize(Utils.dipToPixels(context, dipSize));

		WidthHeightTuple maxLineBounds = getMaxTextBounds(paint, lines);

		Bitmap myBitmap = Bitmap.createBitmap(
				(int) (maxLineBounds.width * (1 + 2 * paddingX)),
				(int) (maxLineBounds.height * (1 + 2 * paddingY))
						* lines.length, Bitmap.Config.ARGB_8888);
		Canvas myCanvas = new Canvas(myBitmap);
		Rect textBounds = new Rect();

		int offsetY = 0;
		for (String line : lines) {
			int textLength = line.length();
			paint.getTextBounds(line, 0, textLength, textBounds);

			myCanvas.drawText(line, textBounds.width() * (.5f + paddingX),
					textBounds.height() + offsetY, paint);

			offsetY += -paint.ascent() + paint.descent();
		}

		return myBitmap;
	}

	private static class WidthHeightTuple {
		int width;
		int height;
	}

	private static WidthHeightTuple getMaxTextBounds(Paint paint, String[] lines) {
		Rect textBounds = new Rect();
		WidthHeightTuple maxBounds = new WidthHeightTuple();

		for (String line : lines) {
			paint.getTextBounds(line, 0, line.length(), textBounds);
			maxBounds.width = Math.max(maxBounds.width, textBounds.width());
			maxBounds.height = Math.max(maxBounds.height, textBounds.height());
		}

		// in case the note were empty
		if (maxBounds.width == 0)
			maxBounds.width = 1;
		if (maxBounds.height == 0)
			maxBounds.height = 1;

		return maxBounds;
	}

	public static Bitmap string2bitmap(Context context, String str, int color,
			int dipSize, int charsPerLine) {
		int textLength = str.length();
		if (textLength <= charsPerLine)
			return string2bitmap(context, str, color, dipSize);
		float paddingX = 0.3f;
		float paddingY = 0.3f;
		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setSubpixelText(true);
		paint.setTypeface(Utils.getCustomFont(context));
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(color);
		paint.setTextAlign(Align.CENTER);
		paint.setTextSize(Utils.dipToPixels(context, dipSize));
		Rect textBounds = new Rect();
		paint.getTextBounds(str, 0, charsPerLine, textBounds);
		int lines = textLength / charsPerLine + 1;
		lines = lines > MAX_NUM_LINES ? MAX_NUM_LINES : lines;
		Bitmap myBitmap = Bitmap.createBitmap(
				(int) (textBounds.width() * (1 + 2 * paddingX)),
				(int) (textBounds.height() * (lines + 1) * (1 + 2 * paddingY)),
				Bitmap.Config.ARGB_8888);
		Canvas myCanvas = new Canvas(myBitmap);
		int textY = textBounds.height();
		String lineBuffer = "";
		int numOfLines = 0;
		for (String word : str.split(" ")) {
			lineBuffer += word + " ";
			if (lineBuffer.length() >= charsPerLine) {
				myCanvas.drawText(lineBuffer, textBounds.width()
						* (.5f + paddingX), textY, paint);
				numOfLines++;
				lineBuffer = "";
				textY += -paint.ascent() + paint.descent();
			}
			if (numOfLines >= MAX_NUM_LINES) {
				myCanvas.drawText("...", textBounds.width() * (.5f + paddingX),
						textY, paint);
				break;
			}
		}
		return myBitmap;
	}

	public static Bitmap notes2bitmap(Context context, Note[] notes, int color,
			int dipSize) {

		String[] lines = new String[notes.length];
		for (int i = 0; i < notes.length; i++) {
			lines[i] = notes[i].note;
		}

		return string2bitmap(context, lines, color, dipSize);
	}

}
