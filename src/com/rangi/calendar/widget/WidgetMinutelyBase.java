package com.rangi.calendar.widget;

import java.util.Calendar;

public abstract class WidgetMinutelyBase extends WidgetBase {
	
	private static final long INTERVAL_MINUTE = 1 * 60 * 1000; /* Milliseconds */

	@Override
	protected Calendar getFirstUpdateCalendar() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal;
	}

	@Override
	protected long getUpdateInterval() {
		return INTERVAL_MINUTE;
	}

}
