package com.rangi.calendar.widget;

import java.util.Calendar;

import android.app.AlarmManager;

public abstract class WidgetDailyBase extends WidgetBase {

	@Override
	protected Calendar getFirstUpdateCalendar() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.add(Calendar.DATE, 1);
		return cal;
	}

	@Override
	protected long getUpdateInterval() {
		return AlarmManager.INTERVAL_DAY;
	}

}
