package com.rangi.calendar.widget;

import android.content.Context;
import android.graphics.Color;
import android.widget.RemoteViews;

import com.rangi.calendar.SolarCalendar;
import com.rangi.calendar.widget.layout.LayoutBase;
import com.rangi.calendar.R;

public class WidgetComponents {

	public static class WeekdayBar {

		private static final int[] WEEKDAY_IDS = { R.id.widget_w0,
				R.id.widget_w1, R.id.widget_w2, R.id.widget_w3, R.id.widget_w4,
				R.id.widget_w5, R.id.widget_w6 };
		private static final int[] WEEKDAY_FRAME_IDS = { R.id.frame_w0,
				R.id.frame_w1, R.id.frame_w2, R.id.frame_w3, R.id.frame_w4,
				R.id.frame_w5, R.id.frame_w6 };
		private static final int WEEKDAY_TEXT_COLOR = Color
				.parseColor("#cdead7");

		public static void drawWeekdayInitials(Context context,
				RemoteViews remoteViews) {
			for (int i = 0; i < WEEKDAY_IDS.length; i++) {
				remoteViews.setImageViewBitmap(WEEKDAY_IDS[i], LayoutBase
						.string2bitmap(context, String.valueOf(SolarCalendar
								.getWeekday(i).charAt(0)), WEEKDAY_TEXT_COLOR,
								10));
			}
		}

		public static void refreshWeekdayBar(RemoteViews remoteViews,
				SolarCalendar today, int todayWeekdayColor) {
			drawWeekdayBackground(remoteViews, today.getRealWeekday(), todayWeekdayColor);
			clearWeekdayBackground(remoteViews, getPreviousWeekday(today));
		}

		public static void clearWeekdayBackground(RemoteViews remoteViews,
				int weekday) {
			remoteViews.setInt(WEEKDAY_FRAME_IDS[weekday],
					"setBackgroundResource", R.color.transparent);
		}

		public static void drawWeekdayBackground(RemoteViews remoteViews,
				int weekday, int todayWeekdayColor) {
			remoteViews.setInt(WEEKDAY_FRAME_IDS[weekday],
					"setBackgroundColor", todayWeekdayColor);
		}

		public static int getPreviousWeekday(SolarCalendar today) {
			return (today.getRealWeekday() + 6) % 7;
		}

	}

}
