package com.rangi.calendar;

import java.util.HashSet;
import java.util.Set;

import org.joda.time.LocalDate;

import android.content.Context;
import android.util.SparseArray;

import com.rangi.calendar.database.DatabaseHelper;
import com.rangi.calendar.utils.Utils;

public class Cache {

	private static SparseArray<String> holidayEvents = null;
	private static String[][] dailyEventTitles = null;
	private static String[][] dailyEventDescriptions = null;
	private static Set<Integer> holidayDates = null;

	public static void cacheHolidayEvents(Context context) {
		// checking if current cache is holding the requested year events
		if (holidayEvents != null)
			return;
		holidayEvents = new SparseArray<String>();
		DatabaseHelper.getDbHelper(context).cacheHolidays(holidayEvents);
	}

	public static String getHolidayEvent(Context context, int year, int month,
			int day) {
		cacheHolidayEvents(context);
		return holidayEvents.get(Utils.getDateAsInteger(year, month, day));
	}

	public static void cacheEventTitles(Context context) {
		if (dailyEventTitles != null)
			return;
		dailyEventTitles = new String[13][32];
		DatabaseHelper.getDbHelper(context).cacheEventTitles(dailyEventTitles);
	}

	public static String getDailyEventTitle(Context context, int month, int day) {
		cacheEventTitles(context);
		return dailyEventTitles[month][day];
	}

	public static void cacheEventDescriptions(Context context) {
		if (dailyEventDescriptions != null)
			return;
		dailyEventDescriptions = new String[13][32];
		DatabaseHelper.getDbHelper(context).cacheEventDescriptions(
				dailyEventDescriptions);
	}

	public static String getDailyEventDescription(Context context, int month,
			int day) {
		cacheEventDescriptions(context);
		return dailyEventDescriptions[month][day];
	}

	public static void cacheHolidayDates(Context context) {
		if (holidayDates != null)
			return;
		holidayDates = new HashSet<Integer>();
		DatabaseHelper.getDbHelper(context).cacheHolidayDates(holidayDates);
	}

	public static boolean isHoliday(Context context, int year, int month,
			int day) {
		cacheHolidayDates(context);
		return holidayDates.contains(Utils.getDateAsInteger(year, month, day));
	}

	public static boolean isHoliday(Context context, LocalDate date) {
		SolarCalendar j = new SolarCalendar(date);
		return isHoliday(context, j.year, j.month, j.date);
	}

	public static boolean isHoliday(Context context, SolarCalendar date) {
		return isHoliday(context, date.year, date.month, date.date);
	}

}
