package com.rangi.calendar;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

/*
 * Here is the implementation of the dialog which is shown any where that we should ask the customer to buy
 * the related component(package). any pro feature which is not purchased will show this dialog, through which we handle the purchase 
 * process and do IAP
 */
public class DialogUpdate extends Dialog implements
		android.view.View.OnClickListener {

	public Activity c;
	public Dialog d;
	public Button yes, no;

	public DialogUpdate(Activity a) {
		super(a);
		this.c = a;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_update);
		yes = (Button) findViewById(R.id.btn_yes);
		no = (Button) findViewById(R.id.btn_no);
		yes.setOnClickListener(this);
		no.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_yes:

			dismiss();

			final String appPackageName = c.getPackageName(); // getPackageName()
			Log.e("checkFOrUpdate", appPackageName);
			try {

				c.startActivity(new Intent(Intent.ACTION_VIEW, Uri
						.parse("market://details?id=" + appPackageName)));
				Log.i("checkForUpdate", " in try block, going to market ");
			} catch (android.content.ActivityNotFoundException anfe) {
				Log.i("checkForUpdate", " exception occured, we in catch block");
				c.startActivity(new Intent(Intent.ACTION_VIEW, Uri
						.parse("http://cafebazaar.ir/app/" + appPackageName)));
			}

			break;
		case R.id.btn_no:
			dismiss();
			break;
		default:
			break;
		}

	}
}
