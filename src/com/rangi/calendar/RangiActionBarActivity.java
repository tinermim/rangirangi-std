package com.rangi.calendar;

import org.joda.time.LocalDate;
import org.joda.time.chrono.IslamicChronology;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.rangi.calendar.customviews.MyTextView;
import com.rangi.calendar.note.NoteView;
import com.rangi.calendar.settings.SettingsActivity;
import com.rangi.calendar.settings.SettingsUtil;
import com.rangi.calendar.utils.Utils;

/*
 * This is the parent of Store,DateConverter,Notes and Birthday Activities which are placed in the drawer activity(on the right of the 
 * screen). it actually set up the drawer and handle the intents between mentioned activities
 */

public class RangiActionBarActivity extends SimpleActionBarActivity {

	protected static final int HOME_PAGE = 0;
	protected static final int STORE = 1;
	protected static final int DATE_CONVERTER = 2;
	protected static final int NOTES = 3;
	protected static final int BIRTHDAYS = 4;
	protected static final int SETTINGS = 5;
	protected static final int HELP = 6;

	protected DrawerLayout mDrawerLayout;
	protected LinearLayout mDrawer;
	protected String[] menuItems;
	protected TypedArray menuItemsIcons;
	protected ListView mDrawerList;

	protected final Class<? extends RangiActionBarActivity> current = this
			.getClass();

	public void openMenu(View view) {
		toggleSideDrawer();
	}
	
	

	public void toggleSideDrawer() {
		if (!mDrawerLayout.isDrawerOpen(mDrawer))
			mDrawerLayout.openDrawer(mDrawer);
		else
			mDrawerLayout.closeDrawer(mDrawer);
	}

	@SuppressLint("Recycle")
	public void setupDrawer() {
		// setDrawerBackground();
		menuItems = getResources().getStringArray(R.array.menu_items);
		menuItemsIcons = getResources().obtainTypedArray(
				R.array.menu_items_icons);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawer = (LinearLayout) findViewById(R.id.right_drawer);
		mDrawerList = (ListView) findViewById(R.id.right_drawer_list);

		// Set the adapter for the list view
		mDrawerList.setAdapter(new DrawerArrayAdapter(this, menuItems,
				menuItemsIcons));
		// Set the list's click listener
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		addTodaysDate();
	}

	protected void addTodaysDate() {
		SolarCalendar jDate = new SolarCalendar();
		LocalDate gDate = new LocalDate();
		LocalDate iDate = new LocalDate(gDate.toDate(),
				IslamicChronology.getInstance());

		String jalali = jDate.date + " " + jDate.strMonth + " " + jDate.year;
		String gregorian = gDate.toString("d MMM yyyy");
		String islamic = iDate.getDayOfMonth() + " "
				+ SolarCalendar.getIslamicMonthName(iDate) + " "
				+ iDate.getYear();
		TextView primaryDate = (TextView) findViewById(R.id.primary_date);
		TextView rightSecondaryDate = (TextView) findViewById(R.id.right_secondary_date);
		if (SettingsUtil.primaryCalendarIsJalali(this)) {
			primaryDate.setTypeface(Utils.getCustomFont(this));
			primaryDate.setText(jalali);
			rightSecondaryDate.setText(gregorian);
		} else {
			primaryDate.setText(gregorian);
			rightSecondaryDate.setTypeface(Utils.getCustomFont(this));
			rightSecondaryDate.setText(jalali);
		}
		((TextView) findViewById(R.id.left_secondary_date)).setText(islamic);
	}

	protected void goToNoteView() {
		if (!PurchaseControl.getInstance(this).isPurchased(PurchaseComponent.PURCHASE_NOTES)) {
			DialogPurchase cdd = new DialogPurchase(this,
					PurchaseComponent.PURCHASE_NOTES);
			cdd.show();
		} else
			startActivity(new Intent(this, NoteView.class));
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// Pressing physical menu button should toggle the side drawer.
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			toggleSideDrawer();
			return true;
		}
		// Other keys are handled by system.
		return super.onKeyDown(keyCode, event);
	}

	protected class DrawerArrayAdapter extends BaseAdapter {
		private Activity activity;
		private String[] titles;
		private TypedArray icons;
		private LayoutInflater inflater = null;

		public DrawerArrayAdapter(Activity activity, String[] titles,
				TypedArray icons) {
			this.activity = activity;
			this.titles = titles;
			this.icons = icons;
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		public int getCount() {
			return titles.length;
		}

		public Object getItem(int position) {
			return titles[position];
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View vi = convertView;
			if (convertView == null) {
				vi = inflater.inflate(R.layout.drawer_item, null);
				if (position != 5 && position != 4)
					vi.setBackgroundResource(R.drawable.selector_list);
				else
					vi.setBackgroundResource(R.drawable.selector_list_bottom);
			}

			MyTextView title = (MyTextView) vi.findViewById(R.id.title);
			ImageView icon = (ImageView) vi.findViewById(R.id.icon);

			title.setText(titles[position]);
			switch (position) {
			case HOME_PAGE:
				title.setTextColor(getResources().getColor(
						R.color.menu_home_title));
				break;
				// TODO
			case STORE:
				title.setTextColor(getResources().getColor(
						R.color.menu_store_title));
				break;

			case DATE_CONVERTER:
				title.setTextColor(getResources().getColor(
						R.color.menu_converter_title));
				break;

			case NOTES:
				title.setTextColor(getResources().getColor(
						R.color.menu_notes_title));
				break;
			case BIRTHDAYS:
				title.setTextColor(getResources().getColor(
						R.color.menu_birthdays_title));
				break;

			default:
				title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
				title.setTextColor(Color.parseColor("#575757"));
				break;
			}
			icon.setImageDrawable(icons.getDrawable(position));

			return vi;
		}
	}

	protected class DrawerItemClickListener implements
	ListView.OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			mDrawerLayout.closeDrawer(mDrawer);

			Log.i("drawerIntent", current + " " + position);

			if ((current.equals(MonthView.class) && position == HOME_PAGE)
					|| (current.equals(Store.class) && position == STORE)
					|| (current.equals(DateConverterActivity.class) && position == DATE_CONVERTER)
					|| (current.equals(NoteView.class) && position == NOTES)
					|| (current.equals(BirthdayView.class) && position == BIRTHDAYS)
					|| (current.equals(IntroActivity.class) && position == HELP))
				return;

			
			switch (position) {
			case HOME_PAGE:
				startActivity(new Intent(RangiActionBarActivity.this,
						MonthView.class)
				.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
				break;

				// TODO
			case STORE:
				startActivity(new Intent(RangiActionBarActivity.this,
						Store.class));
				break;

			case DATE_CONVERTER:
				startActivity(new Intent(RangiActionBarActivity.this,
						DateConverterActivity.class));
				break;
			case NOTES:
				goToNoteView();
				break;
			case BIRTHDAYS:
				startActivity(new Intent(RangiActionBarActivity.this,
						BirthdayView.class));
				break;
			case SETTINGS:
				startActivity(new Intent(RangiActionBarActivity.this,
						SettingsActivity.class));
				break;
			case HELP:
				startActivity(new Intent(RangiActionBarActivity.this,
						IntroActivity.class));
				break;
			}

		}
	}
}
