package com.rangi.calendar;
//package com.rangi.calendar;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import android.app.Activity;
//import android.content.ContentUris;
//import android.content.Context;
//import android.content.Intent;
//import android.database.Cursor;
//import android.net.Uri;
//import android.os.Bundle;
//import android.provider.ContactsContract;
//import android.provider.ContactsContract.PhoneLookup;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.AdapterView.OnItemClickListener;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.TextView;
//
//public class ContactSelectActivity extends SimpleActionBarActivity {
//
//	public static final String CONTACT_NAME = "CONTACT_NAME";
//	public static final String CONTACT_PHOTO = "CONTACT_PHOTO";
//	private ListView contactsList;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_contact_select);
//
//		final List<Contact> contacts = getContactsList();
//		contactsList = (ListView) findViewById(R.id.contacts_list);
//		contactsList.setAdapter(new ContactsAdapter(this, contacts));
//		contactsList.setOnItemClickListener(new OnItemClickListener() {
//
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view,
//					int position, long id) {
//				Intent data = new Intent();
//				Contact contact = contacts.get(position);
//				data.putExtra(CONTACT_NAME, contact.name);
//				data.putExtra(CONTACT_PHOTO, contact.photo == null ? null
//						: contact.photo.toString());
//				setResult(RESULT_OK, data);
//				finish();
//			}
//		});
//
//	}
//
//	private class ContactsAdapter extends BaseAdapter {
//
//		private List<Contact> contacts;
//		private Context context;
//
//		public ContactsAdapter(Context context, List<Contact> contacts) {
//			this.contacts = contacts;
//			this.context = context;
//		}
//
//		@Override
//		public int getCount() {
//			return contacts.size();
//		}
//
//		@Override
//		public Object getItem(int position) {
//			return contacts.get(position);
//		}
//
//		@Override
//		public long getItemId(int position) {
//			return position;
//		}
//
//		/* private view holder class */
//		private class ViewHolder {
//			TextView name;
//			ImageView photo;
//		}
//
//		@Override
//		public View getView(int position, View convertView, ViewGroup parent) {
//			ViewHolder holder = null;
//
//			LayoutInflater mInflater = (LayoutInflater) context
//					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
//			if (convertView == null) {
//				convertView = mInflater.inflate(
//						R.layout.list_row_contact_select, null);
//				holder = new ViewHolder();
//				holder.name = (TextView) convertView.findViewById(R.id.name);
//				holder.photo = (ImageView) convertView.findViewById(R.id.photo);
//				convertView.setTag(holder);
//			} else {
//				holder = (ViewHolder) convertView.getTag();
//			}
//
//			Contact contact = (Contact) getItem(position);
//
//			holder.name.setText(contact.name);
//			holder.photo.setImageURI(contact.photo);
//
//			return convertView;
//		}
//
//	}
//
//	private class Contact {
//		private String name;
//		private Uri photo;
//	}
//
//	private List<Contact> getContactsList() {
//		List<Contact> contacts = new ArrayList<Contact>();
//		Cursor cursor = getContentResolver().query(
//				ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
//
//		while (cursor.moveToNext()) {
//			Contact contact = new Contact();
//			String contactId = cursor.getString(cursor
//					.getColumnIndex(ContactsContract.Contacts._ID));
//			contact.name = cursor.getString(cursor
//					.getColumnIndex(PhoneLookup.DISPLAY_NAME));
//			contact.photo = getPhotoUriFromID(contactId);
//			contacts.add(contact);
//		}
//
//		cursor.close();
//		return contacts;
//	}
//
//	private Uri getPhotoUriFromID(String id) {
//		try {
//			Cursor cur = getContentResolver()
//					.query(ContactsContract.Data.CONTENT_URI,
//							null,
//							ContactsContract.Data.CONTACT_ID
//									+ "="
//									+ id
//									+ " AND "
//									+ ContactsContract.Data.MIMETYPE
//									+ "='"
//									+ ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE
//									+ "'", null, null);
//			if (cur != null) {
//				if (!cur.moveToFirst()) {
//					return null; // no photo
//				}
//			} else {
//				return null; // error in cursor process
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			return null;
//		}
//		Uri person = ContentUris.withAppendedId(
//				ContactsContract.Contacts.CONTENT_URI, Long.parseLong(id));
//		return Uri.withAppendedPath(person,
//				ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
//	}
//
//}
