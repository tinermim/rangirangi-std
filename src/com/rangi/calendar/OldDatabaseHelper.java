package com.rangi.calendar;

import java.io.File;
import java.util.ArrayList;

import com.rangi.calendar.MyDataTypes.Note;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class OldDatabaseHelper extends SQLiteOpenHelper {

	// The Android's default system path of your application database.
	public static String DB_PATH = "/data/data/com.rangi.calendar/databases/";

	public static String DB_NAME = "database.db";

	private static OldDatabaseHelper dbHelper;

	private SQLiteDatabase myDataBase;

	private final Context myContext;

	/**
	 * Constructor Takes and keeps a reference of the passed context in order to
	 * access to the application assets and resources.
	 * 
	 * @param context
	 * @return
	 */
	public OldDatabaseHelper(Context context) {
		super(context, DB_NAME, null, 1);
		this.myContext = context;
	}

	/**
	 * Check if the database already exist to avoid re-copying the file each
	 * time you open the application.
	 * 
	 * @return true if it exists, false if it doesn't
	 */
	public static boolean exists() {
		File dbFile = new File(DB_PATH + DB_NAME);
		return dbFile.exists();
	}

	public static void delete() {
		File dbFile = new File(DB_PATH + DB_NAME);
		dbFile.delete();
	}

	public void openDataBase() throws SQLException {

		// Open the database
		String myPath = DB_PATH + DB_NAME;
		myDataBase = SQLiteDatabase.openDatabase(myPath, null,
				SQLiteDatabase.OPEN_READWRITE);
	}

	@Override
	public synchronized void close() {

		if (myDataBase != null)
			myDataBase.close();

		super.close();

	}

	@Override
	public void onCreate(SQLiteDatabase db) {

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	// Add your public helper methods to access and get content from the
	// database.
	// You could return cursors by doing "return myDataBase.query(....)" so it'd
	// be easy
	// to you to create adapters for your views.

	public static OldDatabaseHelper getDbHelper(Context context) {
		if (dbHelper == null) {
			dbHelper = new OldDatabaseHelper(context);
			try {
				dbHelper.openDataBase();
			} catch (SQLException sqle) {
				throw sqle;
			}
		} else if (!dbHelper.isOpen())
			dbHelper.openDataBase();

		return dbHelper;
	}

	public boolean isOpen() {
		return myDataBase.isOpen();
	}

	public ArrayList<Note> getAllNotes() {
		ArrayList<Note> list = new ArrayList<Note>();
		Cursor cursor = myDataBase.rawQuery("SELECT * FROM notes", null);
		while (cursor.moveToNext()) {
			Note note = new Note(cursor.getInt(cursor.getColumnIndex("_id")),
					cursor.getInt(cursor.getColumnIndex("year")),
					cursor.getInt(cursor.getColumnIndex("month")),
					cursor.getInt(cursor.getColumnIndex("day")),
					cursor.getString(cursor.getColumnIndex("note")));
			list.add(note);
		}
		cursor.close();
		return list;
	}

}
