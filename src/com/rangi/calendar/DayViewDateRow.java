/*
 * File: DayViewDateRow.java
 * -------------------------
 * A custom view for showing the current page date in a day view.
 * It draws the primary date on right hand side and secondry dates
 * on left hand side using canvas.
 */

package com.rangi.calendar;

import org.joda.time.LocalDate;
import org.joda.time.chrono.IslamicChronology;

import com.rangi.calendar.utils.Utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.Paint.Style;
import android.view.View;

public class DayViewDateRow extends View {

	private static int[] PRIMARY_DATE_FONT_COLOR = { 0x009248, 0x00975a,
			0xee5273, 0xe15170, 0xd4447b, 0xff8e40, 0xff5743, 0xf06733,
			0xff8b57, 0x00a9c1, 0x00c092, 0x76af69 };

	public static final int JALALI = 0;
	public static final int ISLAMIC = 1;
	public static final int GREGORIAN = 2;

	private Context context;
	private int width;
	private int height;
	private int circleMargin;
	private Paint paint;

	private int primaryDateYPos;
	private Typeface primaryDateTypeFace;
	private int primaryDateTextColor;

	private LocalDate date;
	private int dayNumber;
	private String monthAndYear;
	private String weekDay;
	private String topSecondaryDate;
	private String bottomSecondaryDate;
	private boolean isHoliday;

	private Typeface bottomSecondaryDateTypeFace;

	public DayViewDateRow(Context context, int width, int height) {
		super(context);

		this.context = context;
		this.width = width;
		this.height = height;
		this.circleMargin = height / 10;
		this.paint = new Paint();
		paint.setAntiAlias(true);
	}

	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		// condition to show the purchased items
		// if not purchased we won't show other dates on top left corner of Day view
		drawPrimaryDate(canvas);
		if(PurchaseControl.getInstance(context).isPurchased(PurchaseComponent.PURCHASE_GEROGRIAN_HIJRI_CALENDAR) ){
				drawTopSecondaryDate(canvas);
				drawSeparator(canvas);
				drawBottomSecondaryDate(canvas);
		}
	}

	private void drawSeparator(Canvas canvas) {
		paint.setColor(Color.DKGRAY);
		canvas.drawLine(height / 4, height / 1.9f, width / 4 + height / 4,
				height / 1.9f, paint);
	}

	private void drawBottomSecondaryDate(Canvas canvas) {
		paint.setTextAlign(Paint.Align.LEFT);
		paint.setTypeface(bottomSecondaryDateTypeFace);
		paint.setColor(Color.BLACK);
		paint.setTextSize(width / 30);
		canvas.drawText(bottomSecondaryDate, height / 4, 6 * height / 8, paint);
	}

	private void drawTopSecondaryDate(Canvas canvas) {
		paint.setTextAlign(Paint.Align.LEFT);
		paint.setTypeface(Utils.getCustomFont(context));
		paint.setColor(Color.BLACK);
		paint.setTextSize(width / 26);
		canvas.drawText(topSecondaryDate, height / 4, 3 * height / 8, paint);
	}

	/*
	 * Draws the primary date (jalali or gregorian) with day number on the right
	 * side, month and year on top and week day name on the bottom
	 */
	private void drawPrimaryDate(Canvas canvas) {
		if (isHoliday)
			drawHolidayCircle(canvas);

		paint.setTypeface(primaryDateTypeFace);
		paint.setColor(primaryDateTextColor);
		paint.setTextAlign(Paint.Align.CENTER);
		paint.setTextSize(width / 8);
		canvas.drawText("" + dayNumber, width - height / 2, primaryDateYPos,
				paint);
		/*
		 * The rest of the date would be printed with Farsi font, regardless of
		 * its type.
		 */
		paint.setTypeface(Utils.getCustomFont(context));
		paint.setTextAlign(Paint.Align.RIGHT);
		paint.setTextSize(width / 20);
		canvas.drawText(monthAndYear, width - height, 3 * height / 8, paint);

		paint.setTextSize(width / 15);
		canvas.drawText(weekDay, width - height, 6 * height / 8, paint);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		setMeasuredDimension(width, height);
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	/*
	 * Sets the texts contents and some other properties like font to show the
	 * primary calendar better.
	 */
	public void setPrimaryDate(int dateType) {
		SolarCalendar jDate = new SolarCalendar(date);
		switch (dateType) {
		case JALALI:
			this.dayNumber = jDate.date;
			this.monthAndYear = jDate.year + " " + jDate.strMonth;
			this.weekDay = jDate.strWeekDay;
			primaryDateYPos = 5 * height / 8;
			primaryDateTypeFace = Utils.getCustomFont(context);
			primaryDateTextColor = 0xff000000 + PRIMARY_DATE_FONT_COLOR[jDate.month - 1];
			break;
		case GREGORIAN:
			this.dayNumber = date.getDayOfMonth();
			this.monthAndYear = date.getYear() + " "
					+ SolarCalendar.getFarsiMonthName(date);
			this.weekDay = jDate.strWeekDay;
			primaryDateYPos = 6 * height / 8;
			primaryDateTypeFace = null;
			primaryDateTextColor = 0xff000000 + PRIMARY_DATE_FONT_COLOR[(date
					.getMonthOfYear() + 9) % 12];
			break;
		}
	}

	public void setTopSecondaryDate(int dateType) {
		/*
		 * In this version top secondary date is islamic calendar.
		 */
		LocalDate islamicDate = new LocalDate(date.toDate(),
				IslamicChronology.getInstance());
		topSecondaryDate = islamicDate.getYear()
				+ SolarCalendar.getIslamicMonthName(islamicDate) + " "
				+ islamicDate.getDayOfMonth();
	}

	public void setBottomSecondaryDate(int dateType) {
		switch (dateType) {
		case JALALI:
			SolarCalendar jDate = new SolarCalendar(date);
			bottomSecondaryDate = jDate.year + jDate.strMonth + " "
					+ jDate.date;
			bottomSecondaryDateTypeFace = Utils.getCustomFont(context);
			break;
		case GREGORIAN:
			bottomSecondaryDate = date.toString("d MMM yyyy");
			bottomSecondaryDateTypeFace = null;
		}
	}

	public void setIsHoliday(boolean isHoliday) {
		this.isHoliday = isHoliday;
	}

	private void drawHolidayCircle(Canvas canvas) {
		paint.setColor(0x0f000000);
		paint.setStyle(Style.FILL);
		canvas.drawOval(new RectF(width - height + circleMargin, circleMargin,
				width - circleMargin, height - circleMargin), paint);
	}

}
