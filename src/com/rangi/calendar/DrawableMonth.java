package com.rangi.calendar;

import com.rangi.calendar.settings.SettingsUtil;
import com.rangi.calendar.utils.ColorUtils;
import com.rangi.calendar.utils.Utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Paint.Style;
import android.util.Log;
import android.view.View;

public class DrawableMonth extends View {

	private static final int TODAY_FONT_COLOR = Color.parseColor("#09b89d");

	// the paint that will be used for entire view
	private final static Paint paint = new Paint();

	// Constants
	final SolarCalendar DATE;

	private static int CELL_WIDTH;
	private static int CELL_HEIGHT;
	private static int CELL_FONT_SIZE;
	private final static int BORDER_COLOR = Color.parseColor("#adadad");
	private static final int TITLE_BG_COLOR = Color.parseColor("#d9d9d9");
	private static final int TITLE_TEXT_COLOR = Color.BLACK;
	private final static int EMPTY_CELL_COLOR = Color.parseColor("#e7e7e8");

	private static final String[] WEEK_DAY_INITIALS = { "ش", "ی", "د", "س",
			"چ", "پ", "ج" };
	private static float TITLE_XPOS;
	private static float TITLE_YPOS;
	private SolarCalendar today;
	private static Context CONTEXT;

	public DrawableMonth(Context context, SolarCalendar jDate, int width,
			int height, boolean small) {
		super(context);
		
		
		CONTEXT = context;

		DATE = jDate;
		today = new SolarCalendar();

		// initializing basic sizes
		CELL_WIDTH = width / 7;
		CELL_HEIGHT = height / 8;
		CELL_FONT_SIZE = small ? CELL_WIDTH / 3 : CELL_WIDTH / 2;
		TITLE_XPOS = CELL_WIDTH / 3f;
		TITLE_YPOS = CELL_HEIGHT / 1.7f;
	}

	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		drawTitle(canvas);

		int firstDayWeekDay = (35 + DATE.weekDay - (DATE.date - 1) + 1) % 7;
		// last "+ 1" is for making "Shambe" weekday == 0

		SolarCalendar date = DATE.addDays(-(DATE.date - 1));
		int currentMonth = date.month;
		date = date.addDays(6 - firstDayWeekDay);

		for (int w = 0; w < 7; w++) {

			int cellY = (w + 1) * CELL_HEIGHT; // leaving space for title [cellY
												// == 0 is for title]

			for (int i = 0; i < 7; i++) {

				int cellX = i * CELL_WIDTH;

				if (w == 0) {
					String weekDayName = WEEK_DAY_INITIALS[6 - i];
					drawCell(canvas, cellX, cellY, weekDayName,
							EMPTY_CELL_COLOR, Color.BLACK);
				} else if (date.month != currentMonth) {
					drawCell(canvas, cellX, cellY, "", EMPTY_CELL_COLOR,
							Color.WHITE);
					date = date.addDays(-1);
				} else {
					String dayLabel = "" + date.date;
					if (date.equals(today)) {
						drawCell(canvas, cellX, cellY, dayLabel, Color.WHITE,
								TODAY_FONT_COLOR);
					} else {
						int color = ColorUtils.getColor(CONTEXT, date.month, 7
								* (w - 1) + 6 - i);
						drawCell(canvas, cellX, cellY, dayLabel, color,
								Color.WHITE);
					}
					if (date.weekDay == 5 || Cache.isHoliday(CONTEXT, date)
							&& SettingsUtil.showHolidays(CONTEXT))
						drawHolidayCircle(canvas, cellX, cellY);
					int oldDate = date.date;
					date = date.addDays(-1);

					// very ugly work. to make Farvardin work.
					if (currentMonth == 1 && date.month == 1
							&& date.date - oldDate != -1)
						date = date.addDays(1);
				}
			}
			if (w != 0)
				date = date.addDays(14);
		}
	}

	private void drawTitle(Canvas canvas) {
		

		
		// drawing background
		paint.setColor(TITLE_BG_COLOR);
		paint.setStyle(Style.FILL);
		canvas.drawRect(0, 0, 7 * CELL_WIDTH, CELL_HEIGHT, paint);

		// drawing name of month
		paint.setColor(TITLE_TEXT_COLOR);
		paint.setTextSize(CELL_FONT_SIZE);
		paint.setTypeface(Utils.getCustomFont(CONTEXT));
		paint.setTextAlign(Paint.Align.RIGHT);
		canvas.drawText(DATE.strMonth, 7 * CELL_WIDTH - TITLE_XPOS, TITLE_YPOS,
				paint);
	}

	private void drawCell(Canvas canvas, int cellX, int cellY,
			String cellLabel, int cellColor, int textColor) {

		// drawing the cell
		paint.setColor(cellColor);
		paint.setStyle(Style.FILL);
		canvas.drawRect(cellX, cellY, CELL_WIDTH + cellX, CELL_HEIGHT + cellY,
				paint);

		// drawing the cell border
		paint.setColor(BORDER_COLOR);
		paint.setStyle(Style.STROKE);
		paint.setStrokeWidth(Utils.dipToPixels(CONTEXT, 0.5f));
		canvas.drawRect(cellX, cellY, CELL_WIDTH + cellX, CELL_HEIGHT + cellY,
				paint);

		// drawing the text of cell
		paint.setColor(textColor);
		paint.setStrokeWidth(0);
		paint.setTextSize(CELL_FONT_SIZE);
		paint.setTypeface(Utils.getCustomFont(CONTEXT));
		paint.setTextAlign(Paint.Align.CENTER);
		canvas.drawText(cellLabel, (2 * cellX + CELL_WIDTH) / 2,
				(2 * cellY + CELL_HEIGHT) / 2, paint);
	}

	private void drawHolidayCircle(Canvas canvas, int cellX, int cellY) {
		paint.setColor(Color.WHITE);
		paint.setStyle(Style.STROKE);
		canvas.drawOval(new RectF(cellX + 1, cellY + 1, CELL_WIDTH + cellX - 1,
				CELL_HEIGHT + cellY - 1), paint);
	}
}
