package com.rangi.calendar;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageView;

public class IntroActivity extends FragmentActivity {

	private static final int[] INTRO_PAGES = { R.drawable.img_intro11,
			R.drawable.img_intro10, R.drawable.img_intro9,
			R.drawable.img_intro8, R.drawable.img_intro7,
			R.drawable.img_intro6, R.drawable.img_intro5,
			R.drawable.img_intro4, R.drawable.img_intro3,
			R.drawable.img_intro2, R.drawable.img_intro1 };

	public static final String PAGE_INDEX = "PAGE_INDEX";

	public ViewPager pager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_intro);

		pager = (ViewPager) findViewById(R.id.pager);
		IntroPagerAdapter pagerAdapter = new IntroPagerAdapter(
				getSupportFragmentManager());
		pager.setAdapter(pagerAdapter);
		pager.setCurrentItem(pagerAdapter.getCount() - 1);
		pager.setOnPageChangeListener(pageChangeListener);
	}

	private OnPageChangeListener pageChangeListener = new OnPageChangeListener() {

		@Override
		public void onPageSelected(int index) {
			ViewGroup container = (ViewGroup) findViewById(R.id.indicator_container);
			for (int i = 0; i < INTRO_PAGES.length; i++)
				if (i != index)
					container.findViewWithTag("" + i).setBackgroundResource(
							R.drawable.img_intro_circle);
				else
					container.findViewWithTag("" + i).setBackgroundResource(
							R.drawable.img_intro_circle_selected);
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
		}

		@Override
		public void onPageScrollStateChanged(int arg0) {
		}
	};

	private class IntroPagerAdapter extends FragmentStatePagerAdapter {

		public IntroPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int index) {
			Fragment fragment = new IntroFragment();
			Bundle args = new Bundle();
			args.putInt(PAGE_INDEX, index);
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			return INTRO_PAGES.length;
		}

	}

	public static class IntroFragment extends Fragment {

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			int index = getArguments().getInt(PAGE_INDEX);
			ViewGroup page = (ViewGroup) inflater.inflate(R.layout.intro_page,
					null);
			ViewStub stub = (ViewStub) page.findViewById(R.id.stub);
			if (index == INTRO_PAGES.length - 1) {
				stub.setLayoutResource(R.layout.intro_page_first_middle);
				stub.inflate();
				startArrowAnimation(page, R.id.arrow_right1);
				startArrowAnimation(page, R.id.arrow_right2);
			} else if (index == 0) {
				stub.setLayoutResource(R.layout.intro_page_last_middle);
				stub.inflate();
			}

			Bitmap bm = BitmapFactory.decodeResource(getResources(),
					INTRO_PAGES[index]);
			page.setBackgroundColor(bm.getPixel(0, 0));
			((ImageView) page.findViewById(R.id.image)).setImageBitmap(bm);
			return page;
		}

		private void startArrowAnimation(ViewGroup page, int id) {
			ImageView arrow = ((ImageView) page.findViewById(id));
			AnimationDrawable anim = (AnimationDrawable) arrow.getBackground();
			anim.start();
		}
	}

	public void startMonthView(View view) {
		Intent intent = new Intent(this, MonthView.class);
		startActivity(intent);
	}

}
