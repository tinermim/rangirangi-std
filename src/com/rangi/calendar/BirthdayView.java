package com.rangi.calendar;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.joda.time.LocalDate;

import android.content.Intent;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.rangi.calendar.MyDataTypes.Birthday;
import com.rangi.calendar.MyDataTypes.Reminder;
import com.rangi.calendar.alarm.AlarmService;
import com.rangi.calendar.customviews.MyTextView;
import com.rangi.calendar.database.DatabaseHelper;
import com.rangi.calendar.utils.ColorUtils;


public class BirthdayView extends RangiActionBarActivity {


	private LinearLayout birthdayList;

	private List<Long> birthdayIdsToDelete = null;
	private static boolean deleteMode = false;

	private static int[] UNDEFINED_CONTACT_PHOTOS = { R.drawable.img_contact1,
			R.drawable.img_contact2, R.drawable.img_contact3,
			R.drawable.img_contact4 };

	/**
	 * New Params
	 */
	private DrawerLayout mDrawerLayout;
	private LinearLayout mDrawer;
	private String[] menuItems;
	private TypedArray menuItemsIcons;
	private ListView mDrawerList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_birthday_view);
		setupActionBar(R.layout.action_bar_birthday_view);

		birthdayList = (LinearLayout) findViewById(R.id.birthday_list);

		refreshBirthdays();
		setupDrawer();
	}

	@Override
	protected void onResume() {
		refreshBirthdays();
		super.onResume();
	}

	private void refreshBirthdays() {
		birthdayList.removeAllViews();
		List<Birthday> birthdays = DatabaseHelper.getDbHelper(this)
				.getAllBirthdays();

		if (birthdays.size() == 0)
			findViewById(R.id.adding_hint).setVisibility(View.VISIBLE);
		else
			findViewById(R.id.adding_hint).setVisibility(View.GONE);

		/*
		 * adding near birthdays
		 */
		SolarCalendar today = new SolarCalendar();
		SolarCalendar next14day = new SolarCalendar(
				new LocalDate().plusDays(14));
		View title = getLayoutInflater().inflate(
				R.layout.birthday_view_list_title_row, null);
		title.setBackgroundColor(0xffd55135);
		((TextView) title.findViewById(R.id.month))
				.setText(R.string.birthday_view_near_birthdays_title);
		birthdayList.addView(title);
		for (Birthday birthday : birthdays) {
			if (today.month != next14day.month) {
				if (birthday.month + 1 == today.month
						&& birthday.day >= today.date
						|| birthday.month + 1 == next14day.month
						&& birthday.day <= next14day.date)
					addBirthdayRow(birthday);
			} else if (birthday.month + 1 == today.month
					&& birthday.day >= today.date
					&& birthday.day <= next14day.date)
				addBirthdayRow(birthday);
		}

		// making a modified list of birthdays so that current month birthdays
		// are listed higher
		List<Birthday> modifiedList = new ArrayList<Birthday>();
		// first pass: current and next months
		for (Birthday birthday : birthdays)
			if (birthday.month + 1 >= today.month)
				modifiedList.add(birthday);
		// second pas: previous months
		for (Birthday birthday : birthdays)
			if (birthday.month + 1 < today.month)
				modifiedList.add(birthday);

		int currentMonth = -1;
		for (final Birthday birthday : modifiedList) {
			if (currentMonth != birthday.month) { // add a new title
				currentMonth = birthday.month;
				title = getLayoutInflater().inflate(
						R.layout.birthday_view_list_title_row, null);
				title.setBackgroundColor(ColorUtils.getColor(this,
						currentMonth + 1, 0));
				((TextView) title.findViewById(R.id.month))
						.setText(SolarCalendar.getMonthNames()[currentMonth]);
				birthdayList.addView(title);
			}

			addBirthdayRow(birthday);
		}
	}

	private void addBirthdayRow(final Birthday birthday) {
		View row = getLayoutInflater().inflate(R.layout.birthday_view_list_row,
				null);
		TextView date = (TextView) row.findViewById(R.id.date);
		date.setText(birthday.day + " "
				+ SolarCalendar.getMonthNames()[birthday.month]);
		row.findViewById(R.id.photo_frame)
				.setBackgroundColor(
						ColorUtils.getColor(this, birthday.month + 1,
								birthday.day - 1));
		((TextView) row.findViewById(R.id.name)).setText(birthday.name);
		ImageView photo = (ImageView) row.findViewById(R.id.photo);
		if (birthday.photoUri != null)
			photo.setImageURI(Uri.parse(birthday.photoUri));
		else
			photo.setImageResource(UNDEFINED_CONTACT_PHOTOS[new Random()
					.nextInt(4)]);

		final CheckBox checkBox = (CheckBox) row.findViewById(R.id.checkbox);
		if (deleteMode) {
			checkBox.setVisibility(View.VISIBLE);
			checkBox.setTag((long) birthday.id);
		}

		row.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (deleteMode)
					maintainDeleteList(checkBox);
				else {
					Intent intent = new Intent(BirthdayView.this,
							BirthdayEditor.class);
					intent.putExtra(BirthdayEditor.BIRTHDAY_ID,
							(long) birthday.id);
					startActivity(intent);
				}
			}
		});

		birthdayList.addView(row);
	}

	protected void maintainDeleteList(CheckBox checkBox) {
		checkBox.toggle();
		if (checkBox.isChecked())
			birthdayIdsToDelete.add((Long) checkBox.getTag());
		else
			birthdayIdsToDelete.remove((Long) checkBox.getTag());
		MyTextView actionBarNumber = (MyTextView) findViewById(R.id.num_of_deletes);
		actionBarNumber.setText("" + birthdayIdsToDelete.size());
	}

	public void close(View view) {
		onBackPressed();
	}

	public void deleteBirthdays(View view) {
		if (!deleteMode) {
			deleteMode = true;
			birthdayIdsToDelete = new ArrayList<Long>();
			refreshBirthdays(); /* Causes to add check box to each row */
			bringUpDeletePanel();
			setupActionBar(R.layout.action_bar_birthday_view_delete_mode);
			((TextView) findViewById(R.id.num_of_deletes)).setText("0");
		}
	}

	public void deleteSelected(View view) {
		for (long id : birthdayIdsToDelete) {
			DatabaseHelper.getDbHelper(this).deleteBirthday(id);
			deleteAlarmsOfBirthday(id);
		}
		refreshBirthdays();
		finishDeleting();
	}

	private void deleteAlarmsOfBirthday(long id) {
		List<Reminder> reminders = DatabaseHelper.getDbHelper(this)
				.getBirthdayReminders(id);
		for (Reminder reminder : reminders) {
			DatabaseHelper.getDbHelper(this).deleteAlarm(reminder.id);
			AlarmService.cancelReminder(this, reminder.id);
		}

	}

	private void finishDeleting() {
		cancelDeleting(null);
	}

	public void cancelDeleting(View view) {
		deleteMode = false;
		birthdayIdsToDelete = null;
		refreshBirthdays(); /* Causes to remove check boxes from rows */
		takeDownDeletePanel();
		setupActionBar(R.layout.action_bar_birthday_view);
	}

	private void bringUpDeletePanel() {
		View deletePanel = findViewById(R.id.delete_panel);
		Animation upFromBottom = AnimationUtils.loadAnimation(this,
				R.anim.up_from_bottom);
		deletePanel.startAnimation(upFromBottom);
		deletePanel.setVisibility(View.VISIBLE);
	}

	private void takeDownDeletePanel() {
		View deletePanel = findViewById(R.id.delete_panel);
		Animation downFromTop = AnimationUtils.loadAnimation(this,
				R.anim.down_from_top);
		deletePanel.startAnimation(downFromTop);
		deletePanel.setVisibility(View.GONE);
	}

	@Override
	public void onBackPressed() {
		if (deleteMode)
			finishDeleting();
		finish();
	}

	public void addBirthday(View view) {
		startActivity(new Intent(this, BirthdayEditor.class));
	}

}