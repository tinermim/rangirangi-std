package com.rangi.calendar;

import org.joda.time.LocalDate;

import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.Window;
import android.widget.TextView;

import com.rangi.calendar.settings.SettingsUtil;
import com.rangi.calendar.sync.CalendarChangeService;
import com.rangi.calendar.utils.Utils;

public class MonthView extends CalendarActivity {

	public static final int NUM_OF_PAGES = 10000;
	public static final int MIDDLE_PAGE = NUM_OF_PAGES / 2;
	public static final int MARGIN = 1;
	MonthsPagerAdapter myMonthsPagerAdapter;
	ViewPager myViewPager;
	private long syncCalId;

	BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.i("FFF", "calId : " + syncCalId);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ViewStub stub = (ViewStub) findViewById(R.id.stub);
		stub.setLayoutResource(R.layout.activity_month_view);
		stub.inflate();

		startService(new Intent(this, CalendarChangeService.class));

		IntentFilter filter = new IntentFilter();
		filter.addAction(CalendarChangeService.CALENDAR_CHANGE);
		registerReceiver(mBroadcastReceiver, filter);
		Log.d("FFF", "Run broadcastreceiver for detect calendar changes");

		Intent intent = getIntent();
		int offset = intent.getIntExtra(MainActivity.MONTH_OFFSET, 0);

		myMonthsPagerAdapter = new MonthsPagerAdapter(
				getSupportFragmentManager());
		myViewPager = (ViewPager) findViewById(R.id.pager);
		myViewPager.setAdapter(myMonthsPagerAdapter);
		myViewPager.setCurrentItem(MIDDLE_PAGE - offset);
		myViewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int currentPage) {
				setCustomTitle(-(currentPage - MIDDLE_PAGE));

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
		setupActionBar();

		showNewFeaturesIfUpgraded();

	}

	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	private void checkForAd() {
		new CheckAdvertisement().execute(this);
	}

	private boolean isTimeToCheckServerFiles() {
		SharedPreferences prefs = getPreferences(0);
		long lastUpdateTime = prefs.getLong("lastUpdateTime", 0);

		if ((lastUpdateTime + 1000*3600*24) > System
				.currentTimeMillis()) {
			Log.i("CheckServerFiles", "notChecked-dayNotPassed");
			return false;
		}

		if (!isNetworkAvailable()) {
			Log.i("CheckServerFiles", "notChecked-NetworkNotAvailable");
			return false;
		}
		/* Save current timestamp for next Check */
		lastUpdateTime = System.currentTimeMillis();
		SharedPreferences.Editor editor = getPreferences(0).edit();
		editor.putLong("lastUpdateTime", lastUpdateTime);
		editor.commit();
		return true;
	}

	private void checkForUpdate() {
		new CheckUpdate().execute(this);
	}

	private void showNewFeaturesIfUpgraded() {
		SharedPreferences settings = SettingsUtil.getSettings(this);
		int currentVersionCode;
		try {
			currentVersionCode = getPackageManager().getPackageInfo(
					getPackageName(), 0).versionCode;
		} catch (NameNotFoundException e) {
			currentVersionCode = 0;
			e.printStackTrace();
		}
		if (settings.getInt(SettingsUtil.LAST_VERSION_CODE, 0) < currentVersionCode) {
			showNewFeaturesDialog();
			settings.edit()
					.putInt(SettingsUtil.LAST_VERSION_CODE, currentVersionCode)
					.commit();
		}
	}

	private void showNewFeaturesDialog() {
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_new_features);
		dialog.findViewById(R.id.btn_ok).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						dialog.dismiss();
					}
				});

		String newFeatures = "";
		for (String feature : getResources().getStringArray(
				R.array.new_version_changes))
			newFeatures += "• " + feature + "\n";
		((TextView) dialog.findViewById(R.id.new_features))
				.setText(newFeatures);

		dialog.show();
	}

	@Override
	protected void onResume() {
		setupActionBar();

		// Reloading the pages on resume
		myMonthsPagerAdapter.notifyDataSetChanged();
		myViewPager.invalidate();

		if (isTimeToCheckServerFiles()) {
			checkForUpdate();
			checkForAd();
		}

		super.onResume();
	}

	@Override
	public void onBackPressed() {
		moveTaskToBack(true);
	}

	private void setCustomTitle(int offset) {
		String month;
		int year;
		if (SettingsUtil.primaryCalendarIsJalali(this)) {
			SolarCalendar date = SolarCalendar.offsetMonth(offset);
			month = date.strMonth;
			year = date.year;
		} else {
			LocalDate date = new LocalDate().plusMonths(offset);
			month = SolarCalendar.getFarsiMonthName(date);
			year = date.getYear();
		}
		TextView title = (TextView) findViewById(R.id.actionbar_title);
		title.setText(month + "\n" + "سال " + year);
		title.setTypeface(Utils.getCustomFont(this));
	}

	public class MonthsPagerAdapter extends FragmentStatePagerAdapter {

		public MonthsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int biasedOffset) {
			Fragment fragment = new MonthFragment();
			Bundle args = new Bundle();
			args.putInt(MonthFragment.OFFSET, -(biasedOffset - MIDDLE_PAGE));
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			return NUM_OF_PAGES;
		}

		// To reload the page once a change occured
		@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE;
		}
	}

	public static class MonthFragment extends Fragment {

		public static final String OFFSET = "offset";

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			Bundle args = getArguments();
			int offset = args.getInt(OFFSET);
			return new MonthDrawer(getActivity(), offset);
		}

	}

	protected void setupActionBar() {
		super.setupActionBar();
		int offset = -(myViewPager.getCurrentItem() - MIDDLE_PAGE);
		setCustomTitle(offset);

		// adding action to year button
		findViewById(R.id.year_view).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
						final int currentItem = MonthView.this.myViewPager
								.getCurrentItem();
						gotoYearView(SolarCalendar
								.offsetMonth(-(currentItem - MIDDLE_PAGE)));
					}
				});

		// adding action to week button
		findViewById(R.id.week_view).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
						final int currentItem = MonthView.this.myViewPager
								.getCurrentItem();
						gotoWeekView(SolarCalendar
								.offsetMonth(-(currentItem - MIDDLE_PAGE)));
					}
				});
	}

	protected void gotoYearView(SolarCalendar jDate) {
		Intent intent = new Intent(this, YearView.class);
		intent.putExtra(MainActivity.YEAR_OFFSET,
				SolarCalendar.calculateYearOffset(jDate));
		startActivity(intent);
	}

	protected void gotoWeekView(SolarCalendar jDate) {
		Intent intent = new Intent(this, WeekView.class);
		intent.putExtra(MainActivity.WEEK_OFFSET,
				SolarCalendar.calculateWeekOffset(jDate));
		startActivity(intent);
	}

}
