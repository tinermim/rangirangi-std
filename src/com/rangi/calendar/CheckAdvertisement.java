package com.rangi.calendar;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.apache.http.util.ByteArrayBuffer;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.support.v4.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

class Ad {
	public String text = "";
	public Bitmap icon = null;
	public String iconUrl = "";
	public String packageName = "";
	public String title = "";
	public int key = -1;
}

public class CheckAdvertisement extends AsyncTask<Activity, Void, Ad> {

	Activity baseActivity;
	@Override
	protected Ad doInBackground(Activity... arg0) {

		baseActivity = arg0[0];
		String s = null;
		Ad ad = null;
		try {
			Log.e("checkFOrAd", "Thread started");
			URL updateURL = new URL(baseActivity.getString( R.string.ad_file_url));
			URLConnection conn = updateURL.openConnection();
			InputStream is = conn.getInputStream();
			BufferedInputStream bis = new BufferedInputStream(is);
			ByteArrayBuffer baf = new ByteArrayBuffer(200);

			int current = 0;
			while ((current = bis.read()) != -1) {
				baf.append((byte) current);
			}
			s = new String(baf.toByteArray());
		} catch (Exception e) {
			Log.e("problem here", "problem here");
		}

		if (s != null)
			Log.i("string", s);
		else
			Log.i("string", "null");

		/* Convert the Bytes read to a String. */

		try {
			ad = parseXML(s);
		} catch (Exception e) {
			Log.e("adBuilder", "parsingProblem");
			e.printStackTrace();
		}

		if (ad != null)
			ad.icon = getBitmapFromURL(ad.iconUrl);

		Log.e("checkForAd", "Thread ended");

		return ad;

	}

	private static Bitmap getBitmapFromURL(String src) {
		try {
			URL url = new URL(src);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);
			return myBitmap;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	private Ad parseXML(String in_s) throws XmlPullParserException, IOException {

		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		factory.setNamespaceAware(true);
		XmlPullParser parser = factory.newPullParser();

		parser.setInput(new StringReader(in_s));

		ArrayList<Ad> ads = null;
		int eventType = parser.getEventType();
		Ad currentad = null;

		while (eventType != XmlPullParser.END_DOCUMENT) {
			String name = null;
			switch (eventType) {
			case XmlPullParser.START_DOCUMENT:
				ads = new ArrayList<Ad>();
				break;
			case XmlPullParser.START_TAG:
				name = parser.getName();
				if (name.equals("ad")) {
					currentad = new Ad();
				} else if (currentad != null) {
					if (name.equals("adTitle"))
						currentad.title = parser.nextText();
					else if (name.equals("adText")) 
						currentad.text = parser.nextText();
					else if (name.equals("adIcon")) 
						currentad.iconUrl = parser.getAttributeValue(0);// parser.nextText();
					else if (name.equals("adPackageName")) 
						currentad.packageName = parser.nextText();
					else if (name.equals("adKey")) {
						try{
						currentad.key = Integer.parseInt(parser.nextText());
						}
						catch(Exception e){
							e.printStackTrace();
							Log.e("parsingServerAdFile","xml=>keyProblem, notInteger");
						}
					}
				}
				break;
			case XmlPullParser.END_TAG:
				name = parser.getName();
				if (name.equalsIgnoreCase("ad") && currentad != null) {
					ads.add(currentad);
				}
			}
			eventType = parser.next();
		}

//		Log.e("XMLparse", ads.get(0).text + " " + ads.get(0).iconUrl + " "
//				+ ads.get(0).packageName + " " + ads.get(0).key);

		return ads.get(0);
	}

	@Override
	protected void onPostExecute(Ad ad) {
		Log.d("ChekForAd", "methodStart");
		SharedPreferences prefs = baseActivity.getPreferences(0);
		int key = prefs.getInt("key", 0);
		if (ad == null) {
			Log.d("ChekForAd", "ad is null");
			return;
		}
		if (ad.key <= 0 || ad.key <= key) {
			Log.d("ChekForAd", "old Version" + " serverKey:"+ad.key + " ,prefKey:" + key);
			return;
		}
		Log.d("chekForAd", "newAdNotify");
		NotifyNewAd(ad);
	}

	private void NotifyNewAd(Ad ad) {
		NotificationCompat.Builder mBuilder =  new NotificationCompat.Builder(
				baseActivity).setContentTitle(ad.title).setLargeIcon(ad.icon)
				.setSmallIcon(R.drawable.ic_launcher)
				.setContentText(ad.text);

		// Creates an explicit intent for an Activity in your app
		Intent resultIntent = new Intent(Intent.ACTION_VIEW,
				Uri.parse("market://details?id=" + ad.packageName));
		
		// The stack builder object will contain an artificial back stack for
		// the
		// started Activity.
		// This ensures that navigating backward from the Activity leads out of
		// your application to the Home screen.
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(baseActivity);
		// Adds the back stack for the Intent (but not the Intent itself)
		stackBuilder.addParentStack(MonthView.class);
		// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(resultIntent);
		
		mBuilder.setAutoCancel(true);
		
//		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
//				PendingIntent.FLAG_UPDATE_CURRENT);
		
		PendingIntent resultPendingIntent =
			    PendingIntent.getActivity(
			    baseActivity,
			    0,
			    resultIntent,
			    PendingIntent.FLAG_UPDATE_CURRENT
			);
		
		mBuilder.setContentIntent(resultPendingIntent);
		Log.e("DisplayAd","Notificaion Ad to Manager");
		NotificationManager mNotificationManager = (NotificationManager) baseActivity.getSystemService(Context.NOTIFICATION_SERVICE);
		// mId allows you to update the notification later on.
		mNotificationManager.notify(111, mBuilder.build());

	}
}
