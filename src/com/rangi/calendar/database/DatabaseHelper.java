package com.rangi.calendar.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.util.SparseArray;
import com.rangi.calendar.MyDataTypes.Birthday;
import com.rangi.calendar.MyDataTypes.Note;
import com.rangi.calendar.MyDataTypes.Reminder;
import com.rangi.calendar.OldDatabaseHelper;
import com.rangi.calendar.utils.Utils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DatabaseHelper extends SQLiteOpenHelper {

	// The Android's default system path of your application database.
	private static final String DB_PATH = "/data/data/com.rangi.calendar/databases/";

	private static final String DB_NAME = "rrp.sqlite";
	public static final String NOTE_TABLE = "notes";

	private static DatabaseHelper dbHelper;

	private SQLiteDatabase myDataBase;

	private final Context myContext;

	/**
	 * Constructor Takes and keeps a reference of the passed context in order to
	 * access to the application assets and resources.
	 * 
	 * @param context
	 * @return
	 */
	public DatabaseHelper(Context context) {
		super(context, DB_NAME, null, 1);
		this.myContext = context;
	}
	
	private static final String HOLIDAYS_94 = "holidays1394.json";

	private void initTables() {
		myDataBase.execSQL("CREATE TABLE IF NOT EXISTS " + "Birthday("
				+ "_id INTEGER PRIMARY KEY, " + "month INTEGER, "
				+ "day INTEGER, " + "name TEXT, " + "photo_uri TEXT)");
		myDataBase.execSQL("CREATE TABLE IF NOT EXISTS " + "Reminder("
				+ "_id INTEGER PRIMARY KEY, " + "ref_id INTEGER, "
				+ "time TEXT, " + "for TEXT, " + "type INTEGER)");
		
		addHolidays(HOLIDAYS_94);
		
		if (OldDatabaseHelper.exists()) {
			copyTablesFromOldDb();
			OldDatabaseHelper.delete();
		}
	}
	
	private void addHolidays(String holidaysJsonFile) {
		JSONArray holidays = getJSONArrayFromAsset(holidaysJsonFile);
		
		for (int i = 0; i < holidays.length(); i++) {
			JSONObject holiday = holidays.optJSONObject(i);
			ContentValues cv = new ContentValues();
			int year = holiday.optInt("year");
			int month = holiday.optInt("month");
			int day = holiday.optInt("day");
			if (getHoliday(year, month, day) == null) {
				cv.put("year", year);
				cv.put("month", month);
				cv.put("day", day);
				cv.put("description", holiday.optString("description"));
				myDataBase.insert("Holiday", null, cv);
			}
		}
	}
	
	private String getHoliday(int year, int month, int day) {
		Cursor cursor = myDataBase.rawQuery("select * from Holiday WHERE " + 
				"year=" + year + " AND month=" + month + " AND day=" + day, null);
		if (cursor.moveToFirst()) {
			return cursor.getString(cursor.getColumnIndex("description"));
		} else {
			return null;
		}
	}

	private JSONArray getJSONArrayFromAsset(String assetName) {
		try {
			InputStream is = myContext.getAssets().open(assetName);
			final StringBuilder jsonString = new StringBuilder();

			for (final BufferedReader isReader = new BufferedReader(
					new InputStreamReader(is), 16000); isReader.ready();) {
				jsonString.append(isReader.readLine());
			}
			return new JSONArray(jsonString.toString());
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void copyTablesFromOldDb() {
		ArrayList<Note> notes = OldDatabaseHelper.getDbHelper(myContext)
				.getAllNotes();
		for (Note note : notes) {
			ContentValues values = new ContentValues();
			values.put("_id", note.id);
			values.put("year", note.year);
			values.put("month", note.month);
			values.put("day", note.day);
			values.put("note", note.note);
			myDataBase.insert(NOTE_TABLE, null, values);
		}
	}

	/**
	 * Creates a empty database on the system and rewrites it with your own
	 * database.
	 * */
	public void createDataBase() throws IOException {

		boolean dbExist = checkDataBase();
		// dbExist = false;

		if (dbExist) {
			// do nothing - database already exist
		} else {

			// By calling this method and empty database will be created into
			// the default system path
			// of your application so we are gonna be able to overwrite that
			// database with our database.
			this.getReadableDatabase();

			try {
				copyDataBase();

			} catch (IOException e) {

				throw new Error("Error copying database");

			}
		}
	}

	/**
	 * Check if the database already exist to avoid re-copying the file each
	 * time you open the application.
	 * 
	 * @return true if it exists, false if it doesn't
	 */
	private boolean checkDataBase() {

		/*
		 * SQLiteDatabase checkDB = null;
		 * 
		 * try{ String myPath = DB_PATH + DB_NAME; checkDB =
		 * SQLiteDatabase.openDatabase(myPath, null,
		 * SQLiteDatabase.OPEN_READONLY); checkDB.close(); return true;
		 * 
		 * }catch(SQLiteException e){ //database does't exist yet. }
		 * 
		 * return false;
		 */
		File dbFile = new File(DB_PATH + DB_NAME);
		return dbFile.exists();
	}

	/**
	 * Copies your database from your local assets-folder to the just created
	 * empty database in the system folder, from where it can be accessed and
	 * handled. This is done by transfering bytestream.
	 * */
	private void copyDataBase() throws IOException {

		// Open your local db as the input stream
		InputStream myInput = myContext.getAssets().open(DB_NAME);

		// Path to the just created empty db
		String outFileName = DB_PATH + DB_NAME;

		// Open the empty db as the output stream
		OutputStream myOutput = new FileOutputStream(outFileName);

		// transfer bytes from the inputfile to the outputfile
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer)) > 0) {
			myOutput.write(buffer, 0, length);
		}

		// Close the streams
		myOutput.flush();
		myOutput.close();
		myInput.close();

	}

	public void openDataBase() throws SQLException {

		// Open the database
		String myPath = DB_PATH + DB_NAME;
		myDataBase = SQLiteDatabase.openDatabase(myPath, null,
				SQLiteDatabase.OPEN_READWRITE);
		initTables();
	}
	
	public SQLiteDatabase getDb() {
		return myDataBase;
	}

	@Override
	public synchronized void close() {

		if (myDataBase != null)
			myDataBase.close();

		super.close();

	}

	@Override
	public void onCreate(SQLiteDatabase db) {

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	// Add your public helper methods to access and get content from the
	// database.
	// You could return cursors by doing "return myDataBase.query(....)" so it'd
	// be easy
	// to you to create adapters for your views.

	public String getEventDescription(int day, int month) {
		Cursor cursor = getDailyEventCursor(day, month);
		if (cursor.moveToNext()) {
			String desc = cursor
					.getString(cursor.getColumnIndex("description")).trim();
			cursor.close();
			return desc;
		}
		return null;
	}

	public String getEventTitle(int day, int month) {
		Cursor cursor = getDailyEventCursor(day, month);
		if (cursor.moveToNext()) {
			String title = cursor.getString(cursor.getColumnIndex("title"))
					.trim();
			cursor.close();
			return title;
		}
		return null;
	}

	public int[][] getColors() {
		int[][] colors = new int[13][37];
		Cursor cursor = myDataBase.rawQuery("SELECT * FROM colors", null);
		for (int i = 0; i < 37; i++) {
			cursor.moveToNext();
			for (int month = 1; month <= 12; month++)
				colors[month][i] = Color.parseColor("#"
						+ cursor.getString(cursor.getColumnIndex("" + month)));
		}
		return colors;
	}

	private Cursor getDailyEventCursor(int day, int month) {
		return myDataBase.rawQuery("SELECT * FROM DailyEvent WHERE month="
				+ month + " and day=" + day, null);
	}

	public void cacheEventTitles(String[][] titles) {
		Cursor cursor = myDataBase.rawQuery("select * from DailyEvent", null);
		while (cursor.moveToNext()) {
			int m = cursor.getInt(cursor.getColumnIndex("month"));
			int d = cursor.getInt(cursor.getColumnIndex("day"));
			titles[m][d] = cursor.getString(cursor.getColumnIndex("title"))
					.trim();
		}
		cursor.close();
	}

	public void cacheEventDescriptions(String[][] descriptions) {
		Cursor cursor = myDataBase.rawQuery("select * from DailyEvent", null);
		while (cursor.moveToNext()) {
			int m = cursor.getInt(cursor.getColumnIndex("month"));
			int d = cursor.getInt(cursor.getColumnIndex("day"));
			descriptions[m][d] = cursor.getString(
					cursor.getColumnIndex("description")).trim();
		}
		cursor.close();
	}

	public void cacheHolidays(SparseArray<String> holidayEvents) {
		Cursor cursor = myDataBase.rawQuery("select * from Holiday", null);
		while (cursor.moveToNext()) {
			int y = cursor.getInt(cursor.getColumnIndex("year"));
			int m = cursor.getInt(cursor.getColumnIndex("month"));
			int d = cursor.getInt(cursor.getColumnIndex("day"));
			holidayEvents.put(Utils.getDateAsInteger(y, m, d),
					cursor.getString(cursor.getColumnIndex("description")));
		}
		cursor.close();
	}

	public void cacheHolidayDates(Set<Integer> holidayDates) {
		Cursor cursor = myDataBase.rawQuery("select * from Holiday", null);
		while (cursor.moveToNext()) {
			int y = cursor.getInt(cursor.getColumnIndex("year"));
			int m = cursor.getInt(cursor.getColumnIndex("month"));
			int d = cursor.getInt(cursor.getColumnIndex("day"));
			holidayDates.add(Utils.getDateAsInteger(y, m, d));
		}
		cursor.close();
	}

	public static DatabaseHelper getDbHelper(Context context) {
		if (dbHelper == null) {
			dbHelper = new DatabaseHelper(context);
			try {
				dbHelper.createDataBase();
			} catch (IOException ioe) {
				throw new Error("Unable to create database");
			}
			try {
				dbHelper.openDataBase();
			} catch (SQLException sqle) {
				throw sqle;
			}
		} else if (!dbHelper.isOpen())
			dbHelper.openDataBase();

		return dbHelper;
	}

	public boolean isOpen() {
		return myDataBase.isOpen();
	}

	public List<Birthday> getAllBirthdays() {
		List<Birthday> birthdays = new ArrayList<Birthday>();
		Cursor cursor = myDataBase.rawQuery(
				"select * from Birthday order by month, day", null);
		while (cursor.moveToNext())
			birthdays.add(new Birthday(cursor.getInt(cursor
					.getColumnIndex("_id")), cursor.getInt(cursor
					.getColumnIndex("month")), cursor.getInt(cursor
					.getColumnIndex("day")), cursor.getString(cursor
					.getColumnIndex("name")), cursor.getString(cursor
					.getColumnIndex("photo_uri"))));
		return birthdays;
	}

	public List<Birthday> getBirthdays(int month, int day) {
		List<Birthday> birthdays = new ArrayList<Birthday>();
		Cursor cursor = myDataBase.rawQuery(
				"select * from Birthday where month=" + month + " and day="
						+ day, null);
		while (cursor.moveToNext())
			birthdays.add(new Birthday(cursor.getInt(cursor
					.getColumnIndex("_id")), month, day, cursor
					.getString(cursor.getColumnIndex("name")), cursor
					.getString(cursor.getColumnIndex("photo_uri"))));
		return birthdays;
	}

	public Birthday getBirthday(long id) {
		Cursor cursor = myDataBase.rawQuery("select * from Birthday where _id="
				+ id, null);
		if (cursor.moveToNext())
			return new Birthday(cursor.getInt(cursor.getColumnIndex("_id")),
					cursor.getInt(cursor.getColumnIndex("month")),
					cursor.getInt(cursor.getColumnIndex("day")),
					cursor.getString(cursor.getColumnIndex("name")),
					cursor.getString(cursor.getColumnIndex("photo_uri")));
		return null;
	}

	public long addBirthday(int month, int day, String name, String photoUri) {
		ContentValues values = new ContentValues();
		values.put("month", month);
		values.put("day", day);
		values.put("name", name);
		values.put("photo_uri", photoUri);
		return myDataBase.insert("Birthday", null, values);
	}

	public void updateBirthday(long birthdayId, int month, int day,
			String name, String photoUri) {
		String where = "_id=" + birthdayId;
		ContentValues values = new ContentValues();
		values.put("month", month);
		values.put("day", day);
		values.put("name", name);
		values.put("photo_uri", photoUri);
		myDataBase.update("Birthday", values, where, null);
	}

	public void deleteBirthday(long id) {
		myDataBase.delete("Birthday", "_id=" + id, null);
	}

	public String getNameForBirthdayReminder(int alarmId) {
		Cursor cursor = myDataBase.rawQuery("select name "
				+ "from Birthday, Reminder "
				+ "where Birthday._id=Reminder.ref_id " + "and Reminder._id="
				+ alarmId, null);
		cursor.moveToFirst();
		return cursor.getString(0);
	}

	public Reminder getReminder(long id) {
		Cursor cursor = myDataBase.rawQuery("select * from Reminder where _id="
				+ id, null);
		cursor.moveToNext();
		return new Reminder(id,
				cursor.getLong(cursor.getColumnIndex("ref_id")),
				cursor.getString(cursor.getColumnIndex("time")),
				cursor.getString(cursor.getColumnIndex("for")),
				cursor.getInt(cursor.getColumnIndex("type")));
	}

	public List<Reminder> getAllReminders() {
		List<Reminder> reminders = new ArrayList<Reminder>();
		Cursor cursor = myDataBase.rawQuery("select * from Reminder", null);
		while (cursor.moveToNext())
			reminders.add(new Reminder(cursor.getInt(cursor
					.getColumnIndex("_id")), cursor.getLong(cursor
					.getColumnIndex("ref_id")), cursor.getString(cursor
					.getColumnIndex("time")), cursor.getString(cursor
					.getColumnIndex("for")), cursor.getInt(cursor
					.getColumnIndex("type"))));
		return reminders;
	}

	public Reminder getNoteReminder(long noteId) {
		Cursor cursor = myDataBase.rawQuery(
				"select * from Reminder where ref_id=" + noteId + " and for="
						+ "'" + Reminder.REMINDER_FOR_NOTE + "'", null);
		if (cursor.moveToNext())
			return new Reminder(cursor.getInt(cursor.getColumnIndex("_id")),
					cursor.getLong(cursor.getColumnIndex("ref_id")),
					cursor.getString(cursor.getColumnIndex("time")),
					cursor.getString(cursor.getColumnIndex("for")),
					cursor.getInt(cursor.getColumnIndex("type")));
		return null;
	}

	public List<Reminder> getNoteReminders(long noteId) {
		List<Reminder> reminders = new ArrayList<Reminder>();
		Cursor cursor = myDataBase.rawQuery(
				"select * from Reminder where ref_id=" + noteId + " and for="
						+ "'" + Reminder.REMINDER_FOR_NOTE + "'", null);
		while (cursor.moveToNext())
			reminders.add(new Reminder(cursor.getInt(cursor
					.getColumnIndex("_id")), cursor.getLong(cursor
					.getColumnIndex("ref_id")), cursor.getString(cursor
					.getColumnIndex("time")), cursor.getString(cursor
					.getColumnIndex("for")), cursor.getInt(cursor
					.getColumnIndex("type"))));
		return reminders;
	}

	public List<Reminder> getBirthdayReminders(long birthdayId) {
		List<Reminder> reminders = new ArrayList<Reminder>();
		Cursor cursor = myDataBase.rawQuery(
				"select * from Reminder where ref_id=" + birthdayId
						+ " and for=" + "'" + Reminder.REMINDER_FOR_BIRTHDAY
						+ "'", null);
		while (cursor.moveToNext())
			reminders.add(new Reminder(cursor.getInt(cursor
					.getColumnIndex("_id")), cursor.getLong(cursor
					.getColumnIndex("ref_id")), cursor.getString(cursor
					.getColumnIndex("time")), cursor.getString(cursor
					.getColumnIndex("for")), cursor.getInt(cursor
					.getColumnIndex("type"))));
		return reminders;
	}

	public long addReminder(long noteId, Reminder reminder) {
		ContentValues values = new ContentValues();
		values.put("ref_id", noteId);
		values.put("time", reminder.dateTime);
		values.put("for", reminder.isFor);
		values.put("type", reminder.alarmType);
		return myDataBase.insert("Reminder", null, values);
	}
	
	public void updateReminder(long id, long newRefId) {
		String strFilter = "_id=" + id;
		ContentValues args = new ContentValues();
		args.put("ref_id", newRefId);
		myDataBase.update("Reminder", args, strFilter, null);
	}

	public void updateReminder(long id, String dateTime) {
		String strFilter = "_id=" + id;
		ContentValues args = new ContentValues();
		args.put("time", dateTime);
		myDataBase.update("Reminder", args, strFilter, null);
	}

	public void updateReminder(long id, int alarmType) {
		String strFilter = "_id=" + id;
		ContentValues args = new ContentValues();
		args.put("type", alarmType);
		myDataBase.update("Reminder", args, strFilter, null);
	}

	public void deleteAlarm(Long id) {
		myDataBase.delete("Reminder", "_id=" + id, null);
	}

}
