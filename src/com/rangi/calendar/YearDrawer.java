package com.rangi.calendar;

import org.joda.time.LocalDate;

import com.rangi.calendar.settings.SettingsUtil;
import com.rangi.calendar.utils.Utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

public class YearDrawer extends LinearLayout {

	private static final float WIDTH_MARGIN_RATIO = 640f / 20f;
	private Context CONTEXT;
	public static int MARGIN;

	public YearDrawer(Context context, int offset) {
		super(context);
		if (SettingsUtil.primaryCalendarIsJalali(context))
			drawYear(context, SolarCalendar.offsetYear(offset));
		else
			drawYear(context, new LocalDate().plusYears(offset));
	}

	/**
	 * Draws 12 Jalali months  
	 */
	private void drawYear(Context context, SolarCalendar jDate) {
		CONTEXT = context;
		this.setOrientation(LinearLayout.VERTICAL);
		this.setGravity(Gravity.CENTER);
		this.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));

		int screenWidth = Utils.getScreenWidth(context);
		MARGIN = (int) (screenWidth / WIDTH_MARGIN_RATIO);
		int monthWidth = (int) (screenWidth / 2 - 1.5 * MARGIN);
		int monthHeight = monthWidth * 8 / 7;

		for (int offset = 1 - jDate.month; offset + jDate.month < 12; offset += 2) {
			final DrawableMonth leftMonth = new DrawableMonth(context,
					SolarCalendar.offsetMonth(jDate, offset + 1), monthWidth,
					monthHeight, false);
			final DrawableMonth rightMonth = new DrawableMonth(context,
					SolarCalendar.offsetMonth(jDate, offset), monthWidth,
					monthHeight, false);

			LayoutParams paramsLeft = new LayoutParams(monthWidth, monthHeight);
			LayoutParams paramsRight = new LayoutParams(monthWidth, monthHeight);
			paramsLeft.setMargins(MARGIN, 0, (int) (0.5 * MARGIN), 0);
			paramsRight.setMargins((int) (0.5 * MARGIN), 0, MARGIN, 0);
			leftMonth.setLayoutParams(paramsLeft);
			rightMonth.setLayoutParams(paramsRight);

			LinearLayout twoMonths = new LinearLayout(context);
			twoMonths.addView(leftMonth);
			twoMonths.addView(rightMonth);
			twoMonths.setPadding(0, MARGIN, 0, 0);

			this.addView(twoMonths);

			// click on a month must take user to month view of that month
			makeClickable(leftMonth);
			makeClickable(rightMonth);
		}
	}

	/**
	 * Draws 12 Gregorian months  
	 */
	private void drawYear(Context context, LocalDate date) {
		CONTEXT = context;
		this.setOrientation(LinearLayout.VERTICAL);
		this.setGravity(Gravity.CENTER);
		this.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));

		int screenWidth = Utils.getScreenWidth(context);
		MARGIN = (int) (screenWidth / WIDTH_MARGIN_RATIO);
		int monthWidth = (int) (screenWidth / 2 - 1.5 * MARGIN);
		int monthHeight = monthWidth * 8 / 7;

		for (int offset = 1 - date.getMonthOfYear(); offset
				+ date.getMonthOfYear() < 12; offset += 2) {
			final DrawableGregorianMonth leftMonth = new DrawableGregorianMonth(context,
					date.plusMonths(offset + 1), monthWidth, monthHeight, false);
			final DrawableGregorianMonth rightMonth = new DrawableGregorianMonth(context,
					date.plusMonths(offset), monthWidth, monthHeight, false);

			LayoutParams paramsLeft = new LayoutParams(monthWidth, monthHeight);
			LayoutParams paramsRight = new LayoutParams(monthWidth, monthHeight);
			paramsLeft.setMargins(MARGIN, 0, (int) (0.5 * MARGIN), 0);
			paramsRight.setMargins((int) (0.5 * MARGIN), 0, MARGIN, 0);
			leftMonth.setLayoutParams(paramsLeft);
			rightMonth.setLayoutParams(paramsRight);

			LinearLayout twoMonths = new LinearLayout(context);
			twoMonths.addView(leftMonth);
			twoMonths.addView(rightMonth);
			twoMonths.setPadding(0, MARGIN, 0, 0);

			this.addView(twoMonths);
		}
	}

	private void makeClickable(final DrawableMonth month) {
		month.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(CONTEXT, MonthView.class);
				intent.putExtra(MainActivity.MONTH_OFFSET,
						SolarCalendar.calculateMonthOffset(month.DATE));
				CONTEXT.startActivity(intent);
			}
		});
	}

}
