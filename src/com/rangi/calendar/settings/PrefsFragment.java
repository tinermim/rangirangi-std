package com.rangi.calendar.settings;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.util.Log;

import com.rangi.calendar.DialogPurchase;
import com.rangi.calendar.PurchaseComponent;
import com.rangi.calendar.PurchaseControl;
import com.rangi.calendar.R;
import com.rangi.calendar.alarm.AlarmService;
import com.rangi.calendar.sync.UiSyncIntro;

public class PrefsFragment extends PreferenceFragment {

	static Activity a;
	static Resources res;
	@Override
	public void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);

		a = getActivity();
		res = getResources();
		addPreferencesFromResource(R.xml.pref_general);

		bindPreferenceSummaryToValue(findPreference("notification_time"));
		bindPreferenceSummaryToValue(findPreference("primary_calendar"));

		final CheckBoxPreference enableNotif = (CheckBoxPreference) getPreferenceManager()
				.findPreference("enable_notification");

		final CheckBoxPreference enableHijri = (CheckBoxPreference) getPreferenceManager()
				.findPreference("enable_islamic_calendar");

		PreferenceScreen prefScreen = (PreferenceScreen) getPreferenceManager()
				.findPreference("sync_calendar");

		
		
		
		prefScreen
				.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
					
					
					public boolean onPreferenceClick(Preference arg0) {
						
						
						Log.i("in Sync method", ""+PurchaseControl.getInstance(getActivity()).isPurchased(PurchaseComponent.PURCHASE_SYNC_WITH_GOOGLE));
						if(PurchaseControl.getInstance(getActivity()).isPurchased(PurchaseComponent.PURCHASE_SYNC_WITH_GOOGLE))
						
						{
						
						Intent intent = new Intent(getActivity(),
								UiSyncIntro.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(intent);
						return true;
						}
						else{
							//Toast.makeText( getActivity() , "boro bekharesh :| ", Toast.LENGTH_SHORT).show();
							callPurchaseDialoge(PurchaseComponent.PURCHASE_SYNC_WITH_GOOGLE);
						}
						
						return true;
						
					}
				});
		enableNotif
				.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
					public boolean onPreferenceChange(Preference preference,
							Object newValue) {
						if ((Boolean) newValue == false)
							AlarmService.cancelDailyAlarm(getActivity());
						else
							AlarmService.setDailyAlarm(getActivity());
						return true;
					}
				});
		
		
		enableHijri
		.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			public boolean onPreferenceChange(Preference preference,
					Object newValue) {

				Log.i("in hijriChangeListerner", ""+PurchaseControl.getInstance(getActivity()).isPurchased(PurchaseComponent.PURCHASE_GEROGRIAN_HIJRI_CALENDAR));
				if(!(PurchaseControl.getInstance(getActivity()).isPurchased(PurchaseComponent.PURCHASE_GEROGRIAN_HIJRI_CALENDAR) &&
						preference.getKey().equals("enable_islamic_calendar")))
				{
					//Toast.makeText( a , "boro bekharesh :| ", Toast.LENGTH_SHORT).show();
					callPurchaseDialoge(PurchaseComponent.PURCHASE_GEROGRIAN_HIJRI_CALENDAR);
					return false;
				}
				return true;
			}
		});
		
		
	}

	
	
	private static  void callPurchaseDialoge(PurchaseComponent p)
	{
		DialogPurchase cdd=new DialogPurchase( a ,p);
				cdd.show();  	
	}
	/**
	 * A preference value change listener that updates the preference's summary
	 * to reflect its new value.
	 */
	private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
		
		
		public boolean onPreferenceChange(Preference preference, Object value) {
			
			String stringValue = value.toString();
			if (preference instanceof ListPreference) {
				int index = ((ListPreference) preference)
						.findIndexOfValue((String) value);
				stringValue = (String) ((ListPreference) preference)
						.getEntries()[index];
			}
			
			 final String[] values = res.getStringArray(R.array.pref_primary_calendar_entries);
			Log.i("preferenceKey", ""+preference.getKey());
			Log.i("string value", ""+stringValue.equals(values[1]));
			
			
			
			if(preference.getKey().equals("primary_calendar") && stringValue.equals(values[1]))
					if(!PurchaseControl.getInstance(a).isPurchased(PurchaseComponent.PURCHASE_GEROGRIAN_HIJRI_CALENDAR) )
					{
						//Toast.makeText( a , "boro bekharesh :| ", Toast.LENGTH_SHORT).show();
						callPurchaseDialoge(PurchaseComponent.PURCHASE_GEROGRIAN_HIJRI_CALENDAR);
						return false;
					}
			
				Log.i("stringValue", stringValue, null);
				preference.setSummary(stringValue);
				return true;

		}
	};

	/**
	 * Binds a preference's summary to its value. More specifically, when the
	 * preference's value is changed, its summary (line of text below the
	 * preference title) is updated to reflect the value. The summary is also
	 * immediately updated upon calling this method. The exact display format is
	 * dependent on the type of preference.
	 * 
	 * @see #sBindPreferenceSummaryToValueListener
	 */
	private static void bindPreferenceSummaryToValue(Preference preference) {
		PreferenceManager.getDefaultSharedPreferences(preference.getContext())
				.getString(preference.getKey(), "");
		// Set the listener to watch for value changes.
		preference
				.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

		// Trigger the listener immediately with the preference's
		// current value.
		sBindPreferenceSummaryToValueListener.onPreferenceChange(
				preference,
				PreferenceManager.getDefaultSharedPreferences(
						preference.getContext()).getString(preference.getKey(),
						""));
	}

}
