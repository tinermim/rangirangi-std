package com.rangi.calendar.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SettingsUtil {
	
	public static final String LAST_VERSION_CODE = "last_version_code";

	public static boolean showHolidays(Context context) {
		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(context);
		return settings.getBoolean("enable_show_holidays", true);
	}

	public static boolean isDailyAlarmEnable(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getBoolean("enable_notification", true);
	}

	public static String getDailyAlarmTime(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getString("notification_time", "09:00");
	}

	public static boolean showIslamicCalendar(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getBoolean("enable_islamic_calendar", false);
	}

	public static boolean primaryCalendarIsJalali(Context context) {
		if (PreferenceManager.getDefaultSharedPreferences(context)
				.getString("primary_calendar", "0").equals("1"))
			return true;
		return false;
	}

	private static final String KEY_CALENDAR_ID = "calendar_id";
	public static final long VALUE_NO_CALENDAR = -1;

	public static long getCalendarId(Context context) {
		return getSettings(context).getLong(KEY_CALENDAR_ID, VALUE_NO_CALENDAR);
	}
	
	public static void setCalendarId(Context context, long id) {
		getSettings(context).edit().putLong(KEY_CALENDAR_ID, id).commit();
	}

	public static SharedPreferences getSettings(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context);
	}

}
