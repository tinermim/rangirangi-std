package com.rangi.calendar.settings;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.ViewStub;

import com.rangi.calendar.CalendarActivity;
import com.rangi.calendar.R;
public class SettingsActivity extends CalendarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setupActionBar();

		ViewStub stub = (ViewStub) findViewById(R.id.stub);
		stub.setLayoutResource(R.layout.activity_app_settings);
		stub.inflate();

		getSupportFragmentManager().beginTransaction()
				.replace(R.id.prefs_fragment_place_holder, new PrefsFragment())
				.commit();
	}

	@Override
	protected void setupActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayShowTitleEnabled(false);

		actionBar.setCustomView(getLayoutInflater().inflate(
				R.layout.action_bar_app_settings, null));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// To neutralize damage of the father!
		return false;
	}
}
