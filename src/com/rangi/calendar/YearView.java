package com.rangi.calendar;

import org.joda.time.LocalDate;

import com.rangi.calendar.settings.SettingsUtil;
import com.rangi.calendar.utils.Utils;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ScrollView;
import android.widget.TextView;

public class YearView extends CalendarActivity {

	public static final int NUM_OF_PAGES = 1000;
	public static final int MIDDLE_PAGE = NUM_OF_PAGES / 2;
	YearsPagerAdapter myYearsPagerAdapter;
	ViewPager myViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ViewStub stub = (ViewStub) findViewById(R.id.stub);
		stub.setLayoutResource(R.layout.activity_year_view);
		stub.inflate();

		Intent intent = getIntent();
		final int offset = intent.getIntExtra(MainActivity.YEAR_OFFSET, 0);

		myYearsPagerAdapter = new YearsPagerAdapter(getSupportFragmentManager());
		myViewPager = (ViewPager) findViewById(R.id.year_pager);
		myViewPager.setAdapter(myYearsPagerAdapter);
		myViewPager.setCurrentItem(MIDDLE_PAGE - offset);
		myViewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int currentPage) {
				setCustomTitle(-(currentPage - MIDDLE_PAGE));
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});

		setupActionBar();
	}

	@Override
	protected void onResume() {
		setupActionBar();
		// Reloading the pages on resume
		myYearsPagerAdapter.notifyDataSetChanged();
		myViewPager.invalidate();
		super.onResume();
	}

	// first calculates the year and then sets the title of screen
	protected void setCustomTitle(int offset) {
		int year;
		if (SettingsUtil.primaryCalendarIsJalali(this))
			year = SolarCalendar.offsetYear(offset).year;
		else
			year = new LocalDate().plusYears(offset).getYear();
		TextView title = (TextView) findViewById(R.id.actionbar_title);
		title.setText("سال " + year + "\n" + "تقویم رنگی رنگی");
		title.setTypeface(Utils.getCustomFont(this));
	}

	public class YearsPagerAdapter extends FragmentStatePagerAdapter {

		public YearsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int biasedOffset) {
			Fragment fragment = new YearFragment();
			Bundle args = new Bundle();
			args.putInt(YearFragment.OFFSET, -(biasedOffset - MIDDLE_PAGE));
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			return NUM_OF_PAGES;
		}

		// To reload the page once a change occured
		@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE;
		}
	}

	public static class YearFragment extends Fragment {

		public static final String OFFSET = "offset";

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			Bundle args = getArguments();
			int offset = args.getInt(OFFSET);
			ScrollView sv = new ScrollView(getActivity());
			sv.addView(new YearDrawer(getActivity(), offset));
			return sv;
		}

	}

	protected void setupActionBar() {
		super.setupActionBar();

		// setting the title
		int offset = -(myViewPager.getCurrentItem() - MIDDLE_PAGE);
		setCustomTitle(offset);

		// adding action to month button
		findViewById(R.id.month_view).setOnClickListener(
				new View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
						final int currentItem = YearView.this.myViewPager
								.getCurrentItem();
						gotoMonthView(SolarCalendar
								.offsetYear(-(currentItem - MIDDLE_PAGE)));
					}
				});

	}

	protected void gotoMonthView(SolarCalendar jDate) {
		Intent intent = new Intent(this, MonthView.class);
		intent.putExtra(MainActivity.MONTH_OFFSET,
				SolarCalendar.calculateMonthOffset(jDate));
		startActivity(intent);
	}

}
