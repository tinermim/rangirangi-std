package com.rangi.calendar;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.Button;

import com.rangi.calendar.utils.Utils;

/*
 * Buttons in Store activity implemented for handling the purchase process and dialoges. each button has a purchaseComponent
 * to know by itself it is binded to which purchase item.
 */
public class PurchaseButton extends Button{

	public PurchaseComponent item;
	public PurchaseButton(Context context , PurchaseComponent item) {
		super(context);
		this.item = item;
		init(context );
	}
	
	private void init(Context context){
		this.setTypeface(Utils.getCustomFont(context));
		this.setGravity(Gravity.CENTER);
		//setOnClickListener( this);
	}
	public PurchaseButton(Context context) {
		super(context);
		init(context);
	}
	
	public PurchaseButton(Context context, AttributeSet attrs) {
        super(context, attrs);
		init(context);
	}
	
	public PurchaseButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
		init(context);

	}
	
    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);
    }
    
//    @Override
//	public void onClick(View v ) {			
//		
//		Log.e("item clikced" , ""+((PurchaseButton)v).item);
//				DialogPurchase cdd=new DialogPurchase((Activity)v.getContext() , ((PurchaseButton)v).item);
//				cdd.show();  
//	}	
}
