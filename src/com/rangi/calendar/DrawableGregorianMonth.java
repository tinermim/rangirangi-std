package com.rangi.calendar;

import java.text.DateFormatSymbols;
import org.joda.time.LocalDate;
import org.joda.time.Months;

import com.rangi.calendar.utils.ColorUtils;
import com.rangi.calendar.utils.Utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.view.View;

public class DrawableGregorianMonth extends View {

	// Colors
	private static final int TODAY_FONT_COLOR = Color.parseColor("#09b89d");
	private static final int BORDER_COLOR = Color.parseColor("#adadad");
	private static final int TITLE_BG_COLOR = Color.parseColor("#d9d9d9");
	private static final int TITLE_TEXT_COLOR = Color.BLACK;
	private static final int EMPTY_CELL_COLOR = Color.parseColor("#e7e7e8");

	// Sizes
	private static int CELL_WIDTH;
	private static int CELL_HEIGHT;
	private static int CELL_FONT_SIZE;
	private static float TITLE_XPOS;
	private static float TITLE_YPOS;

	private Paint paint = new Paint();
	private static Context CONTEXT;
	private final LocalDate DATE;
	private static final String[] SHORT_WEEKDAYS = new DateFormatSymbols()
			.getShortWeekdays();

	public DrawableGregorianMonth(Context context, LocalDate date, int width,
			int height, boolean small) {
		super(context);
		CONTEXT = context;

		DATE = new LocalDate(date);

		// initializing basic sizes
		CELL_WIDTH = width / 7;
		CELL_HEIGHT = height / 8;
		CELL_FONT_SIZE = small ? CELL_WIDTH / 3 : CELL_WIDTH / 2;
		TITLE_XPOS = CELL_WIDTH / 3f;
		TITLE_YPOS = CELL_HEIGHT / 1.7f;

		setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(CONTEXT, MonthView.class);
				intent.putExtra(MainActivity.MONTH_OFFSET, Months
						.monthsBetween(new LocalDate().withDayOfMonth(1), DATE.withDayOfMonth(1)).getMonths());
				CONTEXT.startActivity(intent);
			}
		});
	}

	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		drawTitle(canvas);

		LocalDate date = new LocalDate(DATE).withDayOfMonth(1);
		int currentMonth = date.getMonthOfYear();
		date = date.minusDays(date.getDayOfWeek() % 7);

		for (int w = 0; w < 7; w++) {
			int cellY = (w + 1) * CELL_HEIGHT; // leaving space for title [cellY
			for (int i = 0; i < 7; i++) {
				int cellX = i * CELL_WIDTH;

				if (w == 0) {
					String weekDayName = SHORT_WEEKDAYS[i + 1].substring(0, 2);
					drawCell(canvas, cellX, cellY, weekDayName,
							EMPTY_CELL_COLOR, Color.BLACK);
				} else if (date.getMonthOfYear() != currentMonth) {
					drawCell(canvas, cellX, cellY, "", EMPTY_CELL_COLOR,
							Color.WHITE);
					date = date.plusDays(1);
				} else {
					String dayLabel = "" + date.getDayOfMonth();
					if (date.equals(LocalDate.now())) {
						drawCell(canvas, cellX, cellY, dayLabel, Color.WHITE,
								TODAY_FONT_COLOR);
					} else {
						int color = ColorUtils.getColor(CONTEXT,
								(currentMonth + 9) % 12 + 1, 7 * (w - 1) + i);
						drawCell(canvas, cellX, cellY, dayLabel, color,
								Color.WHITE);
					}
					date = date.plusDays(1);
				}
			}
		}
	}

	private void drawTitle(Canvas canvas) {
		// drawing background
		paint.setColor(TITLE_BG_COLOR);
		paint.setStyle(Style.FILL);
		canvas.drawRect(0, 0, 7 * CELL_WIDTH, CELL_HEIGHT, paint);

		// drawing name of month
		paint.setColor(TITLE_TEXT_COLOR);
		paint.setTypeface(Utils.getCustomFont(CONTEXT));
		paint.setTextSize(CELL_FONT_SIZE);
		paint.setTextAlign(Paint.Align.RIGHT);
		canvas.drawText(SolarCalendar.getFarsiMonthName(DATE), 7 * CELL_WIDTH - TITLE_XPOS, TITLE_YPOS, paint);
	}

	private void drawCell(Canvas canvas, int cellX, int cellY,
			String cellLabel, int cellColor, int textColor) {

		// drawing the cell
		paint.setColor(cellColor);
		paint.setStyle(Style.FILL);
		canvas.drawRect(cellX, cellY, CELL_WIDTH + cellX, CELL_HEIGHT + cellY,
				paint);

		// drawing the cell border
		paint.setColor(BORDER_COLOR);
		paint.setStyle(Style.STROKE);
		paint.setStrokeWidth(Utils.dipToPixels(CONTEXT, 0.5f));
		canvas.drawRect(cellX, cellY, CELL_WIDTH + cellX, CELL_HEIGHT + cellY,
				paint);

		// drawing the text of cell
		paint.setColor(textColor);
		paint.setTypeface(null);
		paint.setStrokeWidth(0);
		paint.setTextSize(CELL_FONT_SIZE);
		paint.setTextAlign(Paint.Align.CENTER);
		canvas.drawText(cellLabel, (2 * cellX + CELL_WIDTH) / 2,
				(2 * cellY + CELL_HEIGHT) / 2, paint);
	}
}