package com.rangi.calendar;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.joda.time.LocalDate;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.rangi.calendar.customviews.MyEditText;
import com.rangi.calendar.customviews.MyTextView;

public class DateConverterActivity extends RangiActionBarActivity {

	private static final int MIN_JALALI_CONVERTIBLE_YEAR = 979;
	private static final int MIN_GREGORIAN_CONVERTIBLE_YEAR = 623;

	private SolarCalendar todayJalali;
	private LocalDate todayGregorian;

	private MyEditText jalaliYearEditText;
	private int selectedJalaliMonth;
	private MyEditText jalaliDayEditText;

	private EditText gregorianYearEditText;
	private int selectedGregorianMonth;
	private EditText gregorianDayEditText;

	private DrawerLayout mDrawerLayout;
	private LinearLayout mDrawer;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_date_converter);
		setupActionBar();

		todayJalali = new SolarCalendar();
		todayGregorian = new LocalDate();

		setupJalaliMonthSpinner();
		setupJalaliYearEditText();
		setupJalaliDayEditText();

		setupGregorianYearEditText();
		setupGregorianMonthSpinner();
		setupGregorianDayEditText();

		setupDrawer();
	}
	
	protected void setupActionBar() {
		super.setupActionBar(R.layout.action_bar_simple);
		// ((ImageView) findViewById(R.id.icon))
		// .setBackgroundResource(R.drawable.ic_convertor);
		((MyTextView) findViewById(R.id.title))
				.setText(R.string.action_date_converter);
		((MyTextView) findViewById(R.id.title)).setTextSize(
				TypedValue.COMPLEX_UNIT_SP, 18);
	}

	private void setupJalaliYearEditText() {
		jalaliYearEditText = (MyEditText) findViewById(R.id.year_jalali);
		jalaliYearEditText.setText("" + todayJalali.year);
		removeFocusOnDoneClicked(jalaliYearEditText);
		removeRedBorderOnClick(jalaliYearEditText);
	}

	private void setupJalaliMonthSpinner() {
		Spinner spinner = (Spinner) findViewById(R.id.spinner_month_jalali);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.date_converter_spinner_item_fa,
				SolarCalendar.getMonthNames());
		adapter.setDropDownViewResource(R.layout.date_converter_spinner_item_fa);
		spinner.setAdapter(adapter);
		spinner.setSelection(todayJalali.month - 1);
		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				selectedJalaliMonth = (int) id;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
	}

	private void setupJalaliDayEditText() {
		jalaliDayEditText = (MyEditText) findViewById(R.id.day_jalali);
		jalaliDayEditText.setText("" + todayJalali.date);
		removeFocusOnDoneClicked(jalaliDayEditText);
		removeRedBorderOnClick(jalaliDayEditText);
	}

	private void setupGregorianYearEditText() {
		gregorianYearEditText = (EditText) findViewById(R.id.year_gregorian);
		gregorianYearEditText.setText("" + todayGregorian.getYear());
		removeFocusOnDoneClicked(gregorianYearEditText);
		removeRedBorderOnClick(gregorianYearEditText);
	}

	private void setupGregorianMonthSpinner() {
		Spinner spinner = (Spinner) findViewById(R.id.spinner_month_gregorian);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.date_converter_spinner_item_fa,
				new DateFormatSymbols().getMonths());
		adapter.setDropDownViewResource(R.layout.date_converter_spinner_item_fa);
		spinner.setAdapter(adapter);
		spinner.setSelection(todayGregorian.getMonthOfYear() - 1);
		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				selectedGregorianMonth = (int) id;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
	}

	private void setupGregorianDayEditText() {
		gregorianDayEditText = (EditText) findViewById(R.id.day_gregorian);
		gregorianDayEditText.setText("" + todayGregorian.getDayOfMonth());
		removeFocusOnDoneClicked(gregorianDayEditText);
		removeRedBorderOnClick(gregorianDayEditText);
	}

	private void removeRedBorderOnClick(final EditText gregorianYearEditText2) {
		gregorianYearEditText2
				.setOnFocusChangeListener(new OnFocusChangeListener() {

					@Override
					public void onFocusChange(View view, boolean hasFocus) {
						if (hasFocus)
							((View) gregorianYearEditText2.getParent())
									.setBackgroundColor(Color.TRANSPARENT);
					}
				});
	}

	private void removeFocusOnDoneClicked(final EditText gregorianYearEditText2) {
		gregorianYearEditText2
				.setOnEditorActionListener(new OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						if (actionId == EditorInfo.IME_ACTION_DONE) {
							gregorianYearEditText2.clearFocus();
							closeKeyboard();
						}
						return false;
					}
				});
	}

	public void convertJalaliToGregorianAndShow(View view) {
		closeKeyboard();
		if (validateJalaliInput()) {
			LocalDate date = SolarCalendar.jalaliToGregorian(getJalaliYear(),
					selectedJalaliMonth, getJalaliDay());
			((TextView) findViewById(R.id.gregorian_date_result)).setText(date
					.toString("dd MMMM yyyy"));
		}
	}

	public void convertGregorianToJalaliAndShow(View view) {
		closeKeyboard();
		if (validateGregorianInput()) {
			SolarCalendar date = new SolarCalendar(new LocalDate(
					getGregorianYear(), selectedGregorianMonth + 1,
					getGregorianDay()));
			((TextView) findViewById(R.id.jalali_date_result)).setText(""
					+ date.date + " " + date.strMonth + " " + date.year);
		}
	}

	private boolean validateJalaliInput() {
		if (jalaliYearEditText.getText().toString().equals(""))
			return invalidJalaliYear();
		if (jalaliDayEditText.getText().toString().equals(""))
			return invaliJalaliDay();
		int year = getJalaliYear();
		int day = getJalaliDay();
		if (year < MIN_JALALI_CONVERTIBLE_YEAR)
			return invalidJalaliYear();
		if ((day < 1)
				|| (day > SolarCalendar.daysOfMonth(year,
						selectedJalaliMonth + 1)))
			return invaliJalaliDay();
		return true;
	}

	private boolean validateGregorianInput() {
		if (gregorianYearEditText.getText().toString().equals(""))
			return invalidGregorianYear();
		if (gregorianDayEditText.getText().toString().equals(""))
			return invaliGregorianDay();
		int year = getGregorianYear();
		int day = getGregorianDay();
		if (year < MIN_GREGORIAN_CONVERTIBLE_YEAR)
			return invalidGregorianYear();
		if ((day < 1)
				|| (day > new GregorianCalendar(year, selectedGregorianMonth, 1)
						.getActualMaximum(Calendar.DATE)))
			return invaliGregorianDay();
		return true;
	}

	private boolean invalidJalaliYear() {
		((View) jalaliYearEditText.getParent())
				.setBackgroundResource(R.drawable.red_border);
		Toast.makeText(
				this,
				"کمترین سال قابل تبدیل " + MIN_JALALI_CONVERTIBLE_YEAR
						+ " است.", Toast.LENGTH_SHORT).show();
		return false;
	}

	private boolean invaliJalaliDay() {
		((View) jalaliDayEditText.getParent())
				.setBackgroundResource(R.drawable.red_border);
		Toast.makeText(this, "روز انتخاب شده در محدوده ی صحیح نیست",
				Toast.LENGTH_SHORT).show();
		return false;
	}

	private boolean invalidGregorianYear() {
		((View) gregorianYearEditText.getParent())
				.setBackgroundResource(R.drawable.red_border);
		Toast.makeText(
				this,
				"کمترین سال قابل تبدیل " + MIN_GREGORIAN_CONVERTIBLE_YEAR
						+ " است.", Toast.LENGTH_SHORT).show();
		return false;
	}

	private boolean invaliGregorianDay() {
		((View) gregorianDayEditText.getParent())
				.setBackgroundResource(R.drawable.red_border);
		Toast.makeText(this, "روز انتخاب شده در محدوده ی صحیح نیست",
				Toast.LENGTH_SHORT).show();
		return false;
	}

	private int getJalaliYear() {
		return Integer.valueOf(jalaliYearEditText.getText().toString());
	}

	private int getJalaliDay() {
		return Integer.valueOf(jalaliDayEditText.getText().toString());
	}

	private int getGregorianYear() {
		return Integer.valueOf(gregorianYearEditText.getText().toString());
	}

	private int getGregorianDay() {
		return Integer.valueOf(gregorianDayEditText.getText().toString());
	}

	private void closeKeyboard() {
		((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE))
				.hideSoftInputFromWindow(jalaliYearEditText.getWindowToken(), 0);
	}
}
