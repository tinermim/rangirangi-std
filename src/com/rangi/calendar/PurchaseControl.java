package com.rangi.calendar;

import android.content.Context;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * this class is an interface between the program and shared preferences where in three boolean are saved. these boolean variables shows 
 * which packages are bought. through this class you can get & set the variables without caring about the codes needed to communicate 
 * with the shared preferences
 * @author ramtin
 * @version <$id><$tag>
 */
public class PurchaseControl {

	static private SharedPreferences mPref;
	static private boolean Notes;
	static private boolean Calendar;
	static private boolean SyncGoogle;
	static private Editor edit;
	
	private static PurchaseControl sInstance;
	
	public PurchaseControl(Context context) {
		// To be consistent with previous version, we get shared preferences with MainActivity class name. 
		mPref = context.getSharedPreferences(MainActivity.class.getSimpleName(), Context.MODE_PRIVATE);
	}

	public static PurchaseControl getInstance(Context context) {
		if (sInstance == null)
			sInstance = new PurchaseControl(context.getApplicationContext());
		return sInstance;
	}

	public boolean isPurchased(PurchaseComponent p) {
		Notes = mPref.getBoolean("Notes", true);
		Calendar = mPref.getBoolean("Calendar", true);
		SyncGoogle = mPref.getBoolean("SyncGoogle", true);

		switch (p) {
		case PURCHASE_NOTES:
			return Notes;
		case PURCHASE_GEROGRIAN_HIJRI_CALENDAR:
			return Calendar;
		case PURCHASE_SYNC_WITH_GOOGLE:
			return SyncGoogle;
		default:
			return false;
		}
	}

	public void setPurchaseSituation(PurchaseComponent p, boolean value) {
		edit = mPref.edit();
		String key = null;
		switch (p) {
		case PURCHASE_NOTES:
			key = "Notes";
			break;
		case PURCHASE_GEROGRIAN_HIJRI_CALENDAR:
			key = "Calendar";
			break;
		case PURCHASE_SYNC_WITH_GOOGLE:
			key = "SyncGoogle";
			break;
		default:
			break;

		}
		edit.putBoolean(key, value).commit();
	}

}
