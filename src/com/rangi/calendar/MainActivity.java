package com.rangi.calendar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;

import com.rangi.calendar.MyDataTypes.Note;
import com.rangi.calendar.alarm.AlarmService;
import com.rangi.calendar.note.NoteManager;
import com.rangi.calendar.settings.SettingsUtil;
import com.rangi.calendar.utils.Utils;

import java.util.ArrayList;

public class MainActivity extends ActionBarActivity {

	public static final String DAY = "com.notionplex.ashkan.DAY";
	public static final String MONTH = "com.notionplex.ashkan.MONTH";
	public static final String YEAR = "com.notionplex.ashkan.YEAR";
	protected static final String G_DAY = "com.notionplex.ashkan.G_DAY";
	protected static final String G_MONTH = "com.notionplex.ashkan.G_MONTH";
	protected static final String G_YEAR = "com.notionplex.ashkan.G_YEAR";
	protected static final String MONTH_OFFSET = "com.notionplex.ashkan.MONTH_OFFSET";
	protected static final String YEAR_OFFSET = "com.notionplex.ashkan.YEAR_OFFSET";
	protected static final String WEEK_OFFSET = "com.notionplex.ashkan.WEEK_OFFSET";
	public static final String DAY_OFFSET = "com.notionplex.ashkan.DAY_OFFSET";
	private static final String PREF_FIRST_LAUNCH = "PREF_FIRST_LAUNCH";

	private SharedPreferences settings;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		PreferenceManager.setDefaultValues(this, R.xml.pref_general, false);
		settings = SettingsUtil.getSettings(this);
		Editor preferenceEditor = settings.edit();

		fixPreviousVersionBugs();

		if (!AlarmService.isRunning())
			AlarmService.setDailyAlarm(this);

		if (settings.getBoolean(PREF_FIRST_LAUNCH, true)) {
			/* Executed on first run */
			preferenceEditor.putBoolean(PREF_FIRST_LAUNCH, false);
			preferenceEditor.commit();
			Intent intent = new Intent(this, IntroActivity.class);
			startActivity(intent);
		} else {
			startMonthView();
		}
	}

	private void startMonthView() {
		Intent intent = new Intent(this, MonthView.class);
		intent.putExtra(MONTH_OFFSET, 0);
		startActivity(intent);
	}

	private void fixPreviousVersionBugs() {
		if (settings.getInt(SettingsUtil.LAST_VERSION_CODE, 0) == 250) {
			fixVersion250Bug();
		}
	}

	/**
	 * Version 2.5.1 bug: Internal notes were not copied to external calendar
	 * after syncing.
	 */
	private void fixVersion250Bug() {
		long calId = SettingsUtil.getCalendarId(this);
		if (calId != SettingsUtil.VALUE_NO_CALENDAR) {
			SettingsUtil.setCalendarId(this, SettingsUtil.VALUE_NO_CALENDAR);
			ArrayList<Note> notes = NoteManager.getInstance(this).getAllNotes();
			SettingsUtil.setCalendarId(this, calId);
			Utils.copyNotesToCalendar(this, notes);
		}
	}

}
