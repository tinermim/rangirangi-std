package com.rangi.calendar.customviews;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.CheckBox;

import com.rangi.calendar.utils.Utils;

public class MyCheckBox extends CheckBox {
	
	public MyCheckBox(Context context) {
		super(context);
		this.setTypeface(Utils.getCustomFont(context));
	}
	
	public MyCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Utils.getCustomFont(context));
	}
	
	public MyCheckBox(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setTypeface(Utils.getCustomFont(context));
	}
	
    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);
    }
}
