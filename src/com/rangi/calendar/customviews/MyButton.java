package com.rangi.calendar.customviews;

import com.rangi.calendar.utils.Utils;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.Button;

public class MyButton extends Button {

	public MyButton(Context context) {
		super(context);
		
		this.setTypeface(Utils.getCustomFont(context));
		this.setGravity(Gravity.CENTER);
	}
	
	public MyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Utils.getCustomFont(context));
        this.setGravity(Gravity.CENTER);
	}
	
	public MyButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setTypeface(Utils.getCustomFont(context));
        this.setGravity(Gravity.CENTER);
	}
	
    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);
    }

}
