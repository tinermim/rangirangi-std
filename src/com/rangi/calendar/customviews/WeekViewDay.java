package com.rangi.calendar.customviews;

import com.rangi.calendar.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

public class WeekViewDay extends LinearLayout {

	public WeekViewDay(Context context) {
		super(context);

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.week_view_day, this);
	}

}
