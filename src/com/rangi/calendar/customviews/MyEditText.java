package com.rangi.calendar.customviews;

import com.rangi.calendar.utils.Utils;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.EditText;

public class MyEditText extends EditText {
	
	public MyEditText(Context context) {
		super(context);
		this.setTypeface(Utils.getCustomFont(context));
	}
	
	public MyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Utils.getCustomFont(context));
	}
	
	public MyEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.setTypeface(Utils.getCustomFont(context));
	}
	
    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);
    }
}
