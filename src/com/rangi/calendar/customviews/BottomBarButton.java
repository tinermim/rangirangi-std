package com.rangi.calendar.customviews;

import com.rangi.calendar.utils.Utils;
import com.rangi.calendar.R;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.Button;

public class BottomBarButton extends Button {

	private final static int TEXT_COLOR = Color.parseColor("#666666");

	public BottomBarButton(Context context) {
		super(context);
		this.setBackgroundResource(R.drawable.selector_bottom_bar_button);
		this.setTextColor(TEXT_COLOR);
		this.setTypeface(Utils.getCustomFont(context));
	}

	public BottomBarButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.setBackgroundResource(R.drawable.selector_bottom_bar_button);
		this.setTextColor(TEXT_COLOR);
		this.setTypeface(Utils.getCustomFont(context));
	}

	public BottomBarButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.setBackgroundResource(R.drawable.selector_bottom_bar_button);
		this.setTextColor(TEXT_COLOR);
		this.setTypeface(Utils.getCustomFont(context));
	}

	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
	}

}
