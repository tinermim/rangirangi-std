package com.rangi.calendar;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.PhoneLookup;
import android.util.TypedValue;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.rangi.calendar.MyDataTypes.Birthday;
import com.rangi.calendar.MyDataTypes.Reminder;
import com.rangi.calendar.alarm.AlarmService;
import com.rangi.calendar.database.DatabaseHelper;
import com.rangi.calendar.utils.Utils;

import java.util.List;

import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.ArrayWheelAdapter;
import kankan.wheel.widget.adapters.NumericWheelAdapter;

public class BirthdayEditor extends SimpleActionBarActivity {

	public static final String BIRTHDAY_ID = "BIRTHDAY_ID";
	public static final String MONTH = "MONTH";
	public static final String DAY = "DAY";
	public static final String NAME = "NAME";
	public static final String PHOTO_URI = "PHOTO_URI";

	private static final long UNDEFINED = -1;

	private WheelView monthWheel;
	private WheelView dayWheel;

	private EditText enteredName;
	private String contactPhoto;
	private long birthdayId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_birthday_editor);
		setupActionBar(R.layout.action_bar_birthday_editor);

		Intent intent = getIntent();
		SolarCalendar today = new SolarCalendar();
		birthdayId = intent.getLongExtra(BIRTHDAY_ID, UNDEFINED);
		Birthday birthday;
		int month = today.month - 1;
		int day = today.date;

		enteredName = (EditText) findViewById(R.id.entered_name);

		if (birthdayId != UNDEFINED) {
			birthday = DatabaseHelper.getDbHelper(this).getBirthday(birthdayId);
			month = birthday.month;
			day = birthday.day;
			enteredName.setText(birthday.name);
		}

		monthWheel = (WheelView) findViewById(R.id.month);
		dayWheel = (WheelView) findViewById(R.id.day);

		//monthWheel.setCenterDrawable(R.drawable.wheel_box_birthday_editor);
		monthWheel.setViewAdapter(new MonthWheelAdapter(this, SolarCalendar
				.getMonthNames()));
		monthWheel.setCurrentItem(month);
		monthWheel.addChangingListener(new OnWheelChangedListener() {
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				updateDays(monthWheel, dayWheel);
			}
		});

		//dayWheel.setCenterDrawable(R.drawable.wheel_box_birthday_editor);
		updateDays(monthWheel, dayWheel);
		dayWheel.setCurrentItem(day - 1);
	}

	void updateDays(WheelView month, WheelView day) {
		int maxDays = month.getCurrentItem() > 5 ? 30 : 31;
		day.setViewAdapter(new DayWheelAdapter(this, maxDays));
		int curDay = Math.min(maxDays, day.getCurrentItem() + 1);
		day.setCurrentItem(curDay - 1, true);
	}

	private class DayWheelAdapter extends NumericWheelAdapter {

		public DayWheelAdapter(Context context, int maxValue) {
			super(context, 1, maxValue);
		}

		@Override
		protected void configureTextView(TextView view) {
			super.configureTextView(view);
			view.setTypeface(Utils.getCustomFont(context));
			view.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
		}

	}

	private class MonthWheelAdapter extends ArrayWheelAdapter<String> {

		public MonthWheelAdapter(Context context, String[] items) {
			super(context, items);
		}

		@Override
		protected void configureTextView(TextView view) {
			super.configureTextView(view);
			view.setTypeface(Utils.getCustomFont(context));
			view.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
		}

	}

	public void selectFromContacts(View view) {
		Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
				Contacts.CONTENT_URI);
		startActivityForResult(contactPickerIntent, 1);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data != null) {
			Uri uri = data.getData();

			if (uri != null) {
				Cursor c = null;
				try {
					c = getContentResolver().query(
							uri,
							new String[] { ContactsContract.Contacts._ID,
									PhoneLookup.DISPLAY_NAME }, null, null,
							null);

					if (c != null && c.moveToFirst()) {
						String contactId = c.getString(0);
						String contactName = c.getString(1);
						enteredName.setText(contactName);
						Uri photo = getPhotoUriFromID(contactId);
						contactPhoto = photo == null ? null : photo.toString();
					}
				} finally {
					if (c != null) {
						c.close();
					}
				}
			}
		}
	}

	private Uri getPhotoUriFromID(String id) {
		try {
			Cursor cur = getContentResolver()
					.query(ContactsContract.Data.CONTENT_URI,
							null,
							ContactsContract.Data.CONTACT_ID
									+ "="
									+ id
									+ " AND "
									+ ContactsContract.Data.MIMETYPE
									+ "='"
									+ ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE
									+ "'", null, null);
			if (cur != null) {
				if (!cur.moveToFirst()) {
					return null; // no photo
				}
			} else {
				return null; // error in cursor process
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		Uri person = ContentUris.withAppendedId(
				ContactsContract.Contacts.CONTENT_URI, Long.parseLong(id));
		return Uri.withAppendedPath(person,
				ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
	}

	public void finishEditing(View view) {
		if (birthdayId == UNDEFINED)
			addBirthday();
		else
			updateBirthday();
		finish();
	}

	public void cancelEditing(View view) {
		finish();
	}

	private void addBirthday() {
		int month = monthWheel.getCurrentItem();
		int day = dayWheel.getCurrentItem() + 1;
		String name = enteredName.getText().toString();
		long birthdayId = DatabaseHelper.getDbHelper(this).addBirthday(month,
				day, name, contactPhoto);
		addReminders(birthdayId, month, day);
	}

	private void updateBirthday() {
		int month = monthWheel.getCurrentItem();
		int day = dayWheel.getCurrentItem() + 1;
		String name = enteredName.getText().toString();
		DatabaseHelper.getDbHelper(this).updateBirthday(birthdayId, month, day,
				name, contactPhoto);
		updateReminders(month, day);
	}

	private void addReminders(long birthdayId, int month, int day) {
		for (int type : new int[] { Reminder.BIRTHDAY_ALARM_0,
				Reminder.BIRTHDAY_ALARM_3 }) {

			Reminder reminder = new Reminder(-1, birthdayId,
					getBirthdayDateTimeString(month, day),
					Reminder.REMINDER_FOR_BIRTHDAY, type);
			reminder.id = DatabaseHelper.getDbHelper(this).addReminder(
					birthdayId, reminder);
			AlarmService.setReminder(this, reminder.id);
		}
	}

	private void updateReminders(int month, int day) {
		List<Reminder> reminders = DatabaseHelper.getDbHelper(this)
				.getBirthdayReminders(birthdayId);
		for (Reminder reminder : reminders) {
			DatabaseHelper.getDbHelper(this).updateReminder(reminder.id,
					getBirthdayDateTimeString(month, day));
			AlarmService.resetReminder(this, reminder.id);
		}
	}

	private String getBirthdayDateTimeString(int month, int day) {
		return month + "-" + day + "-" + 8 + "-" + 0;
	}

	public void cancel(View view) {
		finish();
	}
}
